package me.nothixal.hugs.managers.items;

import com.cryptomorin.xseries.XMaterial;
import java.util.List;
import java.util.UUID;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public interface ItemManager {

  ItemStack createItem(ItemStack item);

  ItemStack createItem(ItemStack item, String name);

  ItemStack createItem(ItemStack item, String name, List<String> lore);

  ItemStack createItem(Material material);

  ItemStack createItem(Material material, String name);

  ItemStack createItem(Material material, String name, List<String> lore);

  // XMaterials
  ItemStack createItem(XMaterial material);

  ItemStack createItem(XMaterial material, String name);

  ItemStack createItem(XMaterial material, String name, List<String> lore);

  ItemStack createItemNoAttrib(XMaterial material);

  ItemStack createItemNoAttrib(XMaterial material, String displayName);

  ItemStack createItemNoAttrib(XMaterial material, String displayName, List<String> lore);

  // Skulls
  ItemStack createSkullItem(String owner);

  ItemStack createSkullItem(String owner, String displayName);

  ItemStack createSkullItem(String owner, String displayName, List<String> lore);

  ItemStack createSkullItem(UUID owner);

  ItemStack createSkullItem(UUID owner, String displayName);

  ItemStack createSkullItem(UUID owner, String displayName, List<String> lore);

  ItemStack createSkullItem(ItemStack item, String displayName);

  ItemStack createSkullItem(ItemStack item, String displayName, List<String> lore);

  ItemStack createSkullItem(ItemStack item, List<String> lore);

  // Skulls from NMS
  ItemStack createNMSSkullItem(String textureValue, String displayName);

  ItemStack createNMSSkullItem(String textureValue, String displayName, List<String> lore);

  ItemStack createNMSSkullItem(String textureValue, ItemStack item);

  ItemStack createNMSSkullItem(String textureValue, ItemStack item, String name);

  // Extra NMS values.


  ItemStack setGlowing(ItemStack item, boolean bool);

  ItemStack getEros(UUID uuid);

  ItemStack getSilvia(UUID uuid);

  ItemStack getRandomLovebird(UUID uuid);

  void openBook(JavaPlugin plugin, Player player, ItemStack book);
}
