//package me.nothixal.hugs.managers.advancements;
//
//import com.google.gson.JsonObject;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.UUID;
//import me.nothixal.hugs.enums.AdvancementBackground;
//import me.nothixal.hugs.me.nothixal.hugs.utils.json.JSONUtils;
//import me.nothixal.hugs.me.nothixal.hugs.utils.text.TextUtils;
//import net.minecraft.advancements.Advancement;
//import net.minecraft.advancements.AdvancementProgress;
//import net.minecraft.advancements.AdvancementRewards;
//import net.minecraft.advancements.Criterion;
//import net.minecraft.advancements.CriterionTriggerInstance;
//import net.minecraft.advancements.DisplayInfo;
//import net.minecraft.advancements.FrameType;
//import net.minecraft.advancements.critereon.SerializationContext;
//import net.minecraft.network.chat.Component;
//import net.minecraft.network.protocol.game.PacketPlayOutAdvancements;
//import net.minecraft.resources.MinecraftKey;
//import net.minecraft.resources.ResourceLocation;
//import org.bukkit.Bukkit;
//import org.bukkit.craftbukkit.v1_18_R1.entity.CraftPlayer;
//import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
//import org.bukkit.entity.Player;
//import org.bukkit.inventory.ItemStack;
//
//public class Advancement_v1_18_R1 implements AdvancementManager {
//
//  @Override
//  public void sendToast(ItemStack item, UUID uuid, String title, String description) {
//    Player player = Bukkit.getPlayer(uuid);
//
//    ResourceLocation pluginKey = new ResourceLocation("hugs.plugin", "notification");
//
//    AdvancementRewards advRewards = new AdvancementRewards(0, new ResourceLocation[0], new ResourceLocation[0], null);
//
//    Map<String, Criterion> advCriteria = new HashMap<>();
//    String[][] advRequirements;
//
//    advCriteria.put("for_free", new Criterion(new CriterionTriggerInstance() {
//
//      @Override
//      public ResourceLocation getCriterion() {
//        return new ResourceLocation("minecraft", "impossible");
//      }
//
//      @Override
//      public JsonObject serializeToJson(SerializationContext serializationContext) {
//        return null;
//      }
//    }));
//
//    List<String[]> fixedRequirements = new ArrayList<>();
//
//    fixedRequirements.add(new String[]{"for_free"});
//
//    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);
//
//    DisplayInfo display = new DisplayInfo(CraftItemStack.asNMSCopy(item),
//        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
//        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
//        new ResourceLocation(AdvancementBackground.SNOW.getValue()),
//        FrameType.GOAL, true, true, true);
//    Advancement advancement = new Advancement(pluginKey, null, display, advRewards, advCriteria, advRequirements);
//
//    Map<MinecraftKey, AdvancementProgress> prg = new HashMap<>();
//
//    AdvancementProgress advPrg = new AdvancementProgress();
//    advPrg.getCriterion("for_free").getObtained();
//    prg.put(MinecraftKey.a(pluginKey.getNamespace()), advPrg);
//
//    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, Collections.singletonList(advancement), new HashSet<>(), prg);
//    ((CraftPlayer) player).getHandle().connection.send(packet);
//
//    Set<MinecraftKey> rm = new HashSet<>();
//    rm.add(MinecraftKey.a(pluginKey.getNamespace()));
//    prg.clear();
//
//    MinecraftKey key = new MinecraftKey("");
//    key.b();
//
//    packet = new PacketPlayOutAdvancements(false, new ArrayList<>(), rm, prg);
//    ((CraftPlayer) player).getHandle().connection.send(packet);
//  }
//
//  @Override
//  public void sendToast(UUID uuid, String title, String description) {
//
//  }
//
//  @Override
//  public void sendToast(Player player, String title, String description) {
//
//  }
//
//  @Override
//  public void sendToast(String playerName, String title, String description) {
//
//  }
//
//  @Override
//  public void sendGoalToast(ItemStack item, UUID uuid, String title, String description) {
//
//  }
//
//  @Override
//  public void sendChallengeToast(ItemStack item, UUID uuid, String title, String description) {
//    Player player = Bukkit.getPlayer(uuid);
//
//    ResourceLocation notName = new ResourceLocation("hugs.plugin", "notification");
//
//    AdvancementRewards advRewards = new AdvancementRewards(0, new ResourceLocation[0], new ResourceLocation[0], null);
//
//    Map<String, Criterion> advCriteria = new HashMap<>();
//    String[][] advRequirements;
//
//    advCriteria.put("for_free", new Criterion(new CriterionTriggerInstance() {
//
//      @Override
//      public ResourceLocation getCriterion() {
//        return new ResourceLocation("minecraft", "impossible");
//      }
//
//      @Override
//      public JsonObject serializeToJson(SerializationContext serializationContext) {
//        return null;
//      }
//    }));
//
//    ArrayList<String[]> fixedRequirements = new ArrayList<>();
//
//    fixedRequirements.add(new String[]{"for_free"});
//
//    advRequirements = Arrays.stream(fixedRequirements.toArray()).toArray(String[][]::new);
//
//    DisplayInfo display = new DisplayInfo(CraftItemStack.asNMSCopy(item),
//        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(title))),
//        Component.Serializer.fromJson(JSONUtils.formatSimpleJSON(TextUtils.colorText(description))),
//        new ResourceLocation(AdvancementBackground.SNOW.getValue()),
//        FrameType.CHALLENGE, true, true, true);
//    Advancement advancement = new Advancement(notName, null, display, advRewards, advCriteria, advRequirements);
//
//    HashMap<MinecraftKey, AdvancementProgress> prg = new HashMap<>();
//
//    AdvancementProgress advPrg = new AdvancementProgress();
//    advPrg.getCriterion("for_free").getObtained();
//    prg.put(MinecraftKey.a(notName.getNamespace()), advPrg);
//
//    PacketPlayOutAdvancements packet = new PacketPlayOutAdvancements(false, List.of(advancement), new HashSet<>(), prg);
//    ((CraftPlayer) player).getHandle().connection.send(packet);
//  }
//}
