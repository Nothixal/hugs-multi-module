package me.nothixal.hugs.utils.inventory;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public class ButtonSet {

  private List<ItemStack> buttons;

  public ButtonSet(List<ItemStack> buttons) {
    this.buttons = buttons;
  }

  public List<ItemStack> getButtons() {
    return buttons;
  }

  public ItemStack toggle(ItemStack item) {
    return getNext(item);
  }

  private ItemStack getNext(ItemStack element) {
    int index = buttons.indexOf(element);
    if (index < 0 || index + 1 == buttons.size()) {
      return buttons.get(0);
    }
    return buttons.get(index + 1);
  }
}
