package me.nothixal.hugs.utils;

import me.nothixal.hugs.utils.inventory.ButtonSet;
import me.nothixal.hugs.utils.inventory.IconSet;

public class ItemPair {

  private IconSet iconSet;
  private ButtonSet button;

  public ItemPair(IconSet iconSet, ButtonSet button) {
    this.iconSet = iconSet;
    this.button = button;
  }

  public IconSet getIconSet() {
    return iconSet;
  }

  public ButtonSet getButtonSet() {
    return button;
  }
}
