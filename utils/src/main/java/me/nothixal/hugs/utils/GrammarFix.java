package me.nothixal.hugs.utils;

public class GrammarFix {

  public static String getPlural(String word, Boolean bool) {
    if (bool) {
      return word;
    } else {
      return word.concat("s");
    }
  }

}
