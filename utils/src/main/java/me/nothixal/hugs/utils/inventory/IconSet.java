package me.nothixal.hugs.utils.inventory;

import java.util.List;
import org.bukkit.inventory.ItemStack;

public class IconSet {

  private List<ItemStack> icons;

  public IconSet(List<ItemStack> icons) {
    this.icons = icons;
  }

  public List<ItemStack> getIcons() {
    return icons;
  }

  public ItemStack toggle(ItemStack item) {
    return getNext(item);
  }

  private ItemStack getNext(ItemStack element) {
    int index = icons.indexOf(element);
    if (index < 0 || index + 1 == icons.size()) {
      return icons.get(0);
    }
    return icons.get(index + 1);
  }

}
