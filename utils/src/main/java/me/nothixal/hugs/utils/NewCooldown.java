package me.nothixal.hugs.utils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.UUID;
import org.bukkit.entity.Player;

public class NewCooldown {

  private static ListMultimap<UUID, NewCooldown> cooldowns = ArrayListMultimap.create();

  private final UUID id;
  private final String cooldownName;
  private final int timeInSeconds;

  public NewCooldown(Player player, String cooldownName, int timeInSeconds) {
    this.id = player.getUniqueId();
    this.cooldownName = cooldownName;
    this.timeInSeconds = timeInSeconds;

    cooldowns.put(id, this);
  }


}
