package me.nothixal.hugs.utils.text;

import java.util.regex.Pattern;
import me.nothixal.hugs.enums.ANSIColor;
import org.bukkit.ChatColor;

public class TextUtils {

  private static final Pattern hex = Pattern.compile("#[a-fA-F0-9]{6}");

  public static String colorText(String msg) {
//    Matcher matcher = hex.matcher(msg);
//
//    while (matcher.find()) {
//      String color = msg.substring(matcher.start(), matcher.end());
//
//      System.out.println(color);
//
//      msg = msg.replace(color, "" + net.md_5.bungee.api.ChatColor.of(color));
//    }
//
//    System.out.println(msg);

    return ChatColor.translateAlternateColorCodes('&', msg);
  }

  public static String stripColor(String message) {
    return ChatColor.stripColor(message).replaceAll("(?i)&([a-f0-9])", "");
  }

  /**
   * Converts the color codes into ANSI color codes.
   * @param msg The message with color codes to convert.
   * */
  public static String translateANSIColorCodes(String msg) {
    return ANSIColor.translateColorCodes('&', msg);
  }

  /**
   * Converts the color codes into ANSI color codes.
   * @param character The character that will be looked for in the conversion.
   * @param msg The message with color codes to convert.
   * */
  public static String translateANSIColorCodes(char character, String msg) {
    return ANSIColor.translateColorCodes(character, msg);
  }
}
