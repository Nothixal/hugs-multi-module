package me.nothixal.hugs.utils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import java.util.UUID;

public class ImprovedCooldown {

  private static ListMultimap<UUID, ImprovedCooldown> cooldowns = ArrayListMultimap.create();

  private long start;

  private final UUID id;
  private final String cooldownName;
  private final int timeInSeconds;

  public ImprovedCooldown(UUID uuid, String cooldownName, int timeInSeconds) {
    this.id = uuid;
    this.cooldownName = cooldownName;
    this.timeInSeconds = timeInSeconds;
    this.start = System.currentTimeMillis();

    cooldowns.put(id, this);
  }

  private static ImprovedCooldown getCooldown(UUID uuid, String cooldownName) {
    for (ImprovedCooldown cooldown : cooldowns.get(uuid)) {
      if (cooldown.cooldownName.equals(cooldownName)) {
        return cooldown;
      }
    }

    return null;
  }

  public static boolean hasCooldown(UUID uuid, String cooldownName) {
    for (ImprovedCooldown cooldown : cooldowns.get(uuid)) {
      // Check to see if the inputted name matches the cooldown's name.
      // If it doesn't, it means that they put an invalid cooldown name in their check.
      if (!cooldown.cooldownName.equals(cooldownName)) {
        return false;
      }

      // If the cooldown still has time left, return true. Else, remove the cooldown from the player.
      if (getTimeLeft(uuid, cooldownName) > 0) {
        return true;
      } else {
        cooldowns.get(uuid).remove(cooldown);
        return false;
      }
    }

    return false;
  }

  private static int getTimeLeft(UUID uuid, String cooldownName) {
    ImprovedCooldown cooldown = getCooldown(uuid, cooldownName);
    int f = -1;
    if (cooldown != null) {
      long now = System.currentTimeMillis();
      long cooldownTime = cooldown.start;
      int totalTime = cooldown.timeInSeconds;
      int r = (int) (now - cooldownTime) / 1000;
      f = (r - totalTime) * (-1);
    }
    return f;
  }

  public static String getExactTimeLeft(UUID uuid, String cooldownName) {
    ImprovedCooldown cooldown = getCooldown(uuid, cooldownName);

    if (cooldown == null) {
      return null;
    }

    long timeSinceCreation = System.currentTimeMillis() - cooldown.start;
    int timeLeft = cooldown.timeInSeconds * 10 - (int) (timeSinceCreation / 100);
    String secondsLeft = "" + timeLeft;

    if (timeLeft >= 10) {
      secondsLeft = secondsLeft.substring(0, secondsLeft.length() - 1) + "." + secondsLeft.substring(secondsLeft.length() - 1);
      return secondsLeft;
    }

    if (timeLeft > 0) {
      secondsLeft = secondsLeft.substring(0, secondsLeft.length() - 1) + "0." + secondsLeft.substring(secondsLeft.length() - 1);
      return secondsLeft;
    }

    return null;
  }

  public static String getRoundedTimeLeft(UUID uuid, String cooldownName) {
    ImprovedCooldown cooldown = getCooldown(uuid, cooldownName);

    if (cooldown == null) {
      return null;
    }

    long secondsLeft = ((cooldown.start / 1000) + cooldown.timeInSeconds) - (System.currentTimeMillis() / 1000);
    if (secondsLeft >= 0) {
      return String.valueOf(secondsLeft);
    }
    return null;
  }

  public void printValues() {
    System.out.println(start);
    System.out.println(id);
    System.out.println(cooldownName);
    System.out.println(timeInSeconds);
  }

}
