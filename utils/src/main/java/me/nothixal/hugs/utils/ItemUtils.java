package me.nothixal.hugs.utils;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public final class ItemUtils {

  private ItemUtils() { }

  public static List<String> updateLore(List<String> lore) {
    List<String> newLore = new ArrayList<>();

    for (String s : lore) {
      newLore.add(ChatUtils.colorChat(s));
    }

    return newLore;
  }

  public static ItemStack hideAttributes(ItemStack item) {
    ItemMeta meta = item.getItemMeta();
    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
    meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
    item.setItemMeta(meta);
    return item;
  }

}
