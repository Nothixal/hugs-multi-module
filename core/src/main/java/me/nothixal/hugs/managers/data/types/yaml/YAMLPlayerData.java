package me.nothixal.hugs.managers.data.types.yaml;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YAMLPlayerData {

  private final UUID uuid;
  private File playerFile;
  private FileConfiguration playerConfig;
  private boolean hasBeenModified = false;

  public YAMLPlayerData(HugsPlugin plugin, UUID uuid) {
    this.uuid = uuid;
    this.playerFile = new File(plugin.getFileManager().getPlayerDataDirectory(), uuid + ".yml");
    this.playerConfig = YamlConfiguration.loadConfiguration(playerFile);
    prerequisiteCheck();
  }

  private void prerequisiteCheck() {
    if (playerFileExists()) {
      return;
    }

    // This is to prevent unnecessary files from being created.
    // This help prevent the leaderboard GUI from fucking up.
    if (!Bukkit.getOfflinePlayer(uuid).hasPlayedBefore()) {
      return;
    }

    createPlayerFile();
    createDefaultSections();
  }

  private void createDefaultSections() {
    if (playerConfig.getConfigurationSection("uuid") == null) {
      playerConfig.set("uuid", uuid.toString());
    }

    if (playerConfig.getConfigurationSection("name") == null) {
      playerConfig.set("name", Bukkit.getOfflinePlayer(uuid).getName());
    }

    if (playerConfig.getConfigurationSection("self_hugs") == null) {
      playerConfig.set("self_hugs", 0);
    }

    if (playerConfig.getConfigurationSection("normal_hugs") == null) {
      playerConfig.createSection("normal_hugs");
    }

    if (playerConfig.getConfigurationSection("normal_hugs.given") == null) {
      playerConfig.set("normal_hugs.given", 0);
    }

    if (playerConfig.getConfigurationSection("normal_hugs.received") == null) {
      playerConfig.set("normal_hugs.received", 0);
    }

    if (playerConfig.getConfigurationSection("mass_hugs") == null) {
      playerConfig.createSection("mass_hugs");
    }

    if (playerConfig.getConfigurationSection("mass_hugs.given") == null) {
      playerConfig.set("mass_hugs.given", 0);
    }

    if (playerConfig.getConfigurationSection("mass_hugs.received") == null) {
      playerConfig.set("mass_hugs.received", 0);
    }

    if (playerConfig.getConfigurationSection("settings") == null) {
      playerConfig.createSection("settings");
    }

    if (playerConfig.getConfigurationSection("settings.huggable") == null) {
      playerConfig.createSection("settings.huggable");
      playerConfig.set("settings.huggable", true);
    }

    if (playerConfig.getConfigurationSection("settings.sounds") == null) {
      playerConfig.createSection("settings.sounds");
      playerConfig.set("settings.sounds", true);
    }

    if (playerConfig.getConfigurationSection("settings.particles") == null) {
      playerConfig.createSection("settings.particles");
      //playerConfig.set("settings.particles", "low");
      playerConfig.set("settings.particles", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators") == null) {
      playerConfig.createSection("settings.indicators");
    }

    if (playerConfig.getConfigurationSection("settings.indicators.chat") == null) {
      playerConfig.set("settings.indicators.chat", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.actionbar") == null) {
      playerConfig.set("settings.indicators.actionbar", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.bossbar") == null) {
      playerConfig.set("settings.indicators.bossbar", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.titles") == null) {
      playerConfig.set("settings.indicators.titles", true);
    }

    if (playerConfig.getConfigurationSection("settings.indicators.toast") == null) {
      playerConfig.set("settings.indicators.toast", true);
    }

    if (playerConfig.getConfigurationSection("last_hug") == null) {
      playerConfig.createSection("last_hug");
    }

    if (playerConfig.getConfigurationSection("last_hug.given") == null) {
      playerConfig.createSection("last_hug.given");
    }

    if (playerConfig.getConfigurationSection("last_hug.given.receiver") == null) {
      playerConfig.set("last_hug.given.receiver", "None");
    }

    if (playerConfig.getConfigurationSection("last_hug.given.timestamp") == null) {
      playerConfig.set("last_hug.given.timestamp", 0);
    }

    if (playerConfig.getConfigurationSection("last_hug.received") == null) {
      playerConfig.createSection("last_hug.received");
    }

    if (playerConfig.getConfigurationSection("last_hug.received.sender") == null) {
      playerConfig.set("last_hug.received.sender", "None");
    }

    if (playerConfig.getConfigurationSection("last_hug.received.timestamp") == null) {
      playerConfig.set("last_hug.received.timestamp", 0);
    }

    savePlayerFile();
  }

  public void saveDataFile() {
    prerequisiteCheck();

    try {
      playerConfig.save(playerFile);
      hasBeenModified = false;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void savePlayerFile() {
    try {
      playerConfig.save(playerFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public boolean playerFileExists() {
    return playerFile.exists();
  }

  private void createPlayerFile() {
    boolean fileCreated = playerFileExists();

    if (!fileCreated) {
      try {
        fileCreated = playerFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (!fileCreated) {
      LogUtils.logError("An error occurred while creating " + Bukkit.getPlayer(uuid).getName() + "'s data file.");
    }
  }

  public boolean deletePlayerFile() {
    return playerFile.delete();
  }

  public File getPlayerFile() {
    return playerFile;
  }

  public FileConfiguration getPlayerConfig() {
    return playerConfig;
  }

  // hasBeenModified getter and setter.
  public boolean hasBeenModified() {
    return hasBeenModified;
  }

  public void setHasBeenModified(boolean hasBeenModified) {
    this.hasBeenModified = hasBeenModified;
  }
}
