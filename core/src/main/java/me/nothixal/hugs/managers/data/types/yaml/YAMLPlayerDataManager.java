package me.nothixal.hugs.managers.data.types.yaml;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.ConfigValues;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;
import org.bukkit.configuration.file.FileConfiguration;

public class YAMLPlayerDataManager implements PlayerDataManager {

  private final HugsPlugin plugin;

  private Map<UUID, YAMLPlayerData> dataCache = new HashMap<>();

  public YAMLPlayerDataManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @Override
  public boolean playerExists(UUID uuid) {
    return getPlayerData(uuid).playerFileExists();
  }

  @Override
  public void createPlayer(UUID uuid) {
    dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k));
  }

  @Override
  public void deletePlayer(UUID uuid) {
    dataCache.get(uuid).deletePlayerFile();
    dataCache.remove(uuid);
  }

  @Override
  public void loadDatabase() {
    File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

    File[] files = playerDataDirectory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

      YAMLPlayerData playerData = new YAMLPlayerData(plugin, uuid);

      dataCache.put(uuid, playerData);
    }
  }

  @Override
  public void reloadDatabase() {
    dataCache.clear();
    loadDatabase();
  }

  @Override
  public void saveDatabase() {
    for (YAMLPlayerData playerData : dataCache.values()) {
      if (!playerData.hasBeenModified()) {
        continue;
      }

      playerData.saveDataFile();
    }
  }

  @Override
  public void purgeDatabase() {
    dataCache.clear();
    plugin.getFileManager().deleteDirectory(new File(plugin.getFileManager().getPlayerDataDirectory()));
    plugin.getFileManager().createDirectory(new File(plugin.getFileManager().getPlayerDataDirectory()));
  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {
    dataCache.get(uuid).saveDataFile();
    dataCache.remove(uuid);
  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {
    dataCache.get(uuid).deletePlayerFile();
    dataCache.remove(uuid);
  }

  @Override
  public int getSelfHugs(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.SELF_HUGS.getPath());
  }

  @Override
  public void setSelfHugs(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementSelfHugs(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), getSelfHugs(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.SELF_HUGS.getPath(), getSelfHugs(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getHugsGiven(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.HUGS_GIVEN.getPath());
  }

  @Override
  public void setHugsGiven(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsGiven(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), getHugsGiven(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_GIVEN.getPath(), getHugsGiven(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getHugsReceived(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.HUGS_RECEIVED.getPath());
  }

  @Override
  public void setHugsReceived(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsReceived(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), getHugsReceived(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.HUGS_RECEIVED.getPath(), getHugsReceived(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.MASS_HUGS_GIVEN.getPath());
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), getMassHugsGiven(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_GIVEN.getPath(), getMassHugsGiven(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    return getPlayerConfig(uuid).getInt(ConfigValues.MASS_HUGS_RECEIVED.getPath());
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int value) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), getMassHugsReceived(uuid) + 1);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {
    getPlayerConfig(uuid).set(ConfigValues.MASS_HUGS_RECEIVED.getPath(), getMassHugsReceived(uuid) + amount);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public int getTotalSelfHugsGiven() {
    return plugin.getDataFile().getInt("total_self_hugs_given");
  }

  @Override
  public int getTotalHugsGiven() {
    return plugin.getDataFile().getInt("total_hugs_given");
  }

  @Override
  public int getTotalMassHugsGiven() {
    return plugin.getDataFile().getInt("total_mass_hugs_given");
  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean(ConfigValues.WANTS_HUGS.getPath());
  }

  @Override
  public void setWantsHugs(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set(ConfigValues.WANTS_HUGS.getPath(), bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean("settings.particles");
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set("settings.particles", bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public String getParticleValue(UUID uuid) {
    return getPlayerConfig(uuid).getString("settings.particles");
  }

  @Override
  public void setParticleValue(UUID uuid, String value) {
    getPlayerConfig(uuid).set("settings.particles", value);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean wantsSounds(UUID uuid) {
    return getPlayerConfig(uuid).getBoolean(ConfigValues.WANTS_SOUNDS.getPath());
  }

  @Override
  public void setWantsSounds(UUID uuid, boolean bool) {
    getPlayerConfig(uuid).set(ConfigValues.WANTS_SOUNDS.getPath(), bool);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    return getHugsGiven(uuid) != 0 || getMassHugsGiven(uuid) != 0;
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return getHugsReceived(uuid) != 0 || getMassHugsReceived(uuid) != 0;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    List<Indicator> indicatorsList = new ArrayList<>();

    indicatorsList.add(new Indicator(1, "Chat", getPlayerConfig(uuid).getBoolean("settings.indicators.chat")));
    indicatorsList.add(new Indicator(2, "Titles", getPlayerConfig(uuid).getBoolean("settings.indicators.titles")));
    indicatorsList.add(new Indicator(3, "Actionbar", getPlayerConfig(uuid).getBoolean("settings.indicators.actionbar")));
    indicatorsList.add(new Indicator(4, "Bossbar", getPlayerConfig(uuid).getBoolean("settings.indicators.bossbar")));
    indicatorsList.add(new Indicator(5, "Toast", getPlayerConfig(uuid).getBoolean("settings.indicators.toast")));

    Map<String, Boolean> indicators = new HashMap<>();

    indicators.put("Chat", getPlayerConfig(uuid).getBoolean("settings.indicators.chat"));
    indicators.put("Actionbar", getPlayerConfig(uuid).getBoolean("settings.indicators.actionbar"));
    indicators.put("Bossbar", getPlayerConfig(uuid).getBoolean("settings.indicators.bossbar"));
    indicators.put("Titles", getPlayerConfig(uuid).getBoolean("settings.indicators.titles"));
    indicators.put("Toast", getPlayerConfig(uuid).getBoolean("settings.indicators.toast"));

    return indicatorsList;
  }

  @Override
  public boolean getIndicator(UUID uuid, String type) {
    return getPlayerConfig(uuid).getBoolean("settings.indicators." + type);
  }

  @Override
  public void setIndicator(UUID uuid, String type, boolean enabled) {
    getPlayerConfig(uuid).set("settings.indicators." + type, enabled);
    getPlayerData(uuid).setHasBeenModified(true);
  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.YAML;
  }

  private void test() {
    List<UUID> players = new ArrayList<>();

    File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

    File[] files = playerDataDirectory.listFiles();

    if (files == null) {
      return;
    }

    for (File file : files) {
      if (file.isDirectory()) {
        continue;
      }

      if (!file.getName().endsWith(".yml")) {
        continue;
      }

      UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));
      players.add(uuid);
    }

    Collections.sort(players);

    for (int i = 0; i < 5; i++) {

    }
  }

  private YAMLPlayerData getPlayerData(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k));
  }

  private FileConfiguration getPlayerConfig(UUID uuid) {
    return dataCache.computeIfAbsent(uuid, k -> new YAMLPlayerData(plugin, k)).getPlayerConfig();
  }

}
