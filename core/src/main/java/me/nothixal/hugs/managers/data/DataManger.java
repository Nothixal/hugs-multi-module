package me.nothixal.hugs.managers.data;

import java.util.Map;
import java.util.UUID;

public interface DataManger {

  /**
   * Get the total amount of self hugs given by all players.
   * */
  int getTotalSelfHugsGiven();

  /**
   * Get the total amount of hugs given by all players.
   * */
  int getTotalHugsGiven();

  /**
   * Get the total amount of hugs received by all players.
   * */
  int getTotalHugsReceived();

  /**
   * Get the total amount of mass hugs given by all players.
   * */
  int getTotalMassHugsGiven();

  /**
   * Get the total amount of mass hugs received by all players.
   * */
  int getTotalMassHugsReceived();

  Map<UUID, Integer> getTopHugsGiven();

  Map<UUID, Integer> getTopHugsGiven(int limit);


  Map<UUID, Integer> getTopHugsReceived();

  Map<UUID, Integer> getTopHugsReceived(int limit);


  Map<UUID, Integer> getTopMassHugsGiven();

  Map<UUID, Integer> getTopMassHugsGiven(int limit);


  Map<UUID, Integer> getTopMassHugsReceived();

  Map<UUID, Integer> getTopMassHugsReceived(int limit);

}
