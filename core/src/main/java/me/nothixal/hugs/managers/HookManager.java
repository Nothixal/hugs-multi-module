package me.nothixal.hugs.managers;

import java.util.ArrayList;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.hooks.PlaceholderAPIHook;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;

public class HookManager {

  private final HugsPlugin plugin;
  private List<String> hooks = new ArrayList<>();

  public HookManager(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  public void registerHooks() {
    if (isPluginEnabled("PlaceholderAPI")) {
      if (new PlaceholderAPIHook(plugin).register()) {
        hooks.add("PlaceholderAPI");
      }
    }

//    if (isPluginEnabled("WorldGuard")) {
//      hooks.add("WorldGuard");
//    }
  }

  public void printHooks() {
    if (hooks.size() == 0) {
      return;
    }

    LogUtils.logInfo("-=[ Hooks ]=-");
    LogUtils.logInfo("Hooked into " + hooks.size() + " " + (hooks.size() == 1 ? "plugin" : "plugins") + ".");

    if (hooks.size() < 1) {
      LogUtils.logInfo(" ");
      return;
    }

    LogUtils.logInfo(" ");

    for (String s : hooks) {
      LogUtils.logInfo("- " + s);
    }

    //LogUtils.logInfo(" ");
  }

  private boolean isPluginEnabled(String pluginName) {
    return Bukkit.getPluginManager().isPluginEnabled(pluginName);
  }
}