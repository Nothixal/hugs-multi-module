package me.nothixal.hugs.managers.files;

import com.google.common.base.Charsets;
import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginAwareness;

public class FileManager {

  private final HugsPlugin plugin;

  private final File homeDirectory;
  private final String dataDirectory;
  private final String playerDataDirectory;
  private final String langDirectory;
  private final String logsDirectory;

  // Necessary Files needed to load plugin.
  private File configYMLFile;
  private FileConfiguration configYMLData;

  private File dataYMLFile;
  public FileConfiguration dataYMLData;

  private File langYMLFile;
  public FileConfiguration langYMLData;

  private File updaterYMLFile;
  public FileConfiguration updaterYMLData;

  private File startupYMLFile;
  public FileConfiguration startupYMLData;

  private File defaultsYMLFile;
  public FileConfiguration defaultsYMLData;

  // Files for extended plugin functionality.
  private File overridesYMLFile;
  public FileConfiguration overridesYMLData;

  private File metricsYMLFile;
  public FileConfiguration metricsYMLData;

  private File mysqlYMLFile;
  public FileConfiguration mysqlFileData;


  private Map<String, File> missingDirectories = new HashMap<>();
  private List<String> createdDirectories = new ArrayList<>();
  private List<String> uncreatedDirectories = new ArrayList<>();

  private Map<String, File> missingFiles = new HashMap<>();
  private List<String> createdFiles = new ArrayList<>();
  private List<String> uncreatedFiles = new ArrayList<>();

  private Map<File, FileConfiguration> loadedFiles = new HashMap<>();

  private List<String> necessaryDirectories = new ArrayList<>();

  public FileManager(HugsPlugin plugin) {
    this.plugin = plugin;
    this.homeDirectory = plugin.getDataFolder();
    this.dataDirectory = homeDirectory.getAbsolutePath() + File.separator + "data";
    this.playerDataDirectory = dataDirectory + File.separator + "playerdata";
    this.langDirectory = homeDirectory + File.separator + "lang";
    this.logsDirectory = homeDirectory + File.separator + "logs";
  }

  public File getHomeDirectory() {
    return homeDirectory;
  }

  public String getDataDirectory() {
    return dataDirectory;
  }

  public String getPlayerDataDirectory() {
    return playerDataDirectory;
  }

  public String getLangDirectory() {
    return langDirectory;
  }

  public String getLogsDirectory() {
    return logsDirectory;
  }

  public void checkForAndCreateFiles() {
    if (!createDirectories()) {
      LogUtils.logError("An error occurred whilst creating the directories.");
      LogUtils.logError("Disabling Plugin");
      Bukkit.getPluginManager().disablePlugin(plugin);
      return;
    }

    if (!createFiles()) {
      LogUtils.logError("An error occurred whilst creating the files.");
      LogUtils.logError("Disabling Plugin");
      Bukkit.getPluginManager().disablePlugin(plugin);
      return;
    }

    loadFiles();
  }

  private void checkForMissingDirectories() {
    File homeDir = homeDirectory;
    File data = new File(dataDirectory);
    File playerData = new File(playerDataDirectory);
    File lang = new File(langDirectory);
    File logs = new File(logsDirectory);

    if (!directoryExists(homeDir)) {
      missingDirectories.put(homeDir.getName(), homeDir);
    }

    if (!directoryExists(lang)) {
      missingDirectories.put("Lang", lang);
    }

    if (!directoryExists(data)) {
      missingDirectories.put("Data", data);
    }

    if (!directoryExists(playerData)) {
      missingDirectories.put("Player Data", playerData);
    }

    if (!directoryExists(logs)) {
      missingDirectories.put("Logs", logs);
    }
  }

  private boolean createDirectories() {
    checkForMissingDirectories();

    LogUtils.logInfo("-={ Files }=-");

    if (missingDirectories.size() > 0) {
      List<String> missingDirectoriesKeys = new ArrayList<>(missingDirectories.keySet());

      String unaccountedDirectories = getListAsSortedString(missingDirectoriesKeys, "&e");

      LogUtils.logInfo("Missing Directories: " + unaccountedDirectories);
      LogUtils.logInfo("Attempting Creation...");

      for (Map.Entry<String, File> entry : missingDirectories.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createDirectory(entry.getValue())) {
          createdDirectories.add(entry.getKey());
        } else {
          uncreatedDirectories.add(entry.getKey());
        }
      }

      if (uncreatedDirectories.size() > 0) {
        boolean check = true;

        String uncreatedDirs = getListAsSortedString(uncreatedDirectories, "&c");

        LogUtils.logInfo("Missing Directories: " + uncreatedDirs);

        if (uncreatedDirs.contains(homeDirectory.getName())) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(uncreatedDirs + " directories failed to generate. Disabling plugin.");
          return false;
        }

        return true;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing directories were created successfully.");
      LogUtils.logInfo(" ");
      return true;
    }

    LogUtils.logInfo("Missing Directories: &aNone");
    return true;
  }

  private void checkForMissingFiles() {
    this.configYMLFile = new File(homeDirectory, "config.yml");
    this.dataYMLFile = new File(dataDirectory, "data.yml");
    this.langYMLFile = new File(langDirectory, "lang_en.yml");
    this.startupYMLFile = new File(homeDirectory, "startup.yml");
    this.updaterYMLFile = new File(homeDirectory, "updater.yml");

    this.overridesYMLFile = new File(homeDirectory, "overrides.yml");
    this.defaultsYMLFile = new File(homeDirectory, "defaults.yml");

    this.mysqlYMLFile = new File(dataDirectory, "mysql.yml");

    if (!fileExists(configYMLFile)) {
      missingFiles.put("config.yml", configYMLFile);
      this.plugin.saveDefaultConfig();
    }

    if (!fileExists(dataYMLFile)) {
      missingFiles.put("data.yml", dataYMLFile);
      this.plugin.saveResource("data/data.yml", true);
      this.plugin.reloadData();
    }

    if (!fileExists(langYMLFile)) {
      missingFiles.put("lang_en.yml", langYMLFile);
      this.plugin.saveResource("lang/lang_en.yml", false);
      this.reloadMessageConfig();
    }

    if (!fileExists(startupYMLFile)) {
      missingFiles.put("startup.yml", startupYMLFile);
      this.plugin.saveResource("startup.yml", false);
    }

    if (!fileExists(updaterYMLFile)) {
      missingFiles.put("updater.yml", updaterYMLFile);
      this.plugin.saveResource("updater.yml",false);
    }

    if (!fileExists(overridesYMLFile)) {
      missingFiles.put("overrides.yml", overridesYMLFile);
      this.plugin.saveResource("overrides.yml",false);
    }

    if (!fileExists(defaultsYMLFile)) {
      missingFiles.put("defaults.yml", defaultsYMLFile);
      this.plugin.saveResource("defaults.yml",false);
    }

    if (!fileExists(mysqlYMLFile)) {
      missingFiles.put("mysql.yml", mysqlYMLFile);
      this.plugin.saveResource("data/mysql.yml",false);
    }
  }

  private boolean createFiles() {
    checkForMissingFiles();

    if (missingFiles.size() > 0) {
      List<String> missingFilesKeys = new ArrayList<>(missingFiles.keySet());

      String unaccountedFiles = getListAsSortedString(missingFilesKeys, "&e");

      LogUtils.logInfo("Missing Files: " + unaccountedFiles);
      LogUtils.logInfo("Attempting Creation...");

      for (Map.Entry<String, File> entry : missingFiles.entrySet()) {
        if (entry.getValue().exists()) {
          continue;
        }

        if (createFile(entry.getValue())) {
          createdFiles.add(entry.getKey());
        } else {
          uncreatedFiles.add(entry.getKey());
        }
      }

      if (uncreatedFiles.size() > 0) {
        boolean check = true;

        String test = getListAsSortedString(uncreatedFiles, "&c");

        LogUtils.logInfo("Missing Files: " + test);

        if (test.contains("config.yml")) {
          check = false;
        }

        if (!check) {
          LogUtils.logError(test + " files failed to generate. Disabling plugin.");
          return false;
        }

        return true;
      }

      LogUtils.logInfo("&aSUCCESS: &7All missing files were created successfully.");
      LogUtils.logInfo(" ");
      return true;
    }

    LogUtils.logInfo("Missing Files: &aNone");
    LogUtils.logInfo(" ");
    return true;
  }

  private void loadFiles() {
    configYMLData = YamlConfiguration.loadConfiguration(configYMLFile);
    loadedFiles.put(configYMLFile, configYMLData);

    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
    loadedFiles.put(dataYMLFile, dataYMLData);

    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);
    loadedFiles.put(langYMLFile, langYMLData);

    startupYMLData = YamlConfiguration.loadConfiguration(startupYMLFile);
    loadedFiles.put(startupYMLFile, startupYMLData);

    updaterYMLData = YamlConfiguration.loadConfiguration(updaterYMLFile);
    loadedFiles.put(updaterYMLFile, updaterYMLData);

    overridesYMLData = YamlConfiguration.loadConfiguration(overridesYMLFile);
    loadedFiles.put(overridesYMLFile, overridesYMLData);

    defaultsYMLData = YamlConfiguration.loadConfiguration(defaultsYMLFile);
    loadedFiles.put(defaultsYMLFile, defaultsYMLData);

    mysqlFileData = YamlConfiguration.loadConfiguration(mysqlYMLFile);
    loadedFiles.put(mysqlYMLFile, mysqlFileData);
  }

  public void loadMySQLFile() {
    mysqlFileData = YamlConfiguration.loadConfiguration(mysqlYMLFile);
  }

  private void reloadMessageConfig() {
    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);

    final InputStream defConfigStream = plugin.getResource("lang/lang_en.yml");
    if (defConfigStream == null) {
      return;
    }

    final YamlConfiguration defConfig;
    if (isStrictlyUTF8() /*|| FileConfiguration.UTF8_OVERRIDE */) {
      defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, Charsets.UTF_8));
    } else {
      final byte[] contents;
      defConfig = new YamlConfiguration();
      try {
        contents = ByteStreams.toByteArray(defConfigStream);
      } catch (final IOException e) {
        LogUtils.logError("Unexpected failure reading lang_en.yml");
        return;
      }

      final String text = new String(contents, Charset.defaultCharset());
      if (!text.equals(new String(contents, Charsets.UTF_8))) {
        LogUtils.logWarning("Default system encoding may have misread lang_en.yml from plugin jar");
      }

      try {
        defConfig.loadFromString(text);
      } catch (final InvalidConfigurationException e) {
        LogUtils.logError("Cannot load configuration from jar");
      }
    }

    langYMLData.setDefaults(defConfig);
  }

  @SuppressWarnings("deprecation")
  private boolean isStrictlyUTF8() {
    return plugin.getDescription().getAwareness().contains(PluginAwareness.Flags.UTF8);
  }

  private String getListAsSortedString(List<String> list, String colorCode) {
    StringBuilder stringBuilder = new StringBuilder();
    // Looping through the list.
    for (int i = 0; i < list.size(); i++) {
      //append the value into the builder
      stringBuilder.append(colorCode + list.get(i));

      // If the value is not the last element of the list then append a comma(,).
      if (i != list.size() - 1) {
        stringBuilder.append("&7, ");
      }
    }
    return stringBuilder.toString();
  }

  private void createLangFile() {
    langYMLFile = new File(getLangDirectory(), "lang_en.yml");
    if (!langYMLFile.exists()) {
      langYMLFile.getParentFile().mkdirs();
      plugin.saveResource("lang_en.yml", false);
    }

    langYMLData = new YamlConfiguration();
    try {
      langYMLData.load(langYMLFile);
    } catch (IOException | InvalidConfigurationException e) {
      e.printStackTrace();
    }
  }

  private boolean fileExists(File file) {
    return directoryExists(file);
  }

  private boolean directoryCreated(File file) {
    boolean isDirectoryCreated = directoryExists(file);

    if (!isDirectoryCreated) {
      isDirectoryCreated = file.mkdirs();
    }

    return isDirectoryCreated;
  }

  private boolean createFile(File file) {
    boolean fileCreated = file.exists();

    if (!fileCreated) {
      try {
        fileCreated = file.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    if (!fileCreated) {
      LogUtils.logError("An error occurred while creating the player's data.");
      return false;
    }

    return true;
  }

  public boolean createDirectory(File file) {
    boolean isDirectoryCreated = directoryExists(file);

    if (!isDirectoryCreated) {
      isDirectoryCreated = file.mkdirs();
    }

    return isDirectoryCreated;
  }

  public boolean deleteDirectory(File file) {
    if (!file.isDirectory()) {
      return false;
    }

    return file.delete();
  }

  private boolean directoryExists(File file) {
    return file.exists();
  }

  public File getConfigFile() {
    return configYMLFile;
  }

  public FileConfiguration getConfig() {
    return loadedFiles.get(configYMLFile);
  }

  public void reloadConfig() {
    configYMLData = YamlConfiguration.loadConfiguration(configYMLFile);

    InputStream defConfigStream = plugin.getResource("config.yml");
    if (defConfigStream != null) {
      configYMLData.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream, Charsets.UTF_8)));
    }

    loadedFiles.put(configYMLFile, configYMLData);
  }

  public File getDataFile() {
    return dataYMLFile;
  }

  public FileConfiguration getData() {
    return dataYMLData;
  }

  public void reloadData() {
    dataYMLData = YamlConfiguration.loadConfiguration(dataYMLFile);
  }

  public File getLangFile() {
    return langYMLFile;
  }

  public FileConfiguration getLang() {
    return langYMLData;
  }

  public File getStartupFile() {
    return startupYMLFile;
  }

  public FileConfiguration getStartupData() {
    return startupYMLData;
  }

  public File getUpdaterFile() {
    return updaterYMLFile;
  }

  public FileConfiguration getUpdaterData() {
    return updaterYMLData;
  }

  public File getOverridesYMLFile() {
    return overridesYMLFile;
  }

  public FileConfiguration getOverridesYMLData() {
    return overridesYMLData;
  }

  public File getDefaultsYMLFile() {
    return defaultsYMLFile;
  }

  public FileConfiguration getDefaultsYMLData() {
    return defaultsYMLData;
  }

  public File getMysqlYMLFile() {
    return mysqlYMLFile;
  }

  public FileConfiguration getMysqlFileData() {
    return mysqlFileData;
  }

  public void reloadLang() {
    langYMLData = YamlConfiguration.loadConfiguration(langYMLFile);
  }

  public void reloadYMLFile(File file, FileConfiguration fileConfiguration, String pathToResource) {
    fileConfiguration = YamlConfiguration.loadConfiguration(file);

    InputStream inputStream = plugin.getResource(pathToResource);
    if (inputStream != null) {
      fileConfiguration.setDefaults(YamlConfiguration.loadConfiguration(new InputStreamReader(inputStream, Charsets.UTF_8)));
    }
  }

  //  private File playerYMLFile;
//  private FileConfiguration playerConfigurationFile;
//
//  private File getPlayerYMLFile(OfflinePlayer p, HugPlugin plugin) {
//    playerYMLFile = new File(
//        plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data",
//        p.getUniqueId().toString() + ".yml");
//    return playerYMLFile;
//  }
//
//  public FileConfiguration getPlayerYMLFile(OfflinePlayer p) {
//    playerConfigurationFile = YamlConfiguration.loadConfiguration(getPlayerYMLFile(p, plugin));
//    return playerConfigurationFile;
//  }
//
//  public void savePlayerDataFile(OfflinePlayer p) {
//    try {
//      playerConfigurationFile.savePlayerDataFile(getPlayerYMLFile(p, plugin));
//    } catch (IOException ex) {
//      ex.printStackTrace();
//    }
//  }
//
//  public void reloadPlayerData() {
//    try {
//      playerConfigurationFile = YamlConfiguration.loadConfiguration(playerYMLFile);
//    } catch (Exception ex) {
//      LogUtils.logError("[hugs] An error occurred whilst loading the player data file!");
//      LogUtils.logError("[hugs] Printing stack trace:");
//      ex.printStackTrace();
//      LogUtils.logError("[hugs] End of stack trace.");
//    }
//  }

//  public void createPlayerFile(OfflinePlayer p) {
//    playerYMLFile = new File(
//        plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data",
//        p.getUniqueId() + ".yml");
//
//    if (!plugin.getDataFolder().exists()) {
//      plugin.getDataFolder().mkdir();
//    }
//
//    if (!playerYMLFile.exists()) {
//      try {
//        playerYMLFile.createNewFile();
//      } catch (Exception e) {
//        //Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Error creating " + playerYMLFile.getName() + "!");
//      }
//    }
//    playerConfigurationFile = YamlConfiguration.loadConfiguration(playerYMLFile);
//    plugin.getPlayerDataManager().createDefaultSections(p);
//  }

  /*
   * File Exists
   * Create It
   * Access It
   *
   * Save File Method
   *
   *
   * */

//  private File playerYMLFile;
//  private FileConfiguration playerConfigurationFile;
//
//  public boolean playerFileExists(OfflinePlayer p) {
//    playerYMLFile = getPlayerYMLFile(p);
//    return playerYMLFile.exists();
//  }
//
//  public void createPlayerFile(OfflinePlayer p) {
//    playerYMLFile = new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", p.getUniqueId() + ".yml");
//
//    boolean fileCreated = getPlayerYMLFile(p).exists();
//
//    if (!fileCreated) {
//      try {
//        fileCreated = getPlayerYMLFile(p).createNewFile();
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//
//    if (!fileCreated) {
//      LogUtils.logError("[hugs] An error occurred while creating the player's data.");
//      return;
//    }
//
//    playerConfigurationFile = YamlConfiguration.loadConfiguration(playerYMLFile);
//    plugin.getPlayerDataManager().createDefaultSections(p.getUniqueId());
//  }
//
//  public File getPlayerYMLFile(OfflinePlayer p) {
//    playerYMLFile = new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", p.getUniqueId() + ".yml");
//    return playerYMLFile;
//  }
//
//  public void load(OfflinePlayer p) {
//    playerYMLFile = new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", p.getUniqueId() + ".yml");
//    playerConfigurationFile = YamlConfiguration.loadConfiguration(playerYMLFile);
//  }
//
//  public FileConfiguration getPlayerDataFile(OfflinePlayer player) {
//    return plugin.getPlayerDataManager().getPlayerConfiguration(player.getUniqueId());
//  }
//
//  public void savePlayerDataFile() {
//    try {
//      playerConfigurationFile.save(playerYMLFile);
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }

  //----------------------------------------------------------------------------------------------

//  private boolean playerFileExists(UUID uuid) {
//    return new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", uuid + ".yml").exists();
//  }
//
//  public void createPlayerFile(UUID uuid) {
//    boolean fileCreated = playerFileExists(uuid);
//
//    if (!fileCreated) {
//      try {
//        fileCreated = getPlayerYMLFile(uuid).createNewFile();
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//
//    if (!fileCreated) {
//      LogUtils.logError("[hugs] An error occurred while creating the player's data.");
//      return;
//    }
//
//    //createDefaultSections(uuid);
//  }
//
//  public File getPlayerYMLFile(UUID uuid) {
//    return new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", uuid + ".yml");
//  }
//
//  public YamlConfiguration getPlayerConfiguration(UUID uuid) {
//    File data = new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", uuid + ".yml");
//    if (playerFileExists(uuid)) {
//      return YamlConfiguration.loadConfiguration(data);
//    }
//    createPlayerFile(uuid);
//    return getPlayerConfiguration(uuid);
//  }
//
//  public void savePlayerDataFile(UUID uuid, YamlConfiguration data) {
//    File file = new File(plugin.getDataFolder() + File.separator + "data" + File.separator + "player-data", uuid + ".yml");
//    try {
//      data.save(file);
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//  }

}
