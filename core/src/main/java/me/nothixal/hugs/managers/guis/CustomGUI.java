package me.nothixal.hugs.managers.guis;

import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class CustomGUI implements Listener {

  private Inventory inventory;

  public CustomGUI() {
    this.inventory = Bukkit.createInventory(null, 9 * 6);
  }

  public CustomGUI(int rows, String title) {
    int adjustedRows = rows;

    if (rows > 6 || rows < 1) {
      adjustedRows = 6;
    }

    this.inventory = Bukkit.createInventory(null, 9 * adjustedRows, ChatUtils.colorChat(title));
  }

  public Inventory getInventory() {
    return inventory;
  }

  public void openInventory(Player player) {

  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
  }
}