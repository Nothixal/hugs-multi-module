package me.nothixal.hugs.managers.guis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.AdminPanelGUI;
import me.nothixal.hugs.guis.CommandsGUI;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.guis.LeaderboardGUI;
import me.nothixal.hugs.guis.PluginLexiconGUI;
import me.nothixal.hugs.guis.YourLanguageGUI;
import me.nothixal.hugs.guis.settings.PluginSettingsGUI;
import me.nothixal.hugs.guis.settings.backend.BackendSettingsGUI;
import me.nothixal.hugs.guis.settings.backend.BroadcastSettingsGUI;
import me.nothixal.hugs.guis.settings.player.YourIndicatorsGUI;
import me.nothixal.hugs.guis.settings.player.YourPreferencesGUI;
import me.nothixal.hugs.guis.stats.ErosStatsGUI;
import me.nothixal.hugs.guis.stats.GlobalStatsGUI;
import me.nothixal.hugs.guis.stats.PlayerStatsGUI;
import me.nothixal.hugs.guis.stats.SilviaStatsGUI;
import me.nothixal.hugs.guis.stats.YourStatsGUI;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.text.TextUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.PluginManager;

public class GUIManager {

  private final HugsPlugin plugin;
  private List<Listener> pluginGUIs = new ArrayList<>();
  private Map<UUID, Inventory> helpGUIs = new HashMap<>();
  private Map<UUID, Inventory> leaderboardGUIs = new HashMap<>();
  private Map<UUID, Inventory> quickLinksGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourSettingsGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourStatsGUIs = new HashMap<>();
  private Map<UUID, Inventory> yourIndicatorGUIs = new HashMap<>();

  private Map<UUID, Inventory> playerStatsGUIs = new HashMap<>();
  private Map<UUID, Inventory> commandsGUIs = new HashMap<>();


  private Map<UUID, Inventory> temporaryGUIs = new HashMap<>();

  private Inventory pluginSettingsGUI;
  private Inventory backendSettingsGUI;
  private Inventory broadcastSettingsGUI;

  private Inventory erosStatsGUI;
  private Inventory silviaStatsGUI;

  private String guiPrefix = PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs &8&l>> ";

  public GUIManager(HugsPlugin plugin) {
    this.plugin = plugin;
    pluginSettingsGUI = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Plugin Settings"));
    backendSettingsGUI = Bukkit.createInventory(null, 9 * 6, TextUtils.colorText(guiPrefix + "&8Backend Settings"));

    broadcastSettingsGUI = Bukkit.createInventory(null, 9 * 6, TextUtils.colorText(guiPrefix + "&8Broadcast Settings"));

    erosStatsGUI = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Eros"));
    silviaStatsGUI = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Silvia"));
  }

  public void addGUIs() {
    // Plugin Help Menu
    pluginGUIs.add(new HelpGUI(plugin));
    pluginGUIs.add(new LeaderboardGUI(plugin));
    pluginGUIs.add(new CommandsGUI(plugin));
    pluginGUIs.add(new YourStatsGUI(plugin));
    pluginGUIs.add(new GlobalStatsGUI(plugin));
    pluginGUIs.add(new PlayerStatsGUI(plugin));
    pluginGUIs.add(new PluginLexiconGUI(plugin));
    pluginGUIs.add(new YourPreferencesGUI(plugin));
    pluginGUIs.add(new YourIndicatorsGUI(plugin));
    pluginGUIs.add(new YourLanguageGUI(plugin));

    // Plugin Settings GUIs
    pluginGUIs.add(new AdminPanelGUI(plugin));
    pluginGUIs.add(new PluginSettingsGUI(plugin));
    pluginGUIs.add(new BackendSettingsGUI(plugin));
    pluginGUIs.add(new BroadcastSettingsGUI(plugin));

    // Lovebird Stats GUIs
    pluginGUIs.add(new ErosStatsGUI(plugin));
    pluginGUIs.add(new SilviaStatsGUI(plugin));
  }

  public void registerListeners(PluginManager pluginManager) {
    for (Listener listener : pluginGUIs) {
      pluginManager.registerEvents(listener, plugin);
    }
  }

  public Inventory getPluginSettingsGUI() {
    return pluginSettingsGUI;
  }

  public Inventory getBroadcastSettingsGUI() {
    return broadcastSettingsGUI;
  }

  public Map<UUID, Inventory> getHelpGUIs() {
    return helpGUIs;
  }

  public Map<UUID, Inventory> getLeaderboardGUIs() {
    return leaderboardGUIs;
  }

  public Map<UUID, Inventory> getQuickLinksGUIs() {
    return quickLinksGUIs;
  }

  public Map<UUID, Inventory> getYourSettingsGUIs() {
    return yourSettingsGUIs;
  }

  public Map<UUID, Inventory> getYourStatsGUIs() {
    return yourStatsGUIs;
  }

  public Map<UUID, Inventory> getYourIndicatorGUIs() {
    return yourIndicatorGUIs;
  }

  public Map<UUID, Inventory> getPlayerStatsGUIs() {
    return playerStatsGUIs;
  }

  public Map<UUID, Inventory> getCommandsGUIs() {
    return commandsGUIs;
  }

  public Map<UUID, Inventory> getTemporaryGUIs() {
    return temporaryGUIs;
  }

  public Inventory getErosStatsGUI() {
    return erosStatsGUI;
  }

  public Inventory getSilviaStatsGUI() {
    return silviaStatsGUI;
  }

  public Inventory getBackendSettingsGUI() {
    return backendSettingsGUI;
  }

  public String getGuiPrefix() {
    return guiPrefix;
  }
}
