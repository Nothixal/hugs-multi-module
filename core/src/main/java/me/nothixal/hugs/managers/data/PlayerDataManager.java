package me.nothixal.hugs.managers.data;

import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.utils.Indicator;

public interface PlayerDataManager {

  /**
   * Returns whether or not the player exists in the database.
   * */
  boolean playerExists(UUID uuid);

  /**
   * Create a database entry for the player.
   * */
  void createPlayer(UUID uuid);

  /**
   * Remove the database entry for the player.
   * */
  void deletePlayer(UUID uuid);


  void loadDatabase();

  void reloadDatabase();

  void saveDatabase();

  void purgeDatabase();

  void loadData(UUID uuid);

  void saveData(UUID uuid);

  void reloadData(UUID uuid);

  void purgeData(UUID uuid);

  //
  // Self Hugs
  //

  int getSelfHugs(UUID uuid);

  void setSelfHugs(UUID uuid, int value);

  void incrementSelfHugs(UUID uuid);

  void incrementSelfHugs(UUID uuid, int amount);

  //
  // Normal Hugs
  //

  /**
  * Get the amount of normal hugs a player has given.
  * */
  int getHugsGiven(UUID uuid);

  /**
   * Set the amount of normal hugs a player has given.
   * */
  void setHugsGiven(UUID uuid, int value);

  /**
   * Increment the amount of normal hugs a player has given by 1.
   * */
  void incrementHugsGiven(UUID uuid);

  /**
   * Increment the amount of normal hugs a player has given.
   * @param amount The amount to increment by.
   * */
  void incrementHugsGiven(UUID uuid, int amount);

  /**
   * Get the amount of normal hugs a player has received.
   * */
  int getHugsReceived(UUID uuid);

  /**
   * Set the amount of normal hugs a player has received.
   * */
  void setHugsReceived(UUID uuid, int value);

  /**
   * Increment the amount of normal hugs a player has received by 1.
   * */
  void incrementHugsReceived(UUID uuid);

  /**
   * Increment the amount of normal hugs a player has received.
   * @param amount The amount to increment by.
   * */
  void incrementHugsReceived(UUID uuid, int amount);

  //
  // Mass Hugs
  //

  /**
   * Get the amount of mass hugs a player has given.
   * */
  int getMassHugsGiven(UUID uuid);

  /**
   * Set the amount of mass hugs a player has given.
   * */
  void setMassHugsGiven(UUID uuid, int value);

  /**
   * Increment the amount of mass hugs a player has given by 1.
   * */
  void incrementMassHugsGiven(UUID uuid);

  /**
   * Increment the amount of mass hugs a player has given.
   * @param amount The amount to increment by.
   * */
  void incrementMassHugsGiven(UUID uuid, int amount);

  /**
   * Get the amount of mass hugs a player has received.
   * */
  int getMassHugsReceived(UUID uuid);

  /**
   * Set the amount of mass hugs a player has received.
   * */
  void setMassHugsReceived(UUID uuid, int value);

  /**
   * Increment the amount of mass hugs a player has received by 1.
   * */
  void incrementMassHugsReceived(UUID uuid);

  /**
   * Increment the amount of mass hugs a player has received.
   * @param amount The amount to increment by.
   * */
  void incrementMassHugsReceived(UUID uuid, int amount);

  /**
   * Get the total amount of self hugs given by all players.
   * */
  int getTotalSelfHugsGiven();

  /**
   * Get the total amount of hugs given by all players.
   * */
  int getTotalHugsGiven();

  /**
   * Get the total amount of hugs received by all players.
   * */
  int getTotalMassHugsGiven();

  /**
   * Check to see if player wants to accept hugs from anyone.
   * */
  boolean wantsHugs(UUID uuid);

  /**
   * Set the player's hug status.
   * */
  void setWantsHugs(UUID uuid, boolean bool);

  //
  //
  //
  boolean wantsParticles(UUID uuid);

  void setWantsParticles(UUID uuid, boolean bool);

  /**
   * Check to see if player wants to receive particle effects when getting hugged.
   * */
  String getParticleValue(UUID uuid);

  void setParticleValue(UUID uuid, String value);

  /**
   * Check to see if player wants to hear sounds when getting hugged.
   * */
  boolean wantsSounds(UUID uuid);

  void setWantsSounds(UUID uuid, boolean bool);

  /**
   * Check to see if the player has given any form of hugs before.
   * */
  boolean hasGivenHugsBefore(UUID uuid);

  /**
   * Check to see if the player has received any form of hugs before.
   * */
  boolean hasReceivedHugsBefore(UUID uuid);

  List<Indicator> getIndicators(UUID uuid);

  boolean getIndicator(UUID uuid, String type);

  void setIndicator(UUID uuid, String type, boolean enabled);

  DatabaseType getDatabaseType();
}