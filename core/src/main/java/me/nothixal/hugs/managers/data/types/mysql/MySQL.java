//package me.nothixal.hugs.managers.data.types.mysql;
//
//import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
//import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.sql.Statement;
//import me.nothixal.hugs.HugsPlugin;
//import org.bukkit.configuration.file.FileConfiguration;
//
//public class MySQL {
//
//  private HugsPlugin plugin;
//  private MysqlDataSource dataSource;
//  private Connection connection;
//  private String host;
//  private String database;
//  private String username;
//  private String password;
//  private String table;
//  private int port;
//
//  public MySQL(HugsPlugin plugin) {
//    this.plugin = plugin;
//    initializeValues();
//
//    try {
//      testConnection();
//    } catch (SQLException throwables) {
//      throwables.printStackTrace();
//    }
//  }
//
//  private void initializeValues() {
//    dataSource = new MysqlConnectionPoolDataSource();
//
//    plugin.getFileManager().loadMySQLFile();
//    FileConfiguration mysqlData = plugin.getFileManager().getMysqlFileData();
//
//    host = mysqlData.getString("database.host");
//    port = mysqlData.getInt("database.port");
//    database = mysqlData.getString("database.database");
//    username = mysqlData.getString("database.username");
//    password = mysqlData.getString("database.password");
//
//    dataSource.setServerName(host);
//    dataSource.setPortNumber(port);
//    dataSource.setDatabaseName(database);
//    dataSource.setUser(username);
//    dataSource.setPassword(password);
//    dataSource.setUseSSL(false);
//    try {
//      dataSource.setAllowPublicKeyRetrieval(true);
//    } catch (SQLException throwables) {
//      throwables.printStackTrace();
//    }
//  }
//
//  public void openConnection() {
//
//  }
//
//  public void testConnection() throws SQLException {
//    try (Connection conn = dataSource.getConnection()) {
//      if (!conn.isValid(1000)) {
//        throw new SQLException("Could not establish database connection.");
//      } else {
//        System.out.println(" ");
//        System.out.println("Connection SUCCESSFUL!");
//        System.out.println(" ");
//      }
//    }
//  }
//
//  public void closeConnection() {
//    try {
//      connection.close();
//    } catch (SQLException e) {
//      e.printStackTrace();
//    }
//  }
//
//  public void executeQuery(String query) {
//    try {
//      Statement statement = connection.createStatement();
//      statement.executeQuery(query);
//    } catch (SQLException e) {
//      e.printStackTrace();
//    }
//  }
//
//  public boolean playerExists() {
//    return false;
//  }
//
//  public Connection getConnection() throws SQLException {
//    return dataSource.getConnection();
//  }
//
//  public String getHost() {
//    return host;
//  }
//
//  public String getDatabase() {
//    return database;
//  }
//
//  public String getUsername() {
//    return username;
//  }
//
//  public String getPassword() {
//    return password;
//  }
//
//  public String getTable() {
//    return table;
//  }
//
//  public int getPort() {
//    return port;
//  }
//}