package me.nothixal.hugs.managers.advancements;

import java.util.UUID;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface AdvancementManager {

  void sendToast(ItemStack item, UUID uuid, String title, String description);

  void sendToast(UUID uuid, String title, String description);

  void sendToast(Player player, String title, String description);

  void sendToast(String playerName, String title, String description);

  void sendGoalToast(ItemStack item, UUID uuid, String title, String description);

  void sendChallengeToast(ItemStack item, UUID uuid, String title, String description);

}
