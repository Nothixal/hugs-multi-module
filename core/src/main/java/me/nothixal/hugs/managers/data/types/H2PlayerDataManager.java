package me.nothixal.hugs.managers.data.types;

import java.util.List;
import java.util.UUID;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.utils.Indicator;

public class H2PlayerDataManager implements PlayerDataManager {

  @Override
  public boolean playerExists(UUID uuid) {
    return false;
  }

  @Override
  public void createPlayer(UUID uuid) {

  }

  @Override
  public void deletePlayer(UUID uuid) {

  }

  @Override
  public void loadDatabase() {

  }

  @Override
  public void reloadDatabase() {

  }

  @Override
  public void saveDatabase() {

  }

  @Override
  public void purgeDatabase() {

  }

  @Override
  public void loadData(UUID uuid) {

  }

  @Override
  public void saveData(UUID uuid) {

  }

  @Override
  public void reloadData(UUID uuid) {

  }

  @Override
  public void purgeData(UUID uuid) {

  }

  @Override
  public int getSelfHugs(UUID uuid) {
    return 0;
  }

  @Override
  public void setSelfHugs(UUID uuid, int value) {

  }

  @Override
  public void incrementSelfHugs(UUID uuid) {

  }

  @Override
  public void incrementSelfHugs(UUID uuid, int amount) {

  }

  @Override
  public int getHugsGiven(UUID uuid) {
    return 0;
  }

  @Override
  public void setHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public void incrementHugsGiven(UUID uuid) {

  }

  @Override
  public void incrementHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public int getHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public void setHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public void incrementHugsReceived(UUID uuid) {

  }

  @Override
  public void incrementHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public int getMassHugsGiven(UUID uuid) {
    return 0;
  }

  @Override
  public void setMassHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public void incrementMassHugsGiven(UUID uuid) {

  }

  @Override
  public void incrementMassHugsGiven(UUID uuid, int amount) {

  }

  @Override
  public int getMassHugsReceived(UUID uuid) {
    return 0;
  }

  @Override
  public void setMassHugsReceived(UUID uuid, int value) {

  }

  @Override
  public void incrementMassHugsReceived(UUID uuid) {

  }

  @Override
  public void incrementMassHugsReceived(UUID uuid, int amount) {

  }

  @Override
  public int getTotalSelfHugsGiven() {
    return 0;
  }

  @Override
  public int getTotalHugsGiven() {
    return 0;
  }

  @Override
  public int getTotalMassHugsGiven() {
    return 0;
  }

  @Override
  public boolean wantsHugs(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsHugs(UUID uuid, boolean bool) {

  }

  @Override
  public boolean wantsParticles(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsParticles(UUID uuid, boolean bool) {

  }

  @Override
  public String getParticleValue(UUID uuid) {
    return null;
  }

  @Override
  public void setParticleValue(UUID uuid, String value) {

  }

  @Override
  public boolean wantsSounds(UUID uuid) {
    return false;
  }

  @Override
  public void setWantsSounds(UUID uuid, boolean bool) {

  }

  @Override
  public boolean hasGivenHugsBefore(UUID uuid) {
    return false;
  }

  @Override
  public boolean hasReceivedHugsBefore(UUID uuid) {
    return false;
  }

  @Override
  public List<Indicator> getIndicators(UUID uuid) {
    return null;
  }

  @Override
  public boolean getIndicator(UUID uuid, String type) {
    return false;
  }

  @Override
  public void setIndicator(UUID uuid, String type, boolean enabled) {

  }

  @Override
  public DatabaseType getDatabaseType() {
    return DatabaseType.H2;
  }
}
