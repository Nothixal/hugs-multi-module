package me.nothixal.hugs.managers.files;

import java.io.File;
import me.nothixal.hugs.enums.types.FileType;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileLoader {

  private FileType fileType;
  private File file;
  private FileConfiguration configuration;

  public FileLoader(FileType fileType, File file, FileConfiguration configuration) {
    this.fileType = fileType;
    this.file = file;
    this.configuration = configuration;
    load();
  }

  private void load() {
    if (fileType == FileType.YML) {
      configuration = YamlConfiguration.loadConfiguration(file);
      return;
    }

    if (fileType == FileType.TEXT) {
      return;
    }
  }

  public FileType getFileType() {
    return fileType;
  }

  public void setFileType(FileType fileType) {
    this.fileType = fileType;
  }

  public File getFile() {
    return file;
  }

  public FileConfiguration getConfiguration() {
    return configuration;
  }

  public void setConfiguration(FileConfiguration configuration) {
    this.configuration = configuration;
  }
}
