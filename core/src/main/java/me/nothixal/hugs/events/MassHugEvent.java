package me.nothixal.hugs.events;

import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MassHugEvent extends Event implements Cancellable {

  private static final HandlerList HANDLERS = new HandlerList();

  private final Player giver;
  private final List<Player> receivers;

  public MassHugEvent(Player giver, List<Player> receivers) {
    this.giver = giver;
    this.receivers = receivers;
  }

  @Override
  public HandlerList getHandlers() {
    return HANDLERS;
  }

  public static HandlerList getHandlerList() {
    return HANDLERS;
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  @Override
  public void setCancelled(boolean b) {

  }

  public Player getGiver() {
    return giver;
  }

  public List<Player> getReceivers() {
    return receivers;
  }
}
