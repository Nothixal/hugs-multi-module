package me.nothixal.hugs.events;

import me.nothixal.hugs.enums.types.HugType;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class HugEvent extends Event implements Cancellable {

  private static final HandlerList HANDLERS = new HandlerList();

  private final Player giver;
  private final Player receiver;
  private final HugType hugType;

  public HugEvent(Player giver, Player receiver, HugType hugType) {
    this.giver = giver;
    this.receiver = receiver;
    this.hugType = hugType;
  }

  @Override
  public HandlerList getHandlers() {
    return HANDLERS;
  }

  public static HandlerList getHandlerList() {
    return HANDLERS;
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  @Override
  public void setCancelled(boolean b) {

  }

  public Player getGiver() {
    return giver;
  }

  public Player getReceiver() {
    return receiver;
  }

  public HugType getHugType() {
    return hugType;
  }
}