package me.nothixal.hugs.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NormalHugEvent extends Event implements Cancellable {

  private static final HandlerList HANDLERS = new HandlerList();

  private final Player giver;
  private final Player receiver;

  public NormalHugEvent(Player giver, Player receiver) {
    this.giver = giver;
    this.receiver = receiver;
  }

  @Override
  public HandlerList getHandlers() {
    return HANDLERS;
  }

  public static HandlerList getHandlerList() {
    return HANDLERS;
  }

  @Override
  public boolean isCancelled() {
    return false;
  }

  @Override
  public void setCancelled(boolean b) {

  }

  public Player getGiver() {
    return giver;
  }

  public Player getReceiver() {
    return receiver;
  }

}
