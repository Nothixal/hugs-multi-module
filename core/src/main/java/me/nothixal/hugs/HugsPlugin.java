package me.nothixal.hugs;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.commands.HugCommand;
import me.nothixal.hugs.commands.hugs.HugsCommand;
import me.nothixal.hugs.commands.hugs.NewHugsCommand;
import me.nothixal.hugs.enums.Filter;
import me.nothixal.hugs.enums.Type;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.holidays.Holiday;
import me.nothixal.hugs.enums.holidays.HolidayOverride;
import me.nothixal.hugs.enums.types.DatabaseType;
import me.nothixal.hugs.listeners.ChatConfirmationListener;
import me.nothixal.hugs.listeners.InventoryClickListener;
import me.nothixal.hugs.listeners.JoinListener;
import me.nothixal.hugs.listeners.NewHugListener;
import me.nothixal.hugs.listeners.PluginUpdateListener;
import me.nothixal.hugs.listeners.ShiftHugListener;
import me.nothixal.hugs.listeners.experimental.InvincibilityListener;
import me.nothixal.hugs.listeners.experimental.PassiveModeListener;
import me.nothixal.hugs.managers.HookManager;
import me.nothixal.hugs.managers.VerboseManager;
import me.nothixal.hugs.managers.advancements.AdvancementManager;
import me.nothixal.hugs.managers.chat.ChatManager;
import me.nothixal.hugs.managers.data.LocalSettingsManager;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.managers.data.types.H2PlayerDataManager;
import me.nothixal.hugs.managers.data.types.local.YAMLLocalSettingsManager;
import me.nothixal.hugs.managers.data.types.mysql.MySQLPlayerDataManager;
import me.nothixal.hugs.managers.data.types.yaml.YAMLPlayerDataManager;
import me.nothixal.hugs.managers.files.FileManager;
import me.nothixal.hugs.managers.guis.GUIManager;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.Constants;
import me.nothixal.hugs.utils.HolidayChecker;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.VersionChecker;
import me.nothixal.hugs.utils.Watermark;
import me.nothixal.hugs.utils.logger.LogUtils;
import me.nothixal.hugs.utils.updater.PluginUpdater;
import me.nothixal.hugs.utils.updater.UpdateChecker;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author ShadowMasterGaming
 * @created April 19th, 2017
 */
public class HugsPlugin extends JavaPlugin {

  private HugCommand hugCommand;
  private HugsCommand hugsCommand;

  private NewHugsCommand newHugsCommand;

  private Watermark watermark;

  private VersionChecker versionChecker;
  private VerboseManager verboseManager;
  private FileManager fileManager;
  private PlayerDataManager playerDataManager;
  private LocalSettingsManager localSettingsManager;
  private ChatManager chatManager;
  private ItemManager itemManager;
  private AdvancementManager advancementManager;
  private HookManager hookManager;
  private GUIManager guiManager;

  private PluginUpdater pluginUpdater;

  private Constants constants;

  private boolean initialSetupFailed = false;

  private HolidayChecker holidayChecker;
  private Holiday holiday = Holiday.NONE;
  private HolidayOverride override = HolidayOverride.NONE;

  private final Map<UUID, Filter> filterList = new HashMap<>();
  private final Map<Player, Boolean> invinciblePlayers = new HashMap<>();

  @Override
  public void onLoad() {

  }

  @Override
  public void onEnable() {
    registerRequiredClasses();

    hookManager.registerHooks();

    watermark.sendPluginPreEnableWatermark();

    fileManager.checkForAndCreateFiles();

    if (!initialSetup()) {
      initialSetupFailed = true;
      verboseManager.sendPluginFailure();
      getServer().getPluginManager().disablePlugin(this);
      return;
    }

    registerClasses();

    scheduleEvents();



    if (fileManager.getStartupData().getBoolean(Settings.SHOW_HOOKS.getValue())) {
      hookManager.printHooks();
    }

    playerDataManager.loadDatabase();

    registerCommands();
    registerListeners();

    if (getConfig().getBoolean(Settings.STARTUP_VERBOSE_ENABLED.getValue())) {
      String setting = getConfig().getString(Settings.STARTUP_VERBOSE_DISPLAY_SETTINGS.getValue(), "None");

      if (setting.equalsIgnoreCase("All")) {
        verboseManager.sendFullConfigSettings();
      } else if (setting.equalsIgnoreCase("Informative")) {
        verboseManager.sendLightConfigSettings();
      }
    }

    watermark.sendPluginEnableWatermark();

    if (fileManager.getUpdaterData().getBoolean(Settings.CHECK_FOR_UPDATES.getValue())) {
      UpdateChecker.init(this, PluginConstants.PLUGIN_ID);

      this.pluginUpdater = new PluginUpdater(this);
      getServer().getPluginManager().registerEvents(new PluginUpdateListener(this), this);

      pluginUpdater.checkForUpdate();
    }
  }

  @Override
  public void onDisable() {
    if (!initialSetupFailed) {
      hugCommand.getInvinciblePlayers().clear();
      hugsCommand.getTyping().clear();
      hugsCommand.getConfirming().clear();

      if (getDataFolder().exists()) {
        reloadData();
        LogUtils.logInfo("The plugin's data was reloaded.");

        saveData();
        LogUtils.logInfo("The plugin's data was saved.");
      }

      playerDataManager.saveDatabase();
      LogUtils.logInfo(" ");
    }

    watermark.sendPluginDisableWatermark();
  }

  private void registerRequiredClasses() {
    this.holidayChecker = new HolidayChecker(this);
    this.versionChecker = new VersionChecker(this);

    versionChecker.checkServerVersion();

    this.verboseManager = new VerboseManager(this);
    this.watermark = new Watermark(this);
    this.fileManager = new FileManager(this);
    this.hookManager = new HookManager(this);
  }

  private void registerClasses() {
    this.hugCommand = new HugCommand(this);
    this.hugsCommand = new HugsCommand(this);

    this.newHugsCommand = new NewHugsCommand(this);

    this.guiManager = new GUIManager(this);
    guiManager.addGUIs();

    this.constants = new Constants(this);
  }

  private void registerCommands() {
    getCommand("hug").setExecutor(hugCommand);
    getCommand("src/main/java/hugs").setExecutor(newHugsCommand);
  }

  private void registerListeners() {
    PluginManager pluginManager = getServer().getPluginManager();
    pluginManager.registerEvents(new InventoryClickListener(this), this);
    pluginManager.registerEvents(new ChatConfirmationListener(this), this);
    pluginManager.registerEvents(new PassiveModeListener(this), this);
    pluginManager.registerEvents(new InvincibilityListener(this), this);
    pluginManager.registerEvents(new ShiftHugListener(this), this);
    pluginManager.registerEvents(new JoinListener(this), this);
    pluginManager.registerEvents(new NewHugListener(), this);

    guiManager.registerListeners(pluginManager);
  }

  public void reload(CommandSender sender) {
    fileManager.checkForAndCreateFiles();
    fileManager.reloadConfig();

    reloadConfig();

    playerDataManager.reloadDatabase();

    String senderName = (sender instanceof ConsoleCommandSender) ? getConfig().getString(Settings.CONSOLE_NAME.getValue()) : sender.getName();
    verboseManager.logVerbose(senderName + " reloaded the configuration.", Type.INFO);

    if (getConfig().getBoolean(Settings.RELOAD_VERBOSE_ENABLED.getValue())) {
      String setting = getConfig().getString(Settings.RELOAD_VERBOSE_DISPLAY_SETTINGS.getValue());

      if (setting.equalsIgnoreCase("All")) {
        verboseManager.sendFullConfigSettings();
      } else if (setting.equalsIgnoreCase("Informative")) {
        verboseManager.sendLightConfigSettings();
      }
    }
  }

  private boolean initialSetup() {
    this.localSettingsManager = new YAMLLocalSettingsManager(this);

    if (!versionChecker.isSupportedVersion()) {
      return false;
    }

    versionChecker.registerClasses();

    String databaseType = getConfig().getString("storage", "yaml");

    if (databaseType.equalsIgnoreCase(DatabaseType.MYSQL.getValue())) {
      playerDataManager = new MySQLPlayerDataManager(this);
    }

    if (databaseType.equalsIgnoreCase(DatabaseType.H2.getValue())) {
      playerDataManager = new H2PlayerDataManager();
    }

    if (databaseType.equalsIgnoreCase(DatabaseType.YAML.getValue())) {
      playerDataManager = new YAMLPlayerDataManager(this);
    }

    return true;
  }

  private void scheduleEvents() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DAY_OF_MONTH, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    new BukkitRunnable() {

      @Override
      public void run() {
        playerDataManager.saveDatabase();
      }
    }.runTaskTimerAsynchronously(this, 0, 20 * 60 /* * 5*/);

    new BukkitRunnable() {
      long millisTillMidnight = (calendar.getTimeInMillis() - System.currentTimeMillis());

      @Override
      public void run() {
//        if (millisTillMidnight <= 0) {
//          //System.out.println(LogUtils.getConsolePrefix() + "Millis less than 0:  " + millisTillMidnight);
//          //System.out.println("Restarting plugin to check for holidays.");
//        }

        //System.out.println(LogUtils.getConsolePrefix() + "Millis Till Midnight: " + millisTillMidnight);
        millisTillMidnight = (calendar.getTimeInMillis() - System.currentTimeMillis());
      }
    }.runTaskTimerAsynchronously(this, 0, 20 * 60);
  }

  public void saveData() {
    try {
      getDataFile().save(getFileManager().getDataFile());
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public void reloadData() {
    try {
      setDataFile(YamlConfiguration.loadConfiguration(getFileManager().getDataFile()));
    } catch (Exception ex) {
      LogUtils.logError("An error occurred whilst loading the data file!");
      LogUtils.logError("Printing stack trace:");
      ex.printStackTrace();
      LogUtils.logError("End of stack trace.");
    }
  }

  // Commands
  public HugCommand getHugCommand() {
    return hugCommand;
  }

  public HugsCommand getHugsCommand() {
    return hugsCommand;
  }

  // Managers
  public PlayerDataManager getPlayerDataManager() {
    return playerDataManager;
  }

  public GUIManager getGUIManager() {
    return guiManager;
  }

  public ChatManager getChatManager() {
    return chatManager;
  }

  public void setChatManager(ChatManager chatManager) {
    this.chatManager = chatManager;
  }

  // Files
  public FileManager getFileManager() {
    return fileManager;
  }

  public ItemManager getItemManager() {
    return itemManager;
  }

  public void setItemManager(ItemManager itemManager) {
    this.itemManager = itemManager;
  }

  public VerboseManager getVerboseManager() {
    return verboseManager;
  }

  public FileConfiguration getDataFile() {
    return fileManager.getData();
  }

  private void setDataFile(FileConfiguration data) {
    fileManager.dataYMLData = data;
  }

  public FileConfiguration getLangFile() {
    return fileManager.getLang();
  }

  // Other
  public HolidayOverride getHolidayOverride() {
    return override;
  }

  public void setOverride(HolidayOverride override) {
    this.override = override;
  }

  public Holiday getHoliday() {
    return holiday;
  }

  public void setHoliday(Holiday holiday) {
    this.holiday = holiday;
  }

  public HolidayChecker getHolidayChecker() {
    return holidayChecker;
  }

  public void setHolidayChecker(HolidayChecker holidayChecker) {
    this.holidayChecker = holidayChecker;
  }

  public VersionChecker getVersionChecker() {
    return versionChecker;
  }

  public Map<UUID, Filter> getFilterList() {
    return filterList;
  }


  public Constants getItemToggles() {
    return constants;
  }

  public LocalSettingsManager getLocalSettingsManager() {
    return localSettingsManager;
  }

  public PluginUpdater getPluginUpdater() {
    return pluginUpdater;
  }

  public AdvancementManager getAdvancementManager() {
    return advancementManager;
  }

  public void setAdvancementManager(AdvancementManager advancementManager) {
    this.advancementManager = advancementManager;
  }

  public Map<Player, Boolean> getInvinciblePlayers() {
    return invinciblePlayers;
  }
}