package me.nothixal.hugs.listeners;

import com.cryptomorin.xseries.XMaterial;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Cooldowns;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.Cooldown;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class ShiftHugListener implements Listener {

  private final HugsPlugin plugin;

  public ShiftHugListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  // Right Click handler.
  @EventHandler
  public void onShiftHug(PlayerInteractEntityEvent event) {
    if (!plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue())) {
      return;
    }

    if (plugin.getConfig().getString(Settings.SHIFT_HUG_USAGE.getValue()).equalsIgnoreCase("LEFT_CLICK")) {
      return;
    }

    if (!(event.getRightClicked() instanceof Player)) {
      return;
    }

    Player player = event.getPlayer();
    Player receiver = (Player) event.getRightClicked();

    if (!(event.getHand() == EquipmentSlot.HAND)) {
      return;
    }

    if (!player.isSneaking()) {
      return;
    }

    if (!(player.getLocation().distance(receiver.getLocation()) <= 2)) {
      return;
    }

    if (Cooldown.hasCooldown(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue())) {
      player.sendMessage(ChatUtils.colorChat(Errors.COOLDOWN_MESSAGE.getLangValue().replace("%cooldown%", "" + Cooldown.getExactTimeLeft(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue()))));
      return;
    }

    plugin.getHugCommand().giveHugs(player, receiver);

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
      new Cooldown(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue(),
          plugin.getConfig().getInt(Settings.NORMAL_HUG_COOLDOWN.getValue())).start();
    }
  }

  // Left Click handler.
  @EventHandler
  public void onPlayerInteract(EntityDamageByEntityEvent event) {
    if (!plugin.getConfig().getBoolean(Settings.ALLOW_SHIFT_HUG.getValue())) {
      return;
    }

    if (!plugin.getConfig().getString(Settings.SHIFT_HUG_USAGE.getValue()).equalsIgnoreCase("LEFT_CLICK")) {
      return;
    }

    if (!(event.getEntity() instanceof Player)) {
      return;
    }

    if (!(event.getDamager() instanceof Player)) {
      return;
    }

    Player player = (Player) event.getDamager();
    Player hitPlayer = (Player) event.getEntity();

    if (hitPlayer == null) {
      return;
    }

    if (!player.isSneaking()) {
      return;
    }

    if (!(player.getInventory().getItemInMainHand() == null) && !(player.getInventory().getItemInMainHand().getType() == XMaterial.AIR.parseMaterial())) {
      return;
    }

    if (!(player.getLocation().distance(hitPlayer.getLocation()) <= 2)) {
      return;
    }

    if (Cooldown.hasCooldown(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue())) {
      event.setCancelled(true);
      player.sendMessage(ChatUtils.colorChat(Errors.COOLDOWN_MESSAGE.getLangValue().replace("%cooldown%", "" + Cooldown.getExactTimeLeft(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue()))));
      return;
    }

    event.setCancelled(true);
    plugin.getHugCommand().giveHugs(player, hitPlayer);
    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
      new Cooldown(player.getUniqueId(), Cooldowns.NORMAL_HUG_COOLDOWN.getValue(),
          plugin.getConfig().getInt(Settings.NORMAL_HUG_COOLDOWN.getValue())).start();
    }
  }

}
