package me.nothixal.hugs.listeners;

import me.nothixal.hugs.enums.types.HugType;
import me.nothixal.hugs.events.HugEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NewHugListener implements Listener {

  @EventHandler
  public void onHug(HugEvent event) {
    if (event.getHugType() == HugType.SELF) {
      //System.out.println("Self Hug");
    }
  }

}
