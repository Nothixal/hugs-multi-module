package me.nothixal.hugs.listeners;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Filter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinListener implements Listener {

  private final HugsPlugin plugin;

  public JoinListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    if (!plugin.getPlayerDataManager().playerExists(event.getPlayer().getUniqueId())) {
      plugin.getPlayerDataManager().createPlayer(event.getPlayer().getUniqueId());
    }

    plugin.getFilterList().put(event.getPlayer().getUniqueId(), Filter.HUGS_GIVEN);
  }

  @EventHandler
  public void onLeave(PlayerQuitEvent event) {
    plugin.getPlayerDataManager().saveData(event.getPlayer().getUniqueId());

    plugin.getFilterList().remove(event.getPlayer().getUniqueId());
  }

}
