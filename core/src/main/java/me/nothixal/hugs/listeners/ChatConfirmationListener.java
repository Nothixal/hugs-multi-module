package me.nothixal.hugs.listeners;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatConfirmationListener implements Listener {

  private final HugsPlugin plugin;

  public ChatConfirmationListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onAsyncChat(final AsyncPlayerChatEvent event) {
    final Player player = event.getPlayer();
    final String msg = event.getMessage();

    if (!plugin.getHugsCommand().getTyping().containsKey(player)) {
      return;
    }

    event.setCancelled(true);

    if (msg.equalsIgnoreCase("no") || msg.equalsIgnoreCase("n") || msg.equalsIgnoreCase("cancel")) {
      plugin.getHugsCommand().getTyping().remove(player);
      player.sendMessage(ChatUtils.colorChat(Messages.CANCELLED_FULL_DATA_PURGE.getLangValue()));
      return;
    }

    if (msg.equalsIgnoreCase("yes") || msg.equalsIgnoreCase("y") || msg.equalsIgnoreCase("confirm")) {
      String value = plugin.getHugsCommand().getTyping().get(player);

      if (value.equalsIgnoreCase("all") || value.equalsIgnoreCase("everyone") || value
          .equalsIgnoreCase("*")) {
        plugin.getPlayerDataManager().purgeDatabase();
        plugin.getHugsCommand().getTyping().remove(player);
        player.sendMessage(ChatUtils.colorChat(Messages.PURGED_ALL_DATA.getLangValue()));
        return;
      }

      plugin.getPlayerDataManager().purgeData(plugin.getHugsCommand().getTyping2().get(player.getUniqueId()));
      plugin.getHugsCommand().getTyping().remove(player);
      player.sendMessage(ChatUtils.colorChat(Messages.PURGED_PLAYER_DATA.getLangValue().replace("%target%", value)));
    }
  }
}
