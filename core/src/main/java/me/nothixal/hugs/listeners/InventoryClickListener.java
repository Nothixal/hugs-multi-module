package me.nothixal.hugs.listeners;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.guis.ConfirmationGUI;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryClickListener implements Listener {

  private final HugsPlugin plugin;

  public InventoryClickListener(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    String title = event.getView().getTitle();
    Inventory inv = event.getInventory();
    ItemStack is = event.getCurrentItem();
    Player p = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (is == null) {
      return;
    }

    if (!is.hasItemMeta()) {
      return;
    }

//    if (inv == plugin.getGUIManager().getHelpGUI()) {
//      event.setCancelled(true);
//      new HelpGUI(plugin).clicked(p, slot, is, inv);
//      return;
//    }
//
//    if (inv == plugin.getGUIManager().getLeaderboardGUI()) {
//      event.setCancelled(true);
//      LeaderboardGUI leaderboardGUI = new LeaderboardGUI(plugin);
//      leaderboardGUI.clicked(p, slot, is, inv);
//    }

    if (title.equals(ChatUtils.colorChat(Messages.CONFIRMATION_GUI_NAME.getLangValue()))) {
      event.setCancelled(true);
      ConfirmationGUI confirmationGUI = new ConfirmationGUI(plugin);
      confirmationGUI.clicked(p, slot, is, inv);
    }

  }

  @EventHandler
  public void onInventoryClose(InventoryCloseEvent event) {
    if (event.getView().getTitle().equals(ChatUtils.colorChat(Messages.CONFIRMATION_GUI_NAME.getLangValue()))) {
      if (plugin.getHugsCommand().getConfirming().containsKey(event.getPlayer())) {
        plugin.getHugsCommand().getConfirming().remove(event.getPlayer());
      }


    }

    if (event.getView().getTitle().equals(ChatUtils.colorChat(Messages.LEADERBOARD_GUI_NAME.getLangValue()))) {
      //plugin.getPlayerDataManager().getSortedData().clear();
    }
  }
}
