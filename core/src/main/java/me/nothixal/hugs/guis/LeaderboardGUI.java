package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Filter;
import me.nothixal.hugs.enums.leaderboard.FilterState;
import me.nothixal.hugs.managers.items.ItemBuilder;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class LeaderboardGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  private Map<UUID, FilterState> filter = new HashMap<>();

  public LeaderboardGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Leaderboard"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItem(XMaterial.ELYTRA, "&aLeaderboard"));

    for (int i = 20; i < 24; i++) {
      inventory.setItem(20, itemManager.createItem(XMaterial.PLAYER_HEAD, ChatUtils.colorChat("&cLoading...")));
    }

    inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
        Collections.singletonList("&7Hugs Given: &b" + plugin.getPlayerDataManager().getTotalHugsGiven())));

    //TODO: Change this back when I get a proper chance.
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));

    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
        Arrays.asList(
            "",
            "&bNormal Hugs",
            "&7Mass Hugs",
            "&7Self Hugs",
            "",
            "&eClick to switch filter!",
            "&7Right-Click to go backwards!")));

    ItemBuilder builder = new ItemBuilder(XMaterial.BLUE_CARPET);

    ItemStack testItem = builder.setDisplayName("&cTest").build();

//    inventory.setItem(32, testItem);

    plugin.getGUIManager().getLeaderboardGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId()));

    new BukkitRunnable() {

      @Override
      public void run() {
        File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

        File[] files = playerDataDirectory.listFiles();

        if (files == null) {
          return;
        }

        Map<UUID, Integer> unSortedMap = new HashMap<>();

        for (File file : files) {
          if (file.isDirectory()) {
            continue;
          }

          if (!file.getName().endsWith(".yml")) {
            continue;
          }

          UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

          unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));
          plugin.getPlayerDataManager().getHugsGiven(uuid);
        }

        //LinkedHashMap preserve the ordering of elements in which they are inserted
        LinkedHashMap<UUID, Integer> sortedMap = new LinkedHashMap<>();

        unSortedMap.entrySet()
            .stream()
            .limit(5)
            .sorted(Entry.<UUID, Integer>comparingByValue().reversed())
            .forEachOrdered(uuidIntegerEntry -> {
              if (uuidIntegerEntry.getValue() > 0) {
                sortedMap.put(uuidIntegerEntry.getKey(), uuidIntegerEntry.getValue());
              }
            });

        //System.out.println("Sorted Map   : " + sortedMap);

        int addMe = 20;
        int rank = 1;


        if (sortedMap.isEmpty()) {
          for (int i = 0; i < 5 - sortedMap.size(); i++) {
            inventory.setItem(addMe++, itemManager
                .createItem(XMaterial.BARRIER, ChatUtils.colorChat("&cNo one has given any hugs! :("), Collections.emptyList()));
          }
          return;
        }

        for (Entry<UUID, Integer> entry : sortedMap.entrySet()) {
          inventory.setItem(addMe++, itemManager.createSkullItem(entry.getKey(),
              ChatUtils.colorChat("&a" + rank++ + ". " + Bukkit.getServer().getOfflinePlayer(entry.getKey()).getName()),
              Collections.singletonList("&7Hugs Given" + ": &e" + entry.getValue())));
        }

        if (sortedMap.size() < 5) {
          for (int i = 0; i < 5 - sortedMap.size(); i++) {
            inventory.setItem(addMe++, itemManager.createItem(XMaterial.PLAYER_HEAD, "&cEmpty Space"));
          }
        }
      }
    }.runTaskAsynchronously(plugin);

  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();

    if (inv != plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (event.getSlot() == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (!item.hasItemMeta()) {
      return;
    }

    if (!item.getItemMeta().hasLore()) {
      return;
    }

    if (event.isLeftClick() && item.getType() == Material.HOPPER) {
      if (plugin.getFilterList().containsKey(player.getUniqueId())) {
        plugin.getFilterList().put(player.getUniqueId(), plugin.getFilterList().get(player.getUniqueId()).getNext());
      } else {
        plugin.getFilterList().put(player.getUniqueId(), Filter.HUGS_RECEIVED);
      }

      doWork(plugin.getGUIManager().getLeaderboardGUIs().get(player.getUniqueId()), player);
      return;
    }

    if (item.getItemMeta().getLore().equals(Collections.singletonList(ChatUtils.colorChat("&bHugs Given")))) {
      openGUI(player);
      return;
    }

    if (item.getItemMeta().getLore().equals(Collections.singletonList(ChatUtils.colorChat("&bHugs Received")))) {
      openGUI(player);
      return;
    }

    if (item.getItemMeta().getLore().equals(Collections.singletonList(ChatUtils.colorChat("&bMass hugs Given")))) {
      openGUI(player);
      return;
    }

    if (item.getItemMeta().getLore().equals(Collections.singletonList(ChatUtils.colorChat("&bMass hugs Received")))) {
      openGUI(player);
      return;
    }
  }

  private void doWork(Inventory inventory, Player player) {
    new BukkitRunnable() {

      @Override
      public void run() {
        File playerDataDirectory = new File(plugin.getFileManager().getPlayerDataDirectory());

        File[] files = playerDataDirectory.listFiles();

        if (files == null) {
          return;
        }

        Map<UUID, Integer> unSortedMap = new HashMap<>();

        for (File file : files) {
          if (file.isDirectory()) {
            continue;
          }

          if (!file.getName().endsWith(".yml")) {
            continue;
          }

          UUID uuid = UUID.fromString(file.getName().replace(".yml", ""));

          if (!plugin.getFilterList().containsKey(uuid)) {
            continue;
          }

          // Filter Button
          if (filter.containsKey(player.getUniqueId())) {
            filter.put(player.getUniqueId(), filter.get(player.getUniqueId()).getNext());
          } else {
            filter.put(player.getUniqueId(), FilterState.NORMAL);
          }

          if (plugin.getFilterList().get(uuid).getValue().equalsIgnoreCase("Hugs Given")) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));

            inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
                Collections.singletonList("&7Hugs Given: &b" + plugin.getPlayerDataManager().getTotalHugsGiven())));

            inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
                Arrays.asList(
                    "",
                    "&bNormal Hugs",
                    "&7Mass Hugs",
                    "&7Self Hugs",
                    "",
                    "&eClick to switch filter!",
                    "&7Right-Click to go backwards!")));

            test(inventory, player, unSortedMap, "&cNo one has given any hugs! :(");

          } else if (plugin.getFilterList().get(uuid).getValue().equalsIgnoreCase("Hugs Received")) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsReceived(uuid));

            inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
                Collections.singletonList("&7Hugs Received: &b" + plugin.getPlayerDataManager().getTotalHugsGiven())));

            inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
                Arrays.asList(
                    "&7Filtering by: &bHugs Received",
                    "&7Sorting type: &bNormal",
                    "",
                    "&eLEFT CLICK &7to change",
                    "&7between all available",
                    "&7sorting options.",
                    "",
                    "&eRIGHT CLICK &7to reverse",
                    "&7the current order!")));

            test(inventory, player, unSortedMap, "&cNo one has received any hugs! :(");

          } else if (plugin.getFilterList().get(uuid).getValue().equalsIgnoreCase("mass hugs given")) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsGiven(uuid));

            inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
                Collections.singletonList("&7Mass Hugs Given: &b" + plugin.getPlayerDataManager().getTotalMassHugsGiven())));

            inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
                Arrays.asList(
                    "&7Filtering by: &bMass Hugs Given",
                    "&7Sorting type: &bNormal",
                    "",
                    "&eLEFT CLICK &7to change",
                    "&7between all available",
                    "&7sorting options.",
                    "",
                    "&eRIGHT CLICK &7to reverse",
                    "&7the current order!")));

            test(inventory, player, unSortedMap, "&cNo one has given any mass hugs! :(");

          } else if (plugin.getFilterList().get(uuid).getValue().equalsIgnoreCase("mass hugs received")) {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getMassHugsReceived(uuid));

            inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
                Collections.singletonList("&7Mass Hugs Received: &b" + plugin.getPlayerDataManager().getTotalHugsGiven())));

            inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
                Arrays.asList(
                    "&7Filtering by: &bMass Hugs Received",
                    "&7Sorting type: &bNormal",
                    "",
                    "&eLEFT CLICK &7to change",
                    "&7between all available",
                    "&7sorting options.",
                    "",
                    "&eRIGHT CLICK &7to reverse",
                    "&7the current order!")));

            test(inventory, player, unSortedMap, "&cNo one has received any mass hugs! :(");

          } else if(plugin.getFilterList().get(uuid).getValue().equalsIgnoreCase("self hugs given")) {
            inventory.setItem(31, itemManager.createItem(XMaterial.BOOKSHELF, "&eGlobal Total",
                Collections.singletonList("&7Self Hugs Given: &b" + plugin.getPlayerDataManager().getTotalHugsGiven())));

            inventory.setItem(51, itemManager.createItem(XMaterial.HOPPER, "&aChange Filter",
                Arrays.asList(
                    "&7Filtering by: &bSelf Hugs Given",
                    "&7Sorting type: &bNormal",
                    "",
                    "&eLEFT CLICK &7to change",
                    "&7between all available",
                    "&7sorting options.",
                    "",
                    "&eRIGHT CLICK &7to reverse",
                    "&7the current order!")));

            test(inventory, player, unSortedMap, "&cNo one has given any self hugs! :(");
          } else {
            unSortedMap.put(uuid, plugin.getPlayerDataManager().getHugsGiven(uuid));
          }
        }

//        //LinkedHashMap preserve the ordering of elements in which they are inserted
//        LinkedHashMap<UUID, Integer> sortedMap = new LinkedHashMap<>();
//
//        unSortedMap.entrySet()
//            .stream()
//            .limit(5)
//            .sorted(Entry.comparingByValue())
//            .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
//
//        System.out.println("Sorted Map   : " + sortedMap);
//
//        int addMe = 20;
//        int rank = 1;
//
//        if (sortedMap.isEmpty()) {
//          for (int i = 0; i < 5 - sortedMap.size(); i++) {
//            inventory.setItem(addMe++, itemManager
//                .createItem(XMaterial.BARRIER, ChatUtils.colorChat("&cNo one has given any hugs! :("), Collections.emptyList()));
//          }
//
//          return;
//        }
//
//        for (Entry<UUID, Integer> entry : sortedMap.entrySet()) {
//          inventory.setItem(addMe++, itemManager.createSkullItem(entry.getKey(),
//              ChatUtils.colorChat("&a" + rank++ + ". " + Bukkit.getServer().getOfflinePlayer(entry.getKey()).getName()),
//              Collections.singletonList("&7" + plugin.getFilterList().get(player.getUniqueId()).getValue() + ": &e" + entry.getValue())));
//        }
//
//        if (sortedMap.size() < 5) {
//          for (int i = 0; i < 5 - sortedMap.size(); i++) {
//            inventory.setItem(addMe++, itemManager.createItem(XMaterial.PLAYER_HEAD, "&cEmpty Space"));
//          }
//        }
//
//        player.updateInventory();
      }
    }.runTaskAsynchronously(plugin);
  }

  private void test(Inventory inventory, Player player, Map<UUID, Integer> unsortedMap, String noDataFound) {
    //LinkedHashMap preserve the ordering of elements in which they are inserted
    LinkedHashMap<UUID, Integer> sortedMap = new LinkedHashMap<>();

    unsortedMap.entrySet()
        .stream()
        .limit(5)
        .sorted(Entry.<UUID, Integer>comparingByValue().reversed())
        .forEachOrdered(uuidIntegerEntry -> {
          if (uuidIntegerEntry.getValue() > 0) {
            sortedMap.put(uuidIntegerEntry.getKey(), uuidIntegerEntry.getValue());
          }
        });

    //System.out.println("Sorted Map   : " + sortedMap);

    int addMe = 20;
    int rank = 1;

    if (sortedMap.isEmpty()) {
      for (int i = 0; i < 5 - sortedMap.size(); i++) {
        inventory.setItem(addMe++, itemManager
            .createItem(XMaterial.BARRIER, ChatUtils.colorChat(noDataFound), Collections.emptyList()));
      }

      return;
    }

    for (Entry<UUID, Integer> entry : sortedMap.entrySet()) {
      inventory.setItem(addMe++, itemManager.createSkullItem(entry.getKey(),
          ChatUtils.colorChat("&a" + rank++ + ". " + Bukkit.getServer().getOfflinePlayer(entry.getKey()).getName()),
          Collections.singletonList("&7" + plugin.getFilterList().get(player.getUniqueId()).getValue() + ": &e" + entry.getValue())));
    }

    if (sortedMap.size() < 5) {
      for (int i = 0; i < 5 - sortedMap.size(); i++) {
        inventory.setItem(addMe++, itemManager.createItem(XMaterial.PLAYER_HEAD, "&cEmpty Space"));
      }
    }

    player.updateInventory();
  }
}
