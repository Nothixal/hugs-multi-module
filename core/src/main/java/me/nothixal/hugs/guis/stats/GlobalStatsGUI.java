package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GlobalStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public GlobalStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Global Stats"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

//    if (plugin.getPlayerDataManager().getDeveloperHugsGiven(player.getUniqueId()) > 0) {
//      inventory.setItem(2, itemManager.getEros(player.getUniqueId()));
//    }

    inventory.setItem(4, itemManager.createItem(XMaterial.SPRUCE_SIGN, "&aGlobal Stats"));

    inventory.setItem(21, itemManager.createItem(XMaterial.POPPY, "&eGiven",
        Collections.singletonList("&7" + plugin.getPlayerDataManager().getTotalHugsGiven())));

    inventory.setItem(22, itemManager.createItem(XMaterial.ROSE_BUSH, "&eMass Given",
        Collections.singletonList("&7" + plugin.getPlayerDataManager().getTotalMassHugsGiven())));

    //Todo: Create getSelfHugs method in the Player Data Manager.
    inventory.setItem(23, itemManager.createItem(XMaterial.ARMOR_STAND, "&eSelf",
        Collections.singletonList("&7" + plugin.getPlayerDataManager().getTotalSelfHugsGiven())));

    inventory.setItem(30, itemManager.createItem(XMaterial.RABBIT_HIDE, "&eReceived",
        Collections.singletonList("&7Coming Soon")));

    inventory.setItem(31, itemManager.createItem(XMaterial.LEATHER, "&eMass Received",
        Collections.singletonList("&7Coming Soon")));

//    inventory.setItem(32, itemManager.createItem(XMaterial.SUNFLOWER, "&eComing Soon"));


//    inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, "&eFirst Hug",
//        Arrays.asList("&7Date: &f<Date>", "&7Given To: &b<First>")));

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getTemporaryGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getTemporaryGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != this.inventory) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }
  }
}
