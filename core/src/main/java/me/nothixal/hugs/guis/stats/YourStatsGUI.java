package me.nothixal.hugs.guis.stats;

import com.cryptomorin.xseries.XMaterial;
import java.util.Arrays;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourStatsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public YourStatsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit
        .createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Your Stats"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

//     TODO: Remove output.
//    System.out.println(plugin.getPlayerDataManager().getDatabaseType().isLocal());
//    System.out.println(plugin.getPlayerDataManager().getDatabaseType().isGlobal());

    if (plugin.getPlayerDataManager().getDatabaseType().isLocal()) {
      // Show local database GUI.
      inventory.setItem(4, itemManager.createSkullItem(player.getUniqueId(), "&e" + player.getName(),
          Arrays.asList(
              "&7Hugs Given: &e" + plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId()),
              "&7Hugs Received: &e" + plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId()),
              "",
              "&7Mass Given: &6" + plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId()),
              "&7Mass Received: &6" + plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId())
          )));

      inventory.setItem(21, itemManager.createItem(XMaterial.POPPY, "&eGiven",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId()))));

      inventory.setItem(22, itemManager.createItem(XMaterial.ROSE_BUSH, "&eMass Given",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId()))));

      //Todo: Create getSelfHugs method in the Player Data Manager.
      inventory.setItem(23, itemManager.createItem(XMaterial.ARMOR_STAND, "&eSelf",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getSelfHugs(player.getUniqueId()))));

      inventory.setItem(30, itemManager.createItem(XMaterial.RABBIT_HIDE, "&eReceived",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId()))));

      inventory.setItem(31, itemManager.createItem(XMaterial.LEATHER, "&eMass Received",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId()))));

      inventory.setItem(32, itemManager.createItem(XMaterial.SUNFLOWER, "&eComing Soon"));

//    inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, "&eFirst Hug",
//        Arrays.asList("&7Date: &f<Date>", "&7Given To: &b<First>")));

      inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
      inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));
    } else if (plugin.getPlayerDataManager().getDatabaseType().isGlobal()) {
      // Show global database GUI.
      inventory.setItem(4, itemManager.createSkullItem(player.getUniqueId(), "&e" + player.getName(),
          Arrays.asList(
              "&7Achievements Unlocked: &e<Current>/<Total> &8(percentage)",
              "",
              "&7Hugs Given: &e" + plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId()),
              "&7Hugs Received: &e" + plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId()),
              "",
              "&7Mass Given: &6" + plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId()),
              "&7Mass Received: &6" + plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId()),
              "",
              "&7Unique Hugs: &d"
          )));

      inventory.setItem(20, itemManager.createItem(XMaterial.OAK_SAPLING, "&eComing Soon"));
      inventory.setItem(29, itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, "&7Coming Soon After"));

      inventory.setItem(22, itemManager.createItem(XMaterial.POPPY, "&eGiven",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId()))));

      inventory.setItem(23, itemManager.createItem(XMaterial.ROSE_BUSH, "&eMass Given",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId()))));

      //Todo: Create getSelfHugs method in the Player Data Manager.
      inventory.setItem(24, itemManager.createItem(XMaterial.ARMOR_STAND, "&eSelf",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getSelfHugs(player.getUniqueId()))));

      inventory.setItem(31, itemManager.createItem(XMaterial.RABBIT_HIDE, "&eReceived",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId()))));

      inventory.setItem(32, itemManager.createItem(XMaterial.LEATHER, "&eMass Received",
          Collections.singletonList("&7" + plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId()))));

      inventory.setItem(33, itemManager.createItem(XMaterial.SUNFLOWER, "&eComing Soon"));

//    inventory.setItem(32, itemManager.createItem(XMaterial.OAK_SAPLING, "&eFirst Hug",
//        Arrays.asList("&7Date: &f<Date>", "&7Given To: &b<First>")));

      inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
      inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));
    } else {
      System.out.println("ERROR: Database is neither local or global... How???");
    }

//    if (plugin.getPlayerDataManager().getDeveloperHugsGiven(player.getUniqueId()) > 0) {
//      inventory.setItem(2, itemManager.getEros(player.getUniqueId()));
//    }

    plugin.getGUIManager().getYourStatsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourStatsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getYourStatsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }
  }

}
