package me.nothixal.hugs.guis.settings.backend;

import me.nothixal.hugs.HugsPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;

public class BroadcastSettingsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;

  public BroadcastSettingsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.inventory = plugin.getGUIManager().getBroadcastSettingsGUI();
  }

  public void openGUI(Player player) {


    player.openInventory(inventory);
  }

}
