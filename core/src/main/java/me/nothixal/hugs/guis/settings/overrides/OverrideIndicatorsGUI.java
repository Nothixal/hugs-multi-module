package me.nothixal.hugs.guis.settings.overrides;

import com.cryptomorin.xseries.XMaterial;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class OverrideIndicatorsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public OverrideIndicatorsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Override ➜ Indicators"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.RED_STAINED_GLASS_PANE, " "));

    // Row 1
    ItemStack item = itemManager.createItem(XMaterial.FLOWER_BANNER_PATTERN, "&eIndicator Overrides");
    ItemMeta meta = item.getItemMeta();
    meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
    meta.addItemFlags(ItemFlag.HIDE_DESTROYS);
    meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
    meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
    meta.addItemFlags(ItemFlag.HIDE_PLACED_ON);
    item.setItemMeta(meta);

    inventory.setItem(4, item);

    //inventory.setItem(4, itemManager.createItem(Material.FLOWER_BANNER_PATTERN, "&eYour Indicators"));

    // Row 3
    inventory.setItem(20, itemManager.createItem(XMaterial.PAPER, "&aChat Indicator"));
    inventory.setItem(29, itemManager.createItem(XMaterial.LIME_DYE, "&aChat Indicator",
        Collections.singletonList("&7Click to disable!")));

    inventory.setItem(21, itemManager.createItemNoAttrib(XMaterial.DIAMOND_SWORD, "&aActionbar Indicator"));
    inventory.setItem(30, itemManager.createItemNoAttrib(XMaterial.LIME_DYE, "&aActionbar Indicator",
        Collections.singletonList("&7Click to disable!")));

    inventory.setItem(22, itemManager.createItem(XMaterial.DRAGON_HEAD, "&aBossbar Indicator"));
    inventory.setItem(31, itemManager.createItem(XMaterial.LIME_DYE, "&aBossbar Indicator",
        Collections.singletonList("&7Click to disable!")));

    inventory.setItem(23, itemManager.createItem(XMaterial.PAINTING, "&aTitles Indicator"));
    inventory.setItem(32, itemManager.createItem(XMaterial.LIME_DYE, "&aTitles Indicator",
        Collections.singletonList("&7Click to disable!")));

    inventory.setItem(24, itemManager.createItem(XMaterial.BREAD, "&aToast Indicator"));
    inventory.setItem(33, itemManager.createItem(XMaterial.LIME_DYE, "&aToast Indicator",
        Collections.singletonList("&7Click to disable!")));

//    PlayerDataManager dataManager = plugin.getPlayerDataManager();
//
//    if (dataManager.getIndicator(player.getUniqueId(), "chat")) {
//      inventory.setItem(20, plugin.getItemToggles().CHAT_INDICATOR_ENABLED_ICON);
//      inventory.setItem(29, plugin.getItemToggles().CHAT_INDICATOR_ENABLED_BUTTON);
//    } else {
//      inventory.setItem(20, plugin.getItemToggles().CHAT_INDICATOR_DISABLED_ICON);
//      inventory.setItem(29, plugin.getItemToggles().CHAT_INDICATOR_DISABLED_BUTTON);
//    }
//
//    if (dataManager.getIndicator(player.getUniqueId(), "actionbar")) {
//      inventory.setItem(21, plugin.getItemToggles().ACTIONBAR_INDICATOR_ENABLED_ICON);
//      inventory.setItem(30, plugin.getItemToggles().ACTIONBAR_INDICATOR_ENABLED_BUTTON);
//    } else {
//      inventory.setItem(21, plugin.getItemToggles().ACTIONBAR_INDICATOR_DISABLED_ICON);
//      inventory.setItem(30, plugin.getItemToggles().ACTIONBAR_INDICATOR_DISABLED_BUTTON);
//    }
//
//    if (dataManager.getIndicator(player.getUniqueId(), "bossbar")) {
//      inventory.setItem(22, plugin.getItemToggles().BOSSBAR_INDICATOR_ENABLED_ICON);
//      inventory.setItem(31, plugin.getItemToggles().BOSSBAR_INDICATOR_ENABLED_BUTTON);
//    } else {
//      inventory.setItem(22, plugin.getItemToggles().BOSSBAR_INDICATOR_DISABLED_ICON);
//      inventory.setItem(31, plugin.getItemToggles().BOSSBAR_INDICATOR_DISABLED_BUTTON);
//    }
//
//    if (dataManager.getIndicator(player.getUniqueId(), "titles")) {
//      inventory.setItem(23, plugin.getItemToggles().TITLES_INDICATOR_ENABLED_ICON);
//      inventory.setItem(32, plugin.getItemToggles().TITLES_INDICATOR_ENABLED_BUTTON);
//    } else {
//      inventory.setItem(23, plugin.getItemToggles().TITLES_INDICATOR_DISABLED_ICON);
//      inventory.setItem(32, plugin.getItemToggles().TITLES_INDICATOR_DISABLED_BUTTON);
//    }
//
//    if (dataManager.getIndicator(player.getUniqueId(), "toast")) {
//      inventory.setItem(24, plugin.getItemToggles().TOAST_INDICATOR_ENABLED_ICON);
//      inventory.setItem(33, plugin.getItemToggles().TOAST_INDICATOR_ENABLED_BUTTON);
//    } else {
//      inventory.setItem(24, plugin.getItemToggles().TOAST_INDICATOR_DISABLED_ICON);
//      inventory.setItem(33, plugin.getItemToggles().TOAST_INDICATOR_DISABLED_BUTTON);
//    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&c&lClose"));

    //plugin.getGUIManager().getYourIndicatorGUIs().put(player.getUniqueId(), inventory);
    //player.openInventory(plugin.getGUIManager().getYourIndicatorGUIs().get(player.getUniqueId()));

    player.openInventory(inventory);
  }

}
