package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.util.Arrays;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ConfirmationGUI {

  private final HugsPlugin plugin;
  private ItemManager itemManager;

  public ConfirmationGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
  }

  public void openGUI(Player player) {
    Inventory inv = Bukkit.createInventory(null, 9 * 3, ChatUtils.colorChat(Messages.CONFIRMATION_GUI_NAME.getLangValue()));

    if (plugin.getHugsCommand().getConfirming2().containsKey(player.getUniqueId())) {
      UUID offlineTarget = plugin.getHugsCommand().getConfirming2().get(player.getUniqueId());
      OfflinePlayer target = Bukkit.getOfflinePlayer(offlineTarget);

      inv.setItem(13, itemManager.createSkullItem(offlineTarget, ChatUtils.colorChat("&3&lAre you sure?"),
          Arrays.asList("&7Doing this will result in &3%player%'s"
              .replace("%player%", "" + target.getName()), "&7data being deleted!", "", "&7Do you wish to continue?")));
    } else {
      inv.setItem(13, itemManager.createItem(XMaterial.ENDER_EYE, ChatUtils.colorChat("&3&lAre you sure?"), Arrays.asList("&7Doing this will result in all", "&7player data being deleted!", "", "&7Do you wish to continue?")));
    }

    inv.setItem(0, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(1, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(2, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));

    inv.setItem(9, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(10, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(11, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));

    inv.setItem(18, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(19, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));
    inv.setItem(20, itemManager.createItem(XMaterial.LIME_STAINED_GLASS, ChatUtils.colorChat("&a&lCONFIRM")));

    inv.setItem(6, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(7, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(8, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));

    inv.setItem(15, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(16, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(17, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));

    inv.setItem(24, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(25, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));
    inv.setItem(26, itemManager.createItem(XMaterial.RED_STAINED_GLASS, ChatUtils.colorChat("&c&lCANCEL")));

    player.openInventory(inv);
  }

  public void clicked(final Player player, int slot, ItemStack item, Inventory inv) {
    if (item.getItemMeta().getDisplayName().equals(ChatUtils.colorChat("&c&lCANCEL"))) {
      player.sendMessage(ChatUtils.colorChat(Messages.CANCELLED_FULL_DATA_PURGE.getLangValue()));
      player.closeInventory();
      return;
    }

    if (item.getItemMeta().getDisplayName().equals(ChatUtils.colorChat("&a&lCONFIRM"))) {
      if (plugin.getHugsCommand().getConfirming().containsKey(player)) {
        String value = plugin.getHugsCommand().getConfirming().get(player);

        plugin.getPlayerDataManager().purgeData(plugin.getHugsCommand().getConfirming2().get(player.getUniqueId()));
        plugin.getHugsCommand().getConfirming().remove(player);
        player.sendMessage(ChatUtils.colorChat(Messages.PURGED_PLAYER_DATA.getLangValue().replace("%target%", value)));
        player.closeInventory();
        return;
      }

      plugin.getPlayerDataManager().purgeDatabase();
      player.sendMessage(ChatUtils.colorChat(Messages.PURGED_ALL_DATA.getLangValue()));
      player.closeInventory();
    }

  }

}
