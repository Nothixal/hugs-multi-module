package me.nothixal.hugs.guis.settings;

import com.cryptomorin.xseries.XMaterial;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.guis.settings.backend.BackendSettingsGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PluginSettingsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public PluginSettingsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = plugin.getGUIManager().getPluginSettingsGUI();
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItem(XMaterial.HONEYCOMB, "&dPlugin Settings"));

    inventory.setItem(11, itemManager.createItem(XMaterial.ANVIL, "&aGeneral Settings"));
    inventory.setItem(13, itemManager.createItem(XMaterial.BOOK, "&aDefaults"));
    inventory.setItem(15, itemManager.createItem(XMaterial.COMPARATOR, "&aOverrides"));

    for (int i = 18; i < 26; i++) {
      inventory.setItem(i, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    }


    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&c&lClose"));

    inventory.setItem(52, itemManager.createItem(XMaterial.CONDUIT, "&bBackend Settings"));

    player.openInventory(inventory);
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != inventory) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.STRUCTURE_VOID, "&c&lClose"))) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 52) {
      new BackendSettingsGUI(plugin).openGUI(player);
      return;
    }
  }

}
