package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class CommandsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;
  private String guiPrefix;

  public CommandsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.guiPrefix = plugin.getGUIManager().getGuiPrefix();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Commands Menu"));
  }

  public void openGUI(Player player, int category) {
    ItemStack generalCommands = itemManager.createItem(XMaterial.OAK_SIGN, "&aGeneral Commands");
    ItemStack informationCommands = itemManager.createItem(XMaterial.BOOK, "&aInformation Commands");
    ItemStack settingsCommands = itemManager.createItem(XMaterial.COMPARATOR, "&aSettings Commands");
    ItemStack adminCommands = itemManager.createItem(XMaterial.HONEYCOMB, "&aAdmin Commands");

    if (category == 0) {
      this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8General Commands"));
      InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

      ItemStack item = itemManager.setGlowing(generalCommands, true);
      inventory.setItem(9, item);
      inventory.setItem(18, informationCommands);
      inventory.setItem(27, settingsCommands);
      inventory.setItem(36, adminCommands);
    } else if (category == 1) {
      this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Information Commands"));
      InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

      ItemStack item = itemManager.setGlowing(informationCommands, true);
      inventory.setItem(9, generalCommands);
      inventory.setItem(18, item);
      inventory.setItem(27, settingsCommands);
      inventory.setItem(36, adminCommands);
    } else if (category == 2) {
      this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Settings Commands"));
      InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

      ItemStack item = itemManager.setGlowing(settingsCommands, true);
      inventory.setItem(9, generalCommands);
      inventory.setItem(18, informationCommands);
      inventory.setItem(27, item);
      inventory.setItem(36, adminCommands);
    } else if (category == 3) {
      this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(guiPrefix + "&8Admin Commands"));
      InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

      ItemStack item = itemManager.setGlowing(adminCommands, true);
      inventory.setItem(9, generalCommands);
      inventory.setItem(18, informationCommands);
      inventory.setItem(27, settingsCommands);
      inventory.setItem(36, item);
    }

    //InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItem(XMaterial.SWEET_BERRIES, "&bCommands"));

    //Left Glass Panes
    inventory.setItem(10, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(19, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(28, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(37, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    //Right Glass Panes
    inventory.setItem(16, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(17, itemManager.createItem(XMaterial.AIR));

    inventory.setItem(25, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(26, itemManager.createItem(XMaterial.LIME_DYE, "&aToggle Permission View", Collections.singletonList("&7Coming Soon")));
    inventory.setItem(34, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(35, itemManager.createItem(XMaterial.CYAN_DYE, "&aChange Color", Collections.singletonList("&7Coming Soon")));

    inventory.setItem(43, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));
    inventory.setItem(44, itemManager.createItem(XMaterial.AIR));

    if (category == 0) {
      List<ItemStack> items = new ArrayList<>();
      items.add(itemManager.createItem(XMaterial.PAPER, "&b&l/hug <player>", Arrays.asList(
          "&7Description: &fGives a hug to the designated player.",
          "&7Usages:",
          " &f/hug <player>",
          " &f/hug [@a, *, all, everyone, everybody]",
          " &f/hug [@r, rand, random, someone, somebody]")));
      items.add(itemManager.createItem(XMaterial.PAPER, "&b&l/hugs",
          Collections.singletonList("&7Description: &fBase command for the plugin / Plugin Information")));
      items.add(itemManager.createItem(XMaterial.PAPER, "&b&l/hugs help <page>", Arrays.asList(
          "&7Description: &fShows the help page.",
          "&7Usages:",
          " &f/hugs help 1",
          " &f/hugs help 2")));

      updateInventory(player, inventory, items);
    } else if (category == 1) {
      List<ItemStack> items = new ArrayList<>();
      items.add(itemManager.createItem(XMaterial.BOOK, "&b&l/hugs total",
          Collections.singletonList("&7Description: &fShows the total amount of hugs given.")));
      items.add(itemManager.createItem(XMaterial.BOOK, "&b&l/hugs leaderboard",
          Collections.singletonList("&7Description: &fShows the hugs leaderboard.")));
      items.add(itemManager.createItem(XMaterial.BOOK, "&b&l/hugs info <player>", Arrays.asList(
          "&7Description: &fShows a player's hug stats.",
          "&7Usages:",
          " &f/hugs info Notch",
          " &f/hugs info jeb_",
          " &f/hugs info Dinnerbone")));

      updateInventory(player, inventory, items);
    } else if (category == 2) {
      List<ItemStack> items = new ArrayList<>();

      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&lComing Soon"));
      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&lComing Soon"));
      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&lComing Soon"));

//      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&l/hugs sounds <setting>"));
//      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&l/hugs particles <setting>"));
//      items.add(itemManager.createItemNoAttrib(XMaterial.GLOBE_BANNER_PATTERN, "&b&l/hugs indicators <settings>"));

      updateInventory(player, inventory, items);
    } else if (category == 3) {
      List<ItemStack> items = new ArrayList<>();
      items.add(itemManager.createItem(XMaterial.MAP, "&b&l/hugs purge <player>", Arrays.asList(
          "&7Description: &fClear a player's stats.",
          "&7Usages:",
          " &f/hugs purge Notch",
          " &f/hugs purge jeb_",
          " &f/hugs purge Dinnerbone")));
      items.add(itemManager.createItem(XMaterial.MAP, "&b&l/hugs reload",
          Collections.singletonList("&7Description: &fReloads the config.")));
      items.add(itemManager.createItem(XMaterial.MAP, "&b&l/hugs update",
          Collections.singletonList("&7Description: &fChecks for plugin updates.")));
      updateInventory(player, inventory, items);
    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getCommandsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getCommandsGUIs().get(player.getUniqueId()));
  }

  private void updateInventory(Player player, Inventory inventory, List<ItemStack> items) {
    new BukkitRunnable() {

      @Override
      public void run() {
        int start = 11;
        int end = 42;
        int forgotten = 0;

        List<Integer> forget = Arrays.asList(16, 17, 18, 19, 25, 26, 27, 28, 34, 35, 36, 37);

        for (int i = start; i <= end; i++) {
          if (forget.contains(i)) {
            forgotten++;
            continue;
          }

          int current = ((i - 22) + start) - forgotten;

          if (current >= items.size()) {
            continue;
          }

          inventory.setItem(i, items.get(current));
          player.updateInventory();
        }
      }
    }.runTaskAsynchronously(plugin);
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getCommandsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 9) {
      openGUI(player, 0);
      return;
    }

    if (slot == 18) {
      openGUI(player, 1);
      return;
    }

    if (slot == 27) {
      openGUI(player, 2);
      return;
    }

    if (slot == 36) {
      openGUI(player, 3);
      return;
    }
  }

}
