package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.util.Collections;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.guis.settings.player.YourPreferencesGUI;
import me.nothixal.hugs.guis.stats.GlobalStatsGUI;
import me.nothixal.hugs.guis.stats.YourStatsGUI;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class HelpGUI implements Listener {

  private final HugsPlugin plugin;
  private final Inventory inventory;
  private final ItemManager itemManager;

  public HelpGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit
        .createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Help Menu"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createHeader(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(1, itemManager.createNMSSkullItem(PluginConstants.H_TEXTURE_VALUE, " "));
    inventory.setItem(2, itemManager.createNMSSkullItem(PluginConstants.U_TEXTURE_VALUE, " "));
    inventory.setItem(3, itemManager.createNMSSkullItem(PluginConstants.G_TEXTURE_VALUE, " "));
    inventory.setItem(4, itemManager.createNMSSkullItem(PluginConstants.S_TEXTURE_VALUE, " "));

    inventory.setItem(6, itemManager.getEros(UUID.randomUUID()));
    inventory.setItem(7, itemManager.getSilvia(UUID.randomUUID()));

    inventory.setItem(20, itemManager.createItem(XMaterial.ELYTRA, "&aLeaderboards",
        Collections.singletonList("&7Click to view your ranking.")));

    inventory.setItem(21, itemManager.createItem(XMaterial.SPRUCE_SIGN, "&aGlobal Stats",
        Collections.singletonList("&7Click to view.")));

    inventory.setItem(22, itemManager.createSkullItem(player.getUniqueId(), "&aYour Stats",
        Collections.singletonList("&7Click to view your stats.")));

    inventory.setItem(23, itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, "&7Coming Soon"));
    inventory.setItem(24, itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, "&7Coming Soon"));

//    inventory.setItem(24, itemManager.createNMSSkullItem(
//        "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOThkYWExZTNlZDk0ZmYzZTMzZTFkNGM2ZTQzZjAyNGM0N2Q3OGE1N2JhNGQzOGU3NWU3YzkyNjQxMDYifX19",
//        "&aSelect Language",
//        Collections.singletonList("&7Change your language.")));

    inventory.setItem(30, itemManager.createItem(XMaterial.BOOKSHELF, "&aPlugin Lexicon",
        Collections.singletonList("&7Click to view info.")));

    inventory.setItem(31, itemManager.createItem(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE, "&7Coming Soon"));

    inventory.setItem(32, itemManager.createItem(XMaterial.SWEET_BERRIES, "&aCommands",
        Collections.singletonList("&7Click to view command help.")));

    inventory.setItem(39, itemManager.createItem(XMaterial.COMPARATOR, "&aYour Preferences",
        Collections.singletonList("&7Click to edit preferences.")));

    inventory.setItem(41, itemManager.createItem(XMaterial.HONEYCOMB, "&aAdmin Panel",
        Collections.singletonList("&7Click to edit settings.")));

    inventory.setItem(49, itemManager.createItem(XMaterial.RABBIT_STEW, "&dDonations",
        Collections.singletonList("&7Click to donate!")));

//    inventory.setItem(21, itemManager.createItem(XMaterial.BREWING_STAND, "&aComing Soon"));
//    inventory.setItem(30, itemManager.createItem(XMaterial.SWEET_BERRIES, "&aCommands",
//        Collections.singletonList("&7Click to view command help.")));
//    inventory.setItem(33, itemManager.createItem(XMaterial.HONEYCOMB, "&aPlugin Settings",
//        Collections.singletonList("&7Coming Soon!")));
//    //Collections.singletonList("&7Click to edit settings.")
//    inventory.setItem(47, itemManager.createItem(XMaterial.RABBIT_STEW, "&dDonations",
//        Collections.singletonList("&7Click to donate!")));
//    inventory.setItem(51, itemManager.createItem(XMaterial.BOOKSHELF, "&bPlugin Lexicon",
//        Collections.singletonList("&7Coming Soon!")
//        /*Collections.singletonList("&7Click to view.")*/));
//
//    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getHelpGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getHelpGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getHelpGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

//    // Close Button
//    if (slot == 49) {
//      player.closeInventory();
//      return;
//    }

    if (slot == 20) {
      if (player.hasPermission(Permissions.LEADERBOARD.getPermissionNode())) {
        new LeaderboardGUI(plugin).openGUI(player);
      } else {
        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
      }

      return;
    }

    if (slot == 21) {
      new GlobalStatsGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 22) {
      if (player.hasPermission(Permissions.STATS.getPermissionNode())) {
        new YourStatsGUI(plugin).openGUI(player);
      } else {
        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
      }

      return;
    }

    if (slot == 23) {
//      player.sendMessage("Coming Soon");
      return;
    }

    if (slot == 24) {
//      player.sendMessage("Language Menu");
      new YourLanguageGUI(plugin).openGUI(player);
      return;
    }

    if (slot == 30) {
      if (player.hasPermission(Permissions.MENU_PLUGIN_SETTINGS.getPermissionNode())) {
        new PluginLexiconGUI(plugin).openGUI(player);
      } else {
        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
      }

      return;
    }

    if (slot == 31) {
//      player.sendMessage("Coming Soon");
      return;
    }

    if (slot == 32) {
      new CommandsGUI(plugin).openGUI(player, 0);
      return;
    }

    if (slot == 39) {
      if (player.hasPermission(Permissions.PREFERENCES.getPermissionNode())) {
        new YourPreferencesGUI(plugin).openGUI(player);
      } else {
        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
      }

      return;
    }

    if (slot == 41) {
      new AdminPanelGUI(plugin).openGUI(player);
//      if (player.hasPermission(Permissions.SETTINGS.getPermissionNode())) {
//        new PluginSettingsGUI(plugin).openGUI(player);
//      } else {
//        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
//      }

      return;
    }

    if (slot == 49) {
      openDonateBook(player);
    }

    //TODO: Uncomment when GUI is completed.
//    if (slot == 51) {
//      if (player.hasPermission(Permissions.MENU_PLUGIN_SETTINGS.getPermissionNode())) {
//        new PluginLexiconGUI(plugin).openGUI(player);
//      } else {
//        InventoryUtils.showError(inv, slot, item, itemManager.createItem(XMaterial.BARRIER, "&cNo Permission!"), 3);
//      }
//
//    }

  }

  @SuppressWarnings("deprecation")
  private void openDonateBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;

    ComponentBuilder website = new ComponentBuilder("[DONATE HERE]").color(ChatColor.of(primaryColor)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PAYPAL_DONATE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
            new ComponentBuilder("Click to donate!").color(ChatColor.AQUA).create()));

    BaseComponent[] donateBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("        ").append("Donations")
        .append("\n\n")
        .append("If you enjoy using this plugin, consider donating.")
        .append("\n\n")
        .append("My work is free, but I strive to put my best effort into all my works.")
        .append("\n\n")
        .append("   ").append(website.create()).append("").reset()
        .create();

    if (bookMeta == null) {
      player.sendMessage("An error occurred creating the book.");
      return;
    }

    bookMeta.spigot().addPage(donateBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Discord Invite Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }
}
