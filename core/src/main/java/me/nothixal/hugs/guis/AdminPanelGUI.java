package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class AdminPanelGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public AdminPanelGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Admin Panel"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createHeader(inventory, itemManager.createItem(XMaterial.RED_STAINED_GLASS_PANE, " "));

    inventory.setItem(1, itemManager.createNMSSkullItem(PluginConstants.H_TEXTURE_VALUE, " "));
    inventory.setItem(2, itemManager.createNMSSkullItem(PluginConstants.U_TEXTURE_VALUE, " "));
    inventory.setItem(3, itemManager.createNMSSkullItem(PluginConstants.G_TEXTURE_VALUE, " "));
    inventory.setItem(4, itemManager.createNMSSkullItem(PluginConstants.S_TEXTURE_VALUE, " "));

    inventory.setItem(6, itemManager.getEros(UUID.randomUUID()));
    inventory.setItem(7, itemManager.getSilvia(UUID.randomUUID()));

//    inventory.setItem(31, itemManager.createItem(XMaterial.HONEYCOMB, "&aPlugin Settings"));
    inventory.setItem(31, itemManager.createItem(XMaterial.SCAFFOLDING, "&aWork In Progress"));

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getTemporaryGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getTemporaryGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getTemporaryGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }
  }
}
