package me.nothixal.hugs.guis;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

public class PluginLexiconGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;
  private final String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;

  public PluginLexiconGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Plugin Lexicon"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createItem(XMaterial.BOOKSHELF, "&bPlugin Lexicon"));

    // Row 3
    inventory.setItem(20, itemManager.createNMSSkullItem(PluginConstants.WEBSITE_TEXTURE, "&aPlugin Website",
        Arrays.asList(
            "&7While visiting, consider giving",
            "&7the plugin a &e✰✰✰✰✰ &7rating!",
            "",
            "&eClick to visit the website!")));

    inventory.setItem(21, itemManager.createItem(XMaterial.REDSTONE, "&aSource Code",
        Collections.singletonList("&7Click to visit the GitLab repo!")));

    inventory.setItem(22, itemManager.createNMSSkullItem(PluginConstants.BUG_TRACKER_TEXTURE, "&aBug Tracker",
        Collections.singletonList("&7Click to visit the plugin's bug tracker.")));

    inventory.setItem(24, itemManager.createNMSSkullItem(PluginConstants.DISCORD_TEXTURE_VALUE, "&9Discord",
        Collections.singletonList("&7Join our community.")));

    inventory.setItem(31, itemManager.createItem(XMaterial.WRITABLE_BOOK, "&aWiki",
        Arrays.asList(
            "&7Need help understanding the config files?",
            "&7Don't understand all the settings?",
            "",
            "&eClick to visit the wiki!")));

    //inventory.setItem(31, getRandomDonateItem());

    // Row 6
    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getQuickLinksGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getQuickLinksGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getQuickLinksGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    if (!item.hasItemMeta()) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    if (item.getItemMeta().getDisplayName().contains("Source Code")) {
      showSourceCodeBook(player);
      return;
    }

    if (item.getItemMeta().getDisplayName().contains("Website")) {
      showWebsiteBook(player);
      return;
    }

    if (item.getItemMeta().getDisplayName().contains("Wiki")) {
      showWikiBook(player);
      return;
    }

    if (item.getItemMeta().getDisplayName().contains("Bug Tracker")) {
      showBugTrackerBook(player);
      return;
    }

    if (item.getItemMeta().getDisplayName().contains("Discord")) {
      showDiscordBook(player);
      return;
    }
  }

  private void showWebsiteBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder website = new ComponentBuilder("[VISIT SITE]").color(ChatColor.of(primaryColor)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.PLUGIN_WEBSITE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click me to visit the site!").color(ChatColor.AQUA).create()));

    BaseComponent[] websiteBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("        ").append("Spigot Site")
        .append("\n\n")
        .append("Consider giving the plugin a 5-star rating.")
        .append("\n\n\n")
        .append("     ").append(website.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(websiteBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Website Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  private void showSourceCodeBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    BaseComponent[] sourceCodeBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("      ").append("Source Code")
        .append("\n\n")
        .append("Consider contributing to the development!")
        .append("\n\n\n")
        .append("    ").append("[COMING SOON]").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .create();

    bookMeta.spigot().addPage(sourceCodeBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Source Code Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  private void showWikiBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    BaseComponent[] wikiBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("            ").append("Wiki")
        .append("\n\n")
        .append("Need help with configuration?")
        .append("\n\n")
        .append("    ").append("[COMING SOON]").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .create();

    bookMeta.spigot().addPage(wikiBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Wiki Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  private void showBugTrackerBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    BaseComponent[] bugTrackerBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("       ").append("Bug Tracker")
        .append("\n\n")
        .append("Found a bug or need to report an issue?")
        .append("\n\n")
        .append("    ").append("[COMING SOON]").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .create();

    bookMeta.spigot().addPage(bugTrackerBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Bug Tracker Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  private void showDiscordBook(Player player) {
    ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
    BookMeta bookMeta = (BookMeta) book.getItemMeta();

    ComponentBuilder discord = new ComponentBuilder("[JOIN COMMUNITY]").color(ChatColor.of(primaryColor)).bold(true)
        .event(new ClickEvent(Action.OPEN_URL, PluginConstants.DISCORD_INVITE_LINK))
        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("Click to join the community!").color(ChatColor.AQUA).create()));

    BaseComponent[] discordBook = new ComponentBuilder()
        .append("           ").append("Hugs").color(ChatColor.of(primaryColor)).bold(true).append("").reset()
        .append("\n")
        .append("          ").append("Discord")
        .append("\n\n\n\n")
        .append("  ").append(discord.create()).append("").reset()
        .create();

    bookMeta.spigot().addPage(discordBook);
    bookMeta.setAuthor("HugsPlugin");
    bookMeta.setTitle("Discord Invite Link");
    book.setItemMeta(bookMeta);

    player.closeInventory();

    plugin.getItemManager().openBook(plugin, player, book);
  }

  private ItemStack getRandomDonateItem() {
    List<ItemStack> donateItems = new ArrayList<>();
    donateItems.add(itemManager.createItem(Material.RABBIT_STEW, "&aDonate", Collections.singletonList("&7Feed the developer!")));
    donateItems.add(itemManager.createItem(Material.HEART_OF_THE_SEA, "&aDonate", Collections.singletonList("&7Kindness Donation!")));
    donateItems.add(itemManager.createItem(Material.DRAGON_BREATH, "&aDonate", Collections.singletonList("&7Buy me a coffee!")));
    donateItems.add(itemManager.createItem(Material.JUKEBOX, "&aDonate", Collections.singletonList("&7Support me directly!")));
    return donateItems.get(new Random().nextInt(donateItems.size()));
  }

}
