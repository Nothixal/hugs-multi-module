package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.Indicator;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class YourPreferencesGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public YourPreferencesGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Your Preferences"));
  }

  public void openGUI(Player player) {
    PlayerDataManager dataManager = plugin.getPlayerDataManager();

    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    inventory.setItem(4, itemManager.createSkullItem(player.getUniqueId(), "&e" + player.getName()));

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    boolean wantsHugs = dataManager.wantsHugs(player.getUniqueId());

    if (wantsHugs) {
      // Huggable Toggle
      if (dataManager.wantsHugs(player.getUniqueId())) {
        inventory.setItem(20, plugin.getItemToggles().HUGGABLE_ENABLED_ICON);
        inventory.setItem(29, plugin.getItemToggles().HUGGABLE_ENABLED_BUTTON);
      } else {
        inventory.setItem(20, plugin.getItemToggles().HUGGABLE_DISABLED_ICON);
        inventory.setItem(29, plugin.getItemToggles().HUGGABLE_DISABLED_BUTTON);
      }

      if (dataManager.wantsParticles(player.getUniqueId())) {
        inventory.setItem(22, plugin.getItemToggles().PARTICLES_ENABLED_ICON);
        inventory.setItem(31, plugin.getItemToggles().PARTICLES_ENABLED_BUTTON);
      } else {
        inventory.setItem(22, plugin.getItemToggles().PARTICLES_DISABLED_ICON);
        inventory.setItem(31, plugin.getItemToggles().PARTICLES_DISABLED_BUTTON);
      }

      // Particles Toggle
      //inventory.setItem(22, plugin.getItemToggles().getParticleSettingItems(player.getUniqueId()));

      //inventory.setItem(22, itemManager.createItem(XMaterial.POPPY, "&aParticles"));
      //inventory.setItem(31, itemManager.createItem(XMaterial.LIME_DYE));

      // Sounds Toggle
      if (dataManager.wantsSounds(player.getUniqueId())) {
        inventory.setItem(23, plugin.getItemToggles().SOUNDS_ENABLED_ICON);
        inventory.setItem(32, plugin.getItemToggles().SOUNDS_ENABLED_BUTTON);
      } else {
        inventory.setItem(23, plugin.getItemToggles().SOUNDS_DISABLED_ICON);
        inventory.setItem(32, plugin.getItemToggles().SOUNDS_DISABLED_BUTTON);
      }

      // Indicators
      ItemStack indicatorItem = itemManager.createItem(XMaterial.PAINTING, "&eYour Indicators");
      ItemMeta meta = indicatorItem.getItemMeta();

      List<Indicator> indicatorList = dataManager.getIndicators(player.getUniqueId());
      indicatorList.sort(Comparator.comparing(v -> indicatorList.indexOf(v.getPriority())));

      List<String> lore = new ArrayList<>();

      for (Indicator indicator : indicatorList) {
        if (indicator.isEnabled()) {
          lore.add(" &a&l✔ &a" + indicator.getType());
        } else {
          lore.add(" &c&l✖ &c" + indicator.getType());
        }
      }

      List<String> newLore = new ArrayList<>();

      for (String s : lore) {
        newLore.add(ChatUtils.colorChat(s));
      }

      meta.setLore(newLore);
      indicatorItem.setItemMeta(meta);

      inventory.setItem(24, indicatorItem);
      inventory.setItem(33, itemManager.createItem(XMaterial.ORANGE_DYE, "&aIndicator Settings", Collections.singletonList("&eClick to edit your settings!")));
    } else {
      ItemStack eros = itemManager.createSkullItem(itemManager.getEros(player.getUniqueId()), "&eEros", Arrays.asList(
          "&7&o\"Hey, you can't modify your settings",
          "&7&obecause you're not huggable!\""));

      inventory.setItem(2, eros);

      if (dataManager.wantsHugs(player.getUniqueId())) {
        inventory.setItem(20, plugin.getItemToggles().HUGGABLE_ENABLED_ICON);
        inventory.setItem(29, plugin.getItemToggles().HUGGABLE_ENABLED_BUTTON);
      } else {
        inventory.setItem(20, plugin.getItemToggles().HUGGABLE_DISABLED_ICON);
        inventory.setItem(29, plugin.getItemToggles().HUGGABLE_DISABLED_BUTTON);
      }

      ItemStack particles = itemManager.setGlowing(itemManager.createItem(XMaterial.POPPY, "&4Particles",
          Collections.singletonList("&7Setting Disabled")), true);

      ItemStack sounds = itemManager.setGlowing(itemManager.createItem(XMaterial.BELL, "&4Sounds",
          Collections.singletonList("&7Setting Disabled")), true);

      ItemStack indicators = itemManager.setGlowing(itemManager.createItem(XMaterial.PAINTING, "&4Indicators",
          Collections.singletonList("&7Setting Disabled")), true);

      ItemStack unmodifiableButton = itemManager.setGlowing(itemManager.createItem(XMaterial.RED_DYE, "&cUnmodifiable"), true);

      inventory.setItem(22, particles);
      inventory.setItem(31, unmodifiableButton);

      inventory.setItem(23, sounds);
      inventory.setItem(32, unmodifiableButton);

      inventory.setItem(24, indicators);
      inventory.setItem(33, unmodifiableButton);
    }

    plugin.getGUIManager().getYourSettingsGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourSettingsGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    Player player = (Player) event.getWhoClicked();
    int slot = event.getSlot();

    if (inv != plugin.getGUIManager().getYourSettingsGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new HelpGUI(plugin).openGUI(player);
      return;
    }

    // Wants Hugs Button
    if (slot == 20 || slot == 29) {
      plugin.getPlayerDataManager().setWantsHugs(player.getUniqueId(), !plugin.getPlayerDataManager().wantsHugs(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemToggles().getHuggableItems());
      openGUI(player);
      return;
    }

    // Particles Button
    if (slot == 22 || slot == 31) {
      if (!plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {
        return;
      }

      plugin.getPlayerDataManager().setWantsParticles(player.getUniqueId(), !plugin.getPlayerDataManager().wantsParticles(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemToggles().getParticleSettingsPair());
      return;
    }

    // Sounds Button
    if (slot == 23 || slot == 32) {
      if (!plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {
        return;
      }

      plugin.getPlayerDataManager().setWantsSounds(player.getUniqueId(), !plugin.getPlayerDataManager().wantsSounds(player.getUniqueId()));
      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemToggles().getSoundSettingsItems());
      player.updateInventory();
      return;
    }

    // Your Indicators Button
    if (slot == 24 || slot == 33) {
      if (!plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {
        return;
      }

      new YourIndicatorsGUI(plugin).openGUI(player);
      return;
    }
  }

}
