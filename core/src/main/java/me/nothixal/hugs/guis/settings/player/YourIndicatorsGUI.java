package me.nothixal.hugs.guis.settings.player;

import com.cryptomorin.xseries.XMaterial;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.inventory.InventoryUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class YourIndicatorsGUI implements Listener {

  private final HugsPlugin plugin;
  private Inventory inventory;
  private ItemManager itemManager;

  public YourIndicatorsGUI(HugsPlugin plugin) {
    this.plugin = plugin;
    this.itemManager = plugin.getItemManager();
    this.inventory = Bukkit.createInventory(null, 9 * 6, ChatUtils.colorChat(plugin.getGUIManager().getGuiPrefix() + "&8Your Indicators"));
  }

  public void openGUI(Player player) {
    InventoryUtils.createBorder(inventory, itemManager.createItem(XMaterial.BLACK_STAINED_GLASS_PANE, " "));

    if (plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {

    }

    // Row 1
    ItemStack item = itemManager.createItem(XMaterial.PAINTING, "&eYour Indicators");
    inventory.setItem(4, item);

    //inventory.setItem(4, itemManager.createItem(Material.FLOWER_BANNER_PATTERN, "&eYour Indicators"));

    // Row 3
    PlayerDataManager dataManager = plugin.getPlayerDataManager();

    if (dataManager.getIndicator(player.getUniqueId(), "chat")) {
      inventory.setItem(20, plugin.getItemToggles().CHAT_INDICATOR_ENABLED_ICON);
      inventory.setItem(29, plugin.getItemToggles().CHAT_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(20, plugin.getItemToggles().CHAT_INDICATOR_DISABLED_ICON);
      inventory.setItem(29, plugin.getItemToggles().CHAT_INDICATOR_DISABLED_BUTTON);
    }

    if (dataManager.getIndicator(player.getUniqueId(), "titles")) {
      inventory.setItem(21, plugin.getItemToggles().TITLES_INDICATOR_ENABLED_ICON);
      inventory.setItem(30, plugin.getItemToggles().TITLES_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(21, plugin.getItemToggles().TITLES_INDICATOR_DISABLED_ICON);
      inventory.setItem(30, plugin.getItemToggles().TITLES_INDICATOR_DISABLED_BUTTON);
    }

    if (dataManager.getIndicator(player.getUniqueId(), "actionbar")) {
      inventory.setItem(22, plugin.getItemToggles().ACTIONBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(31, plugin.getItemToggles().ACTIONBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(22, plugin.getItemToggles().ACTIONBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(31, plugin.getItemToggles().ACTIONBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (dataManager.getIndicator(player.getUniqueId(), "bossbar")) {
      inventory.setItem(23, plugin.getItemToggles().BOSSBAR_INDICATOR_ENABLED_ICON);
      inventory.setItem(32, plugin.getItemToggles().BOSSBAR_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(23, plugin.getItemToggles().BOSSBAR_INDICATOR_DISABLED_ICON);
      inventory.setItem(32, plugin.getItemToggles().BOSSBAR_INDICATOR_DISABLED_BUTTON);
    }

    if (dataManager.getIndicator(player.getUniqueId(), "toast")) {
      inventory.setItem(24, plugin.getItemToggles().TOAST_INDICATOR_ENABLED_ICON);
      inventory.setItem(33, plugin.getItemToggles().TOAST_INDICATOR_ENABLED_BUTTON);
    } else {
      inventory.setItem(24, plugin.getItemToggles().TOAST_INDICATOR_DISABLED_ICON);
      inventory.setItem(33, plugin.getItemToggles().TOAST_INDICATOR_DISABLED_BUTTON);
    }

    inventory.setItem(48, itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"));
    inventory.setItem(49, itemManager.createItem(XMaterial.STRUCTURE_VOID, "&cClose Menu"));

    plugin.getGUIManager().getYourIndicatorGUIs().put(player.getUniqueId(), inventory);
    player.openInventory(plugin.getGUIManager().getYourIndicatorGUIs().get(player.getUniqueId()));
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    ItemStack item = event.getCurrentItem();
    int slot = event.getSlot();
    Player player = (Player) event.getWhoClicked();

    if (inv != plugin.getGUIManager().getYourIndicatorGUIs().get(player.getUniqueId())) {
      return;
    }

    event.setCancelled(true);

    if (item == null) {
      return;
    }

    // Close Button
    if (slot == 49) {
      player.closeInventory();
      return;
    }

    if (item.equals(itemManager.createItem(XMaterial.SPECTRAL_ARROW, "&eGo Back"))) {
      new YourPreferencesGUI(plugin).openGUI(player);
      return;
    }

    // Chat Indicator Check
    if (slot == 20 || slot == 29) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), "chat", !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "chat"));
      InventoryUtils.togglePair(inv, 20, 29, plugin.getItemToggles().getChatIndicatorItems());
      player.updateInventory();
      return;
    }

    // Titles Indicator Check
    if (slot == 21 || slot == 30) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), "titles", !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "titles"));
      InventoryUtils.togglePair(inv, 21, 30, plugin.getItemToggles().getTitlesIndicatorItems());
      player.updateInventory();
      return;
    }

    // Actionbar Indicator Check
    if (slot == 22 || slot == 31) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), "actionbar", !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "actionbar"));
      InventoryUtils.togglePair(inv, 22, 31, plugin.getItemToggles().getActionbarIndicatorItems());
      player.updateInventory();
      return;
    }

    // Bossbar Indicator Check
    if (slot == 23 || slot == 32) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), "bossbar", !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "bossbar"));
      InventoryUtils.togglePair(inv, 23, 32, plugin.getItemToggles().getBossbarIndicatorItems());
      player.updateInventory();
      return;
    }

    // Toast Indicator Check
    if (slot == 24 || slot == 33) {
      plugin.getPlayerDataManager().setIndicator(player.getUniqueId(), "toast", !plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "toast"));
      InventoryUtils.togglePair(inv, 24, 33, plugin.getItemToggles().getToastIndicatorItems());
      player.updateInventory();
      return;
    }
  }

}
