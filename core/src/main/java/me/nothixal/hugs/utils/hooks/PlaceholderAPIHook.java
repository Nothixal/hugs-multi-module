package me.nothixal.hugs.utils.hooks;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.nothixal.hugs.HugsPlugin;
import org.bukkit.entity.Player;

public class PlaceholderAPIHook extends PlaceholderExpansion {

  // Instance of plugin we want to use.
  private HugsPlugin plugin;

  public PlaceholderAPIHook(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  /**
   * Because this is an internal class,
   * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
   * PlaceholderAPI is reloaded
   *
   * @return true to persist through reloads
   */
  @Override
  public boolean persist(){
    return true;
  }

  /**
   * We can optionally override this method if we need to initialize variables within this class if
   * we need to or even if we have to do other checks to ensure the hook is properly setup.
   *
   * @return true or false depending on if it can register.
   */
  @Override
  public boolean register() {
    return super.register();
  }

  /**
   * The name of the person who created this expansion should go here.
   *
   * @return The name of the author as a String.
   */
  @Override
  public String getAuthor() {
    return "Nothixal";
  }

  /**
   * The placeholder identifier should go here. This is what tells PlaceholderAPI to call our
   * onRequest method to obtain a value if a placeholder starts with our identifier. This must be
   * unique and can not contain % or _
   *
   * @return The identifier in {@code %<identifier>_<value>%} as String.
   */
  @Override
  public String getIdentifier() {
    return "src/main/java/hugs";
  }

  /**
   * This is the version of this expansion.
   * <br>You don't have to use numbers, since it is set as a String.
   *
   * @return The version as a String.
   */
  @Override
  public String getVersion() {
    return "2.0.0";
  }

  /**
   * This is the method called when a placeholder with our identifier is found and needs a value.
   * <br>We specify the value identifier in this method.
   * <br>Since version 2.9.1 can you use OfflinePlayers in your requests.
   * <br>This is not the case for us, since we need an online player.
   *
   * @param player A {@link Player Player}.
   * @param identifier A String containing the identifier/value.
   * @return possibly-null String of the requested identifier.
   */
  @Override
  public String onPlaceholderRequest(Player player, String identifier) {
// Placeholder: %hugs_total%
    if (identifier.equals("total")) {
      return String.valueOf(plugin.getPlayerDataManager().getTotalHugsGiven());
    }
    // Placeholder: %hugs_mass_total%
    if (identifier.equals("mass_total")) {
      return String.valueOf(plugin.getPlayerDataManager().getTotalMassHugsGiven());
    }
    if (identifier.equals("self_total")) {
      return String.valueOf(plugin.getPlayerDataManager().getTotalSelfHugsGiven());
    }

    // Always check if the player is null for placeholders related to the player!
    if (player == null) {
      return "";
    }

    //Placeholder: %hugs_player_total_given%
    if (identifier.equals("player_total_given")) {
      return String.valueOf(plugin.getPlayerDataManager().getHugsGiven(player.getUniqueId()));
    }

    // Placeholder: %hugs_player_total_received%
    if (identifier.equals("player_total_received")) {
      return String.valueOf(plugin.getPlayerDataManager().getHugsReceived(player.getUniqueId()));
    }

    // Placeholder: %hugs_player_total_mass_given%
    if (identifier.equals("player_total_mass_given")) {
      return String.valueOf(plugin.getPlayerDataManager().getMassHugsGiven(player.getUniqueId()));
    }

    // Placeholder: %hugs_player_total_mass_received%
    if (identifier.equals("player_total_mass_received")) {
      return String.valueOf(plugin.getPlayerDataManager().getMassHugsReceived(player.getUniqueId()));
    }

    // Anything else someone types is invalid because we never defined %customplaceholder_<what they want a value for>%
    // we can just return null so the placeholder they specified is not replaced.
    return null;
  }

}
