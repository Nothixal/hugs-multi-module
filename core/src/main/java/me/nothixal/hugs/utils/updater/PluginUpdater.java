package me.nothixal.hugs.utils.updater;

import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.logger.LogUtils;
import me.nothixal.hugs.utils.updater.UpdateChecker.UpdateReason;
import me.nothixal.hugs.utils.updater.UpdateChecker.UpdateResult;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class PluginUpdater implements Listener {

  private final HugsPlugin plugin;
  private final UpdateChecker updateChecker;
  private UpdateResult result;
  private UpdateReason reason;

  public PluginUpdater(HugsPlugin plugin) {
    this.plugin = plugin;
    updateChecker = UpdateChecker.get();
  }

  public void checkForUpdate() {
    updateChecker.requestUpdateCheck().whenComplete((result, exception) -> {
      UpdateReason reason = result.getReason();

      this.result = result;
      this.reason = reason;

      FileConfiguration updaterFile = plugin.getFileManager().getUpdaterData();

      if (updaterFile.getString(Settings.UPDATER_OUTPUT_MODE.getValue()).equals("Fancy")) {
        printFancy(reason, result);
      } else if (updaterFile.getString(Settings.UPDATER_OUTPUT_MODE.getValue()).equals("Minimal")) {
        printMinimal(reason, result);
      }
    });
  }

  public void sendUpdateMessage(Player player) {
    String prefix = plugin.getLangFile().getString(Messages.PREFIX.getLangValue());

    player.sendMessage(ChatUtils.colorChat(prefix + " &7Checking for update..."));

    if (result.getNewestVersion().equals(plugin.getDescription().getVersion())) {
      player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l----&8&l<&3+&8&l>&m&l------&8&l<<&b&l Hugs Updater &8&l>>&m&l------&8&l<&3+&8&l>&m&l----&8&l>"));
      player.sendMessage(ChatUtils.colorChat(prefix + " &7You have the latest version of hugs."));
      player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l-------&8&l<&3+&8&l>&m&l---------&8&l<<&3+&8&l>>&m&l---------&8&l<&3+&8&l>&m&l-------&8&l>"));
      return;
    }

    player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l----&8&l<&3+&8&l>&m&l------&8&l<<&b&l Hugs Updater &8&l>>&m&l------&8&l<&3+&8&l>&m&l----&8&l>"));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7There is a new update available!" + " &8(&7Version &a" + result.getNewestVersion() + "&8)"));
    player.sendMessage(ChatUtils.colorChat(prefix + " &7You're still using version &e" + plugin.getDescription().getVersion() + "&7."));

    plugin.getChatManager().sendMessage(player, "[{\"text\":\"Download the latest version \", \"color\":\"gray\"}, {\"text\":\"HERE\", \"color\":\"dark_aqua\", \"bold\":\"true\", \"clickEvent\":{\"action\":\"open_url\", \"value\":\"https://www.spigotmc.org/resources/hugs.39722/\"}, \"hoverEvent\":{\"action\":\"show_text\", \"value\":{\"text\":\"Click me to download!\", \"color\":\"dark_aqua\"}}}]");

    player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l-------&8&l<&3+&8&l>&m&l---------&8&l<<&3+&8&l>>&m&l---------&8&l<&3+&8&l>&m&l-------&8&l>"));
  }

  private void printMinimal(UpdateReason reason, UpdateResult result) {
    switch (reason) {
      case UP_TO_DATE:
        LogUtils.logInfo(String.format("Your version of Hugs (%s) is up to date!", result.getNewestVersion()));
        break;
      case NEW_UPDATE:
        LogUtils.logInfo("New Updated Ready");
        break;
      case UNRELEASED_VERSION:
        LogUtils.logInfo(String.format("Your version of Hugs (%s) is more recent than the one publicly available. Are you on a development build?", result.getNewestVersion()));
        break;
      case INVALID_JSON:
        LogUtils.logInfo("Invalid JSON");
        break;
      case UNAUTHORIZED_QUERY:
        LogUtils.logInfo("Unauthorized query");
        break;
      case COULD_NOT_CONNECT:
        LogUtils.logInfo("Could not connect to the update servers");
        break;
      case UNSUPPORTED_VERSION_SCHEME:
        LogUtils.logInfo("Unknown Version Scheme");
        break;
      case UNKNOWN_ERROR:
        LogUtils.logInfo("An unknown error has occurred.");
        break;
      default:
        LogUtils.logInfo("Unknown");
    }
  }

  private void printFancy(UpdateReason reason, UpdateResult result) {
    switch (reason) {
      case UP_TO_DATE:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("No update available.");
        LogUtils.logInfo("You have the latest version of hugs.");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case NEW_UPDATE:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("There is an update available for the plugin! &8(&8Version &a" + result.getNewestVersion() + "&8)");
        LogUtils.logInfo("You're still using version &e" + plugin.getDescription().getVersion() + "&7.");
        LogUtils.logInfo("Download the latest version at: &3https://www.spigotmc.org/resources/hugs.39722/");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case UNRELEASED_VERSION:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("How did you get this? Are you on a development build?");
        LogUtils.logInfo("This version of Hugs is more recent than the one publicly available.");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case INVALID_JSON:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("Invalid JSON");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case UNAUTHORIZED_QUERY:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("Unauthorized query");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case COULD_NOT_CONNECT:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("&cERROR: &rUnable to check for updates.");
        LogUtils.logInfo("Is the server in offline mode?");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case UNSUPPORTED_VERSION_SCHEME:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("Unknown Version Scheme");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      case UNKNOWN_ERROR:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("An unknown error has occurred.");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
        break;
      default:
        LogUtils.logInfo("&8<--------<&3+&8>---------<< &bHugs Updater &8>>---------<&3+&8>-------->");
        LogUtils.logInfo("Unknown...");
        LogUtils.logInfo("&8<-----------<&3+&8>------------<<&3+&8>>------------<&3+&8>----------->");
    }
  }

  public UpdateResult getResult() {
    return result;
  }

  public UpdateReason getReason() {
    return reason;
  }
}
