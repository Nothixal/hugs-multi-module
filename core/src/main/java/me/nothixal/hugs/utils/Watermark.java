package me.nothixal.hugs.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.ServerVersion;
import me.nothixal.hugs.enums.holidays.HolidayOverride;
import me.nothixal.hugs.utils.logger.LogUtils;

public class Watermark {

  private final HugsPlugin plugin;

  public Watermark(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  // ASCII Art
  // Loading Version
  // Created By
  // Running On
  // Quote
  public void sendPluginPreEnableWatermark() {
    for (String s : getRandomASCIIArt()) {
      LogUtils.logInfoNoPrefix("    " + s);
    }

    String pluginPrefix = plugin.getHolidayOverride().getPluginPrefix();
    String overrideMessage = plugin.getHoliday().getMessage();

    String padding = String.format("%-3s", "");

    LogUtils.logInfoNoPrefix(padding + pluginPrefix + "&7Loading Version: &f" + plugin.getDescription().getVersion());
    LogUtils.logInfoNoPrefix(padding + pluginPrefix + "&7Created By: &b" + PluginConstants.PLUGIN_CREATOR);

    VersionChecker versionChecker = plugin.getVersionChecker();

    if (versionChecker.getServerVersion() == ServerVersion.UNKNOWN) {
      LogUtils.logInfoNoPrefix(padding + "&8Running on " + versionChecker.getServerVersion().getDisplayValue());
    } else if (plugin.getVersionChecker().getServerVersion().isSupported()) {
      LogUtils.logInfoNoPrefix(padding + "&8Running on " + plugin.getServer().getVersion() + "&f - &8(&a" + plugin.getServer().getBukkitVersion() + "&8)");
    } else {
      LogUtils.logInfoNoPrefix(padding + "&8Running on " + plugin.getServer().getVersion() + "&f - &8(&c" + plugin.getServer().getBukkitVersion() + "&8)");
    }

    if (plugin.getHolidayOverride() == HolidayOverride.NONE) {
      LogUtils.logInfoNoPrefix(padding + getRandomQuote());
    } else {
      LogUtils.logInfoNoPrefix(padding + overrideMessage);
    }

    LogUtils.logInfoNoPrefix("");
  }

  public void sendPluginEnableWatermark() {
    LogUtils.logInfo(" ");
    LogUtils.logInfo(",d88b.d88b,");
    LogUtils.logInfo("88888888888   Hugs Plugin");
    LogUtils.logInfo("`Y8888888Y'     &aEnabled");
    LogUtils.logInfo("  `Y888Y'");
    LogUtils.logInfo("    `Y'");
    LogUtils.logInfo(" ");
  }

  public void sendPluginDisableWatermark() {
    LogUtils.logInfo(" ");
    LogUtils.logInfo(",d88b\\ d88b,");
    LogUtils.logInfo("88888/  8888   Hugs Plugin");
    LogUtils.logInfo("`Y888\\  88Y'     &cDisabled");
    LogUtils.logInfo("  `Y8/ 8Y'");
    LogUtils.logInfo("    `\\ '");
    LogUtils.logInfo(" ");
  }

  private List<String> getRandomASCIIArt() {
    List<List<String>> watermarkMessages = new ArrayList<>();

    watermarkMessages.add(Arrays
        .asList(
            " _    ,",
            "' )  /",
            " /--/ . . _,  _",
            "/  (_(_/_(_)_/_)_",
            "          /|",
            "         |/",
            ""));
    watermarkMessages.add(Arrays
        .asList(
            " __",
            "( /  /",
            " /--/ , , _,  (",
            "/  /_(_/_(_)_/_)_",
            "          /|",
            "         (/",
            ""));
    watermarkMessages.add(Arrays
        .asList("",
            " /_/   _   _",
            "/ //_//_/_\\",
            "      _/",
            ""));
    watermarkMessages.add(Arrays
        .asList(
            "           __   __",
            "|__| |  | / _` /__`",
            "|  | \\__/ \\__> .__/",
            ""));
    watermarkMessages.add(Arrays
        .asList(
            "  _   _",
            " | | | |_   _  __ _ ___",
            " | |_| | | | |/ _` / __|",
            " |  _  | |_| | (_| \\__ \\",
            " |_| |_|\\__,_|\\__, |___/",
            "              |___/",
            ""));
    watermarkMessages.add(Arrays
        .asList(
            "   __ __",
            "  / // /_ _____ ____",
            " / _  / // / _ `(_-<",
            "/_//_/\\_,_/\\_, /___/",
            "          /___/",
            ""));
    watermarkMessages.add(Arrays
        .asList(
            "   ____  ___)",
            "  (, /   /",
            "    /---/      _   _",
            " ) /   (__(_(_(_/_/_)_",
            "(_/          .-/",
            "            (_/",
            ""));

    return watermarkMessages.get(new Random().nextInt(watermarkMessages.size()));
  }

  private String getRandomQuote() {
    List<String> quotes = new ArrayList<>();

    quotes.add("Spread the love.");
    quotes.add("Hugs are powerful.");
    quotes.add("Life is too short. Give someone a hug.");
    quotes.add("Hug me.");
    quotes.add("Do you need a hug?");
    quotes.add("Hug me and I'll hug you back.");
    quotes.add("Give me hugs. :3");
    quotes.add("Sending virtual hug... Hug Sent!");
    quotes.add("I need a hug? No no no, you need a hug.");
    quotes.add("BIG HUG INBOUND!");
    quotes.add("Just hug me.");
    quotes.add("Everyone deserves a hug.");
    quotes.add("Just hug it out you two.");
    quotes.add("You get a hug and you get a hug. Everyone gets a hug!");
    quotes.add("You ever wonder if there is a Cookie Monster equivalent for hugs?");
    quotes.add("How many hugs have you given in your life?");
    quotes.add("A hug for that special someone.");
    quotes.add("TACTICAL HUG INBOUND!");
    quotes.add("How many hugs does it take to get to the center of someone's problems?");
    quotes.add("WARNING: Hugs may contain happiness.");
    quotes.add("I'm hooked on the drug that is hugs.");
    quotes.add("You're hugging me too tight!");
    quotes.add("Don't worry, I'm a professional hugger!");
    quotes.add("If I hug you, will you hug me back?");
    quotes.add("When was the last time you gave someone a hug?");
    quotes.add("I could really use a hug right now.");
    quotes.add("That's not a hug. This is a hug!");
    quotes.add("Jerry. Jerry. I want you to hug me Jerry. HUG ME.");
    quotes.add("Hug me with passion.");

    return quotes.get(new Random().nextInt(quotes.size()));
  }

}