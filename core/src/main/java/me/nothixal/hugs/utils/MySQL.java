package me.nothixal.hugs.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import me.nothixal.hugs.HugsPlugin;

public class MySQL {

  private final HugsPlugin plugin;

  private Connection connection;
  private String host, database, username, password, table;
  private int port;

  public MySQL(HugsPlugin plugin) {
    this.plugin = plugin;
    host = "localhost";
    port = 3306;
    database = "src/main/java/hugs";
    username = "root";
    password = "";
  }

  public void openConnection() throws SQLException, ClassNotFoundException {
    if (connection != null && !connection.isClosed()) {
      return;
    }

    synchronized (this) {
      if (connection != null && !connection.isClosed()) {
        return;
      }

      Class.forName("com.mysql.jdbc.Driver");
      connection = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
    }
  }

  public void setConnection(Connection connection) {
    this.connection = connection;
  }

}
