package me.nothixal.hugs.utils;

public final class PluginConstants {

  public static final int PLUGIN_ID = 39722;

  public static final String PLUGIN_CREATOR = "Nothixal";

  public static final String PLUGIN_WEBSITE_LINK = "https://www.spigotmc.org/resources/hugs.39722/";
  public static final String PLUGIN_AUTHOR_HOMEPAGE = "https://www.spigotmc.org/resources/authors/shadowmasterg23.303901";

  public static final String DISCORD_INVITE_LINK = "https://discord.gg/kJyZwBj";

  public static final String PAYPAL_DONATE_LINK = "https://paypal.me/shadowmastergaming";
  public static final String CASHAPP_DONATE_LINK = "https://cash.app/$Nothixal";

  public static final String DEFAULT_PREFIX_COLOR = "&#03A9F4";
  public static final String DEFAULT_PREFIX_COLOR_BOLD = "&#03A9F4&l";
  public static final String DEFAULT_PREFIX_COLOR_HEX = "#03A9F4";
  public static final String SECONDARY_COLOR = "&#0287c3";
  public static final String SECONDARY_COLOR_HEX = "#0287c3";

  public static final String BACKUP_PREFIX_COLOR = "&#3498DB";
  public static final String BACKUP_PREFIX_COLOR_BOLD = "&#3498DB&l";
  public static final String BACKUP_PREFIX_COLOR_HEX = "#3498DB";

  public static final String HEART = "❤";
  public static final String ARROW_LEFT = "←";
  public static final String ARROW_RIGHT = "→";
  public static final String DOUBLE_ARROW = "»";
  public static final String CHECKMARK = "✔";
  public static final String X = "✖";
  public static final String OLD_DIVISION = "÷";
  public static final String PLUS_MINUS = "±";

  public static final String EROS_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzA2NmZkMjg1OWFmZmY3ZmRlMDQ4NDM1MzgwYzM4MGJlZTg2NmU1OTU2YjhhM2E1Y2NjNWUxY2Y5ZGYwZjIifX19";
  public static final String SILVIA_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2Q2ZjRhMjFlMGQ2MmFmODI0Zjg3MDhhYzYzNDEwZjFhMDFiYmI0MWQ3ZjRhNzAyZDk0NjljNjExMzIyMiJ9fX0=";

  public static final String H_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmRlNGE4OWJlMjE5N2Y4NmQyZTYxNjZhMGFjNTQxY2NjMjFkY2UyOGI3ODU0Yjc4OGQzMjlhMzlkYWVjMzIifX19";
  public static final String U_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWZkYzRmMzIxYzc4ZDY3NDg0MTM1YWU0NjRhZjRmZDkyNWJkNTdkNDU5MzgzYTRmZTlkMmY2MGEzNDMxYTc5In19fQ==";
  public static final String G_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNThjMzM2ZGVkZmUxOTdiNDM0YjVhYjY3OTg4Y2JlOWMyYzlmMjg1ZWMxODcxZmRkMWJhNDM0ODU1YiJ9fX0=";
  public static final String S_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWYyMmQ3Y2Q1M2Q1YmZlNjFlYWZiYzJmYjFhYzk0NDQzZWVjMjRmNDU1MjkyMTM5YWM5ZmJkYjgzZDBkMDkifX19";

  // Skull textures for the Plugin Info GUI.
  public static final String DISCORD_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTNiMTgzYjE0OGI5YjRlMmIxNTgzMzRhZmYzYjViYjZjMmMyZGJiYzRkNjdmNzZhN2JlODU2Njg3YTJiNjIzIn19fQ==";
  public static final String WEBSITE_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGQxOWM2ODQ2MTY2NmFhY2Q3NjI4ZTM0YTFlMmFkMzlmZTRmMmJkZTMyZTIzMTk2M2VmM2IzNTUzMyJ9fX0=";
  public static final String BUG_TRACKER_TEXTURE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjBmNDI4OGFlMDhhMzhkMmFiOWNmMzQzMTQxNjQ3ZTRmM2JlMTZjNWE5MjdlNzIyNGEzYjFkZWNhY2ZmMjU5In19fQ==";

  // Skull texture for the Bossbar icon in user settings.
  public static final String BOSSBAR_TEXTURE_VALUE = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTY0ZTFjM2UzMTVjOGQ4ZmZmYzM3OTg1YjY2ODFjNWJkMTZhNmY5N2ZmZDA3MTk5ZThhMDVlZmJlZjEwMzc5MyJ9fX0=";
}