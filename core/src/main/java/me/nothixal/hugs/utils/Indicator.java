package me.nothixal.hugs.utils;

public class Indicator {

  private int priority;
  private String type;
  protected boolean enabled;

  public Indicator(int priority, String type, boolean enabled) {
    this.priority = priority;
    this.type = type;
    this.enabled = enabled;
  }

  public int getPriority() {
    return priority;
  }

  public String getType() {
    return type;
  }

  public boolean isEnabled() {
    return enabled;
  }
}
