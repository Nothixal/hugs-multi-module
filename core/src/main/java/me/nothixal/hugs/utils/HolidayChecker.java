package me.nothixal.hugs.utils;

import java.util.Calendar;
import java.util.Date;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.holidays.Holiday;
import me.nothixal.hugs.enums.holidays.HolidayOverride;

public class HolidayChecker {

  private final HugsPlugin plugin;
  private Holiday holiday = Holiday.NONE;

  public HolidayChecker(HugsPlugin plugin) {
    this.plugin = plugin;
    checkForHoliday();
  }

  private void test2() {
    if (isHolidayToday()) {
      plugin.setHoliday(holiday);
      plugin.setOverride(holiday.getOverride());
    }
  }

  public void checkForHoliday() {
    Holiday holiday = (Holiday) checkHolidays();

    switch (holiday) {
      case NEW_YEARS:
        plugin.setHoliday(Holiday.NEW_YEARS);
        plugin.setOverride(HolidayOverride.NEW_YEARS);
        break;
      case NATIONAL_HUG_DAY:
        plugin.setHoliday(Holiday.NATIONAL_HUG_DAY);
        plugin.setOverride(HolidayOverride.NATIONAL_HUG_DAY);
        break;
      case VALENTINES_DAY:
        plugin.setHoliday(Holiday.VALENTINES_DAY);
        plugin.setOverride(HolidayOverride.VALENTINES_DAY);
        break;
      case INDEPENDENCE_DAY:
        plugin.setHoliday(Holiday.INDEPENDENCE_DAY);
        plugin.setOverride(HolidayOverride.INDEPENDENCE_DAY);
        break;
      case HALLOWEEN:
        plugin.setHoliday(Holiday.HALLOWEEN);
        plugin.setOverride(HolidayOverride.HALLOWEEN);
        break;
      case VETERANS_DAY:
        plugin.setHoliday(Holiday.VETERANS_DAY);
        plugin.setOverride(HolidayOverride.VETERANS_DAY);
        break;
      case CHRISTMAS:
        plugin.setHoliday(Holiday.CHRISTMAS);
        plugin.setOverride(HolidayOverride.CHRISTMAS);
        break;
      case BOXING_DAY:
        plugin.setHoliday(Holiday.BOXING_DAY);
        plugin.setOverride(HolidayOverride.BOXING_DAY);
        break;
      case NONE:
        plugin.setOverride(HolidayOverride.NONE);
        break;
    }
  }

  private Enum<Holiday> checkHolidays() {
    Calendar today = Calendar.getInstance();
    Date date1 = new Date();

    today.setTime(date1);
    today.set(Calendar.HOUR_OF_DAY, 0);
    today.set(Calendar.MINUTE, 0);
    today.set(Calendar.SECOND, 0);
    today.set(Calendar.MILLISECOND, 0);

    Calendar calendar2 = Calendar.getInstance();
    Date date2 = new Date();

    calendar2.setTime(date2);
    calendar2.set(Calendar.HOUR_OF_DAY, 0);
    calendar2.set(Calendar.MINUTE, 0);
    calendar2.set(Calendar.SECOND, 0);
    calendar2.set(Calendar.MILLISECOND, 0);

    calendar2.set(Calendar.MONTH, 0);
    calendar2.set(Calendar.DAY_OF_MONTH, 1);

    boolean newYears = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (newYears) {
      return Holiday.NEW_YEARS;
    }

    calendar2.set(Calendar.MONTH, 0);
    calendar2.set(Calendar.DAY_OF_MONTH, 21);

    boolean nationalHugDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (nationalHugDay) {
      return Holiday.NATIONAL_HUG_DAY;
    }

    calendar2.set(Calendar.MONTH, 1);
    calendar2.set(Calendar.DAY_OF_MONTH, 14);

    boolean valentinesDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (valentinesDay) {
      return Holiday.VALENTINES_DAY;
    }

    calendar2.set(Calendar.MONTH, 6);
    calendar2.set(Calendar.DAY_OF_MONTH, 4);

    boolean independenceDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (independenceDay) {
      return Holiday.INDEPENDENCE_DAY;
    }

    calendar2.set(Calendar.MONTH, 9);
    calendar2.set(Calendar.DAY_OF_MONTH, 31);

    boolean halloween = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (halloween) {
      return Holiday.HALLOWEEN;
    }

    calendar2.set(Calendar.MONTH, 10);
    calendar2.set(Calendar.DAY_OF_MONTH, 11);

    boolean veteransDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (veteransDay) {
      return Holiday.VETERANS_DAY;
    }

    calendar2.set(Calendar.MONTH, 11);
    calendar2.set(Calendar.DAY_OF_MONTH, 25);

    boolean christmasDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (christmasDay) {
      return Holiday.CHRISTMAS;
    }

    calendar2.set(Calendar.MONTH, 11);
    calendar2.set(Calendar.DAY_OF_MONTH, 26);

    boolean boxingDay = today.get(Calendar.DAY_OF_MONTH) == calendar2.get(Calendar.DAY_OF_MONTH) &&
        today.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);

    if (boxingDay) {
      return Holiday.BOXING_DAY;
    }

    return Holiday.NONE;
  }

  private boolean isHolidayToday() {
    return holiday != Holiday.NONE;
  }

  public Holiday getHoliday() {
    return holiday;
  }
}
