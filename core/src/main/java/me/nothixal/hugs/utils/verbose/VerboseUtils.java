package me.nothixal.hugs.utils.verbose;

import java.util.logging.Logger;
import me.nothixal.hugs.HugsPlugin;

public class VerboseUtils {

  private final HugsPlugin plugin;

  private VerboseUtils(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  private Logger getLogger() {
    return plugin.getLogger().getParent();
  }

  public void logInfo(String message) {
    getLogger().info(message);
  }

  public void logInfo(String message, String prefix) {

  }

}
