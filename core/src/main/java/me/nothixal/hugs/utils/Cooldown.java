package me.nothixal.hugs.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.configuration.Settings;
import org.bukkit.entity.Player;

public class Cooldown {

  private static Map<String, Cooldown> cooldowns = new HashMap<>();
  private long start;
  private final int timeInSeconds;
  private final UUID id;
  private final String cooldownName;

  public Cooldown(Player player, String cooldownName, int timeInSeconds) {
    this.id = player.getUniqueId();
    this.cooldownName = cooldownName;
    this.timeInSeconds = timeInSeconds;
  }

  public Cooldown(UUID id, String cooldownName, int timeInSeconds) {
    this.id = id;
    this.cooldownName = cooldownName;
    this.timeInSeconds = timeInSeconds;
  }

  public static boolean hasCooldown(Player player, String cooldownName) {
    return hasCooldown(player.getUniqueId(), cooldownName);
  }

  public static boolean hasCooldown(UUID id, String cooldownName) {
    if (getTimeLeft(id, cooldownName) > 0) {
      return true;
    } else {
      stop(id, cooldownName);
      return false;
    }
  }

  public void start() {
    this.start = System.currentTimeMillis();
    cooldowns.put(this.id.toString() + this.cooldownName, this);
  }

  private static void stop(UUID id, String cooldownName) {
    Cooldown.cooldowns.remove(id + cooldownName);
  }

  private static Cooldown getCooldown(UUID id, String cooldownName) {
    return cooldowns.get(id.toString() + cooldownName);
  }

  public static int getTimeLeft(UUID id, String cooldownName) {
    Cooldown cooldown = getCooldown(id, cooldownName);
    int f = -1;
    if (cooldown != null) {
      long now = System.currentTimeMillis();
      long cooldownTime = cooldown.start;
      int totalTime = cooldown.timeInSeconds;
      int r = (int) (now - cooldownTime) / 1000;
      f = (r - totalTime) * (-1);
    }
    return f;
  }

  public static String getTimeLeft(HugsPlugin plugin, Player player, String cooldownName) {
    return getTimeLeft(plugin, player.getUniqueId(), cooldownName);
  }

  public static String getTimeLeft(HugsPlugin plugin, UUID uuid, String cooldownName) {
    if (plugin.getConfig().getBoolean(Settings.USE_EXACT_TIME.getValue())) {
      return getExactTimeLeft(uuid, cooldownName);
    } else {
      return getRoundedTimeLeft(uuid, cooldownName);
    }
  }

  public static String getRoundedTimeLeft(UUID uuid, String cooldownName) {
    Cooldown cooldown = getCooldown(uuid, cooldownName);

    if (cooldown == null) {
      return null;
    }

    long secondsLeft = ((cooldown.start / 1000) + cooldown.timeInSeconds) - (System.currentTimeMillis() / 1000);
    if (secondsLeft >= 0) {
      return String.valueOf(secondsLeft);
    }
    return null;
  }

  public static String getExactTimeLeft(UUID uuid, String cooldownName) {
    Cooldown cooldown = getCooldown(uuid, cooldownName);

    if (cooldown == null) {
      return null;
    }

    long timeSinceCreation = System.currentTimeMillis() - cooldown.start;
    int timeLeft = cooldown.timeInSeconds * 10 - (int) (timeSinceCreation / 100);
    String secondsLeft = "" + timeLeft;

    if (timeLeft >= 10) {
      secondsLeft = secondsLeft.substring(0, secondsLeft.length() - 1) + "." + secondsLeft.substring(secondsLeft.length() - 1);
      return secondsLeft;
    }

    if (timeLeft > 0) {
      secondsLeft = secondsLeft.substring(0, secondsLeft.length() - 1) + "0." + secondsLeft.substring(secondsLeft.length() - 1);
      return secondsLeft;
    }

    return null;
  }

}
