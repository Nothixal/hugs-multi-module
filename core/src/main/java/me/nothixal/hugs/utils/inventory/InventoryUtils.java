package me.nothixal.hugs.utils.inventory;

import com.google.common.base.Strings;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.ItemPair;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public final class InventoryUtils {

  private static final HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);

  private InventoryUtils() {
  }

  public static String centerTitle(String title) {
    return Strings.repeat(" ", 26 - ChatColor.stripColor(title).length()) + ChatUtils.colorChat(title);
  }

  // Using the cinematic terminology as that's what it would look like in the GUI.
  // Letterboxing makes horizontal black bars.
  public static void addLetterboxing(Inventory inventory, ItemStack item) {
    int size = inventory.getSize();

    // Fill the top row.
    for (int i = 0; i < 9; i ++) {
      inventory.setItem(i, item);
    }

    // Fill the bottom row.
    for (int i = size - 9; i < size; i++) {
      inventory.setItem(i, item);
    }
  }

  // Using the cinematic terminology as that's what it would look like in the GUI.
  // Pillarboxing makes vertical black bars.
  public static void addPillarboxing(Inventory inventory, ItemStack item) {
    int rows = inventory.getSize() / 9;

    // If the GUI has less than three rows, don't even bother.
    // It's too small to make a border. Use a fill method instead.
    if (rows < 3) {
      return;
    }

    for (int i = 2; i < rows; i++) {
      int[] slots = new int[] { (i - 1) * 9, i * 9 - 1 };
      inventory.setItem(slots[0], item);
      inventory.setItem(slots[1], item);
    }
  }

  // This could also be referred to as windowboxing.
  public static void createBorder(Inventory inventory, ItemStack item) {
    addLetterboxing(inventory, item);
    addPillarboxing(inventory, item);
  }

  public static void createHeader(Inventory inventory, ItemStack item) {
    for (int i = 9; i < 18; i++) {
      inventory.setItem(i, item);
    }
  }

  public static void toggleItem(Inventory inventory, int slot, ItemStack from, ItemStack to) {
    if (inventory.getItem(slot) == from) {
      inventory.setItem(slot, to);
    } else {
      inventory.setItem(slot, from);
    }
  }

  public static void togglePair(Inventory inv, int iconSlot, int buttonSlot, ItemPair items) {
    toggleIcon(inv, iconSlot, items);
    toggleButton(inv, buttonSlot, items);
  }

  private static void toggleIcon(Inventory inv, int slot, ItemPair items) {
    inv.setItem(slot, items.getIconSet().toggle(inv.getItem(slot)));
  }

  private static void toggleButton(Inventory inv, int slot, ItemPair items) {
    inv.setItem(slot, items.getButtonSet().toggle(inv.getItem(slot)));
  }

  public static void showError(Inventory inv, int slot, ItemStack is, ItemStack errorItem, int seconds) {
    inv.setItem(slot, errorItem);
    Bukkit.getScheduler().runTaskLater(plugin, () -> inv.setItem(slot, is), seconds * 20L);
  }
}
