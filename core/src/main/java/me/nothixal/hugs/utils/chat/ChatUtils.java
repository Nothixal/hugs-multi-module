package me.nothixal.hugs.utils.chat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bukkit.ChatColor;

public final class ChatUtils {

  public static String colorChat(String msg) {
    return translateHexColorCodes(ChatColor.translateAlternateColorCodes('&', msg));
  }

  private static String translateHexColorCodes(final String message) {
    final Pattern hexPattern = Pattern.compile("&#([A-Fa-f0-9]{6})");
    final char colorChar = '§';
    final Matcher matcher = hexPattern.matcher(message);
    final StringBuffer buffer = new StringBuffer(message.length() + 32);
    while (matcher.find()) {
      final String group = matcher.group(1);
      matcher.appendReplacement(buffer, "§x§" + group.charAt(0) + '§' + group.charAt(1) + '§' + group.charAt(2) + '§' + group.charAt(3) + '§' + group.charAt(4) + '§' + group.charAt(5));
    }
    return matcher.appendTail(buffer).toString();
  }

  public static String replaceColors(String message) {
    return message.replaceAll("(?i)&([a-f0-9])", "");
  }

  public static String stripColor(String msg) {
    return ChatColor.stripColor(msg);
  }
}