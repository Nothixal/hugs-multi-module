package me.nothixal.hugs.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Confirmation {

  private static Map<String, Confirmation> confirmations = new HashMap<>();
  private long start;
  private final int timeInSeconds;
  private final UUID id;
  private final String cooldownName;

  public Confirmation(UUID id, String cooldownName, int timeInSeconds) {
    this.id = id;
    this.cooldownName = cooldownName;
    this.timeInSeconds = timeInSeconds;
  }

  public static boolean isConfirming(UUID id, String cooldownName) {
    if (getTimeLeft(id, cooldownName) >= 0) {
      return true;
    } else {
      stop(id, cooldownName);
      return false;
    }
  }

  public void start() {
    this.start = System.currentTimeMillis();
    confirmations.put(this.id.toString() + this.cooldownName, this);
  }

  private static void stop(UUID id, String cooldownName) {
    Confirmation.confirmations.remove(id + cooldownName);
  }

  private static Confirmation getCooldown(UUID id, String cooldownName) {
    return confirmations.get(id.toString() + cooldownName);
  }

  public static int getTimeLeft(UUID id, String cooldownName) {
    Confirmation cooldown = getCooldown(id, cooldownName);
    int f = -1;
    if (cooldown != null) {
      long now = System.currentTimeMillis();
      long cooldownTime = cooldown.start;
      int totalTime = cooldown.timeInSeconds;
      int r = (int) (now - cooldownTime) / 1000;
      f = (r - totalTime) * (-1);
    }
    return f;
  }

}
