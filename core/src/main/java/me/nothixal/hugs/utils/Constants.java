package me.nothixal.hugs.utils;

import com.cryptomorin.xseries.XMaterial;
import java.util.Arrays;
import java.util.Collections;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.managers.items.ItemManager;
import me.nothixal.hugs.utils.inventory.ButtonSet;
import me.nothixal.hugs.utils.inventory.IconSet;
import org.bukkit.inventory.ItemStack;

public class Constants {

  private HugsPlugin plugin;

  // Your Settings
  public ItemStack HUGGABLE_ENABLED_ICON;
  public ItemStack HUGGABLE_DISABLED_ICON;

  public ItemStack HUGGABLE_ENABLED_BUTTON;
  public ItemStack HUGGABLE_DISABLED_BUTTON;

  public ItemPair huggableItems;

  // Current Particle Options
  public ItemStack PARTICLES_ENABLED_ICON;
  public ItemStack PARTICLES_DISABLED_ICON;

  public ItemStack PARTICLES_ENABLED_BUTTON;
  public ItemStack PARTICLES_DISABLED_BUTTON;

  public ItemPair particleSettingsPair;

  // TODO: New particle system
  public ItemStack PARTICLES_OFF_ICON;
  public ItemStack PARTICLES_OFF_BUTTON;

  public ItemStack PARTICLES_LOW_ICON;
  public ItemStack PARTICLES_LOW_BUTTON;

  public ItemStack PARTICLES_MEDIUM_ICON;
  public ItemStack PARTICLES_MEDIUM_BUTTON;

  public ItemStack PARTICLES_HIGH_ICON;
  public ItemStack PARTICLES_HIGH_BUTTON;

  public ItemStack PARTICLES_EXTREME_ICON;
  public ItemStack PARTICLES_EXTREME_BUTTON;

  public ItemPair particleSettingItems;

  public ItemStack SOUNDS_ENABLED_ICON;
  public ItemStack SOUNDS_DISABLED_ICON;

  public ItemStack SOUNDS_ENABLED_BUTTON;
  public ItemStack SOUNDS_DISABLED_BUTTON;

  public ItemPair soundSettingsItems;

  // Your Indicators
  public ItemStack CHAT_INDICATOR_ENABLED_ICON;
  public ItemStack CHAT_INDICATOR_DISABLED_ICON;

  public ItemStack CHAT_INDICATOR_ENABLED_BUTTON;
  public ItemStack CHAT_INDICATOR_DISABLED_BUTTON;

  public ItemPair chatIndicatorItems;

  public ItemStack ACTIONBAR_INDICATOR_ENABLED_ICON;
  public ItemStack ACTIONBAR_INDICATOR_DISABLED_ICON;

  public ItemStack ACTIONBAR_INDICATOR_ENABLED_BUTTON;
  public ItemStack ACTIONBAR_INDICATOR_DISABLED_BUTTON;

  public ItemPair actionbarIndicatorItems;

  public ItemStack BOSSBAR_INDICATOR_ENABLED_ICON;
  public ItemStack BOSSBAR_INDICATOR_DISABLED_ICON;

  public ItemStack BOSSBAR_INDICATOR_ENABLED_BUTTON;
  public ItemStack BOSSBAR_INDICATOR_DISABLED_BUTTON;

  public ItemPair bossbarIndicatorItems;

  public ItemStack TITLES_INDICATOR_ENABLED_ICON;
  public ItemStack TITLES_INDICATOR_DISABLED_ICON;

  public ItemStack TITLES_INDICATOR_ENABLED_BUTTON;
  public ItemStack TITLES_INDICATOR_DISABLED_BUTTON;

  public ItemPair titlesIndicatorItems;

  public ItemStack TOAST_INDICATOR_ENABLED_ICON;
  public ItemStack TOAST_INDICATOR_DISABLED_ICON;

  public ItemStack TOAST_INDICATOR_ENABLED_BUTTON;
  public ItemStack TOAST_INDICATOR_DISABLED_BUTTON;

  public ItemPair toastIndicatorItems;

  // Plugin Settings
  public ItemStack PASSIVE_MODE_ENABLED;
  public ItemStack PASSIVE_MODE_DISABLED;

  // - Backend Settings
  public ItemStack CHECK_FOR_UPDATES_ENABLED_ICON;
  public ItemStack CHECK_FOR_UPDATES_DISABLED_ICON;

  public ItemStack CHECK_FOR_UPDATES_ENABLED_BUTTON;
  public ItemStack CHECK_FOR_UPDATES_DISABLED_BUTTON;

  public ItemPair checkForUpdatesItems;

  public Constants(HugsPlugin plugin) {
    this.plugin = plugin;
    registerItems();
  }

  public void registerItems() {
    ItemManager itemManager = plugin.getItemManager();

    String clickToEnable = "&7Click to enable!";
    String clickToDisable = "&7Click to disable!";

    //Your Settings
    String[] huggableDesc = new String[]{"&7Toggle whether or not you want", "&7to be hugged by random people."};

    HUGGABLE_ENABLED_ICON = itemManager.createItem(XMaterial.PARROT_SPAWN_EGG, "&aHuggable",
        Arrays.asList(huggableDesc));
    HUGGABLE_DISABLED_ICON = itemManager.createItem(XMaterial.GHAST_SPAWN_EGG, "&cHuggable",
        Arrays.asList(huggableDesc));

    HUGGABLE_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aHuggable",
        Collections.singletonList(clickToDisable));
    HUGGABLE_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cHuggable",
        Collections.singletonList(clickToEnable));

    huggableItems = new ItemPair(
        new IconSet(Arrays.asList(HUGGABLE_ENABLED_ICON, HUGGABLE_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(HUGGABLE_ENABLED_BUTTON, HUGGABLE_DISABLED_BUTTON)));

    // Current Particle System
    PARTICLES_ENABLED_ICON = itemManager.createItem(XMaterial.POPPY, "&aParticles");
    PARTICLES_DISABLED_ICON = itemManager.createItem(XMaterial.POPPY, "&cParticles");

    PARTICLES_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aParticles",
        Collections.singletonList(clickToDisable));
    PARTICLES_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cParticles",
        Collections.singletonList(clickToEnable));

    particleSettingsPair = new ItemPair(
        new IconSet(Arrays.asList(PARTICLES_ENABLED_ICON, PARTICLES_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(PARTICLES_ENABLED_BUTTON, PARTICLES_DISABLED_BUTTON)));

    // NEW Particle System
    PARTICLES_OFF_ICON = itemManager.createItem(XMaterial.POPPY, "&cParticles");
    PARTICLES_OFF_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cParticles");

    PARTICLES_LOW_ICON = itemManager.createItem(XMaterial.POPPY, "&eParticles");
    PARTICLES_LOW_BUTTON = itemManager.createItem(XMaterial.GREEN_DYE, "&eParticles");

    PARTICLES_MEDIUM_ICON = itemManager.createItem(XMaterial.POPPY, "&dParticles");
    PARTICLES_MEDIUM_BUTTON = itemManager.createItem(XMaterial.YELLOW_DYE, "&dParticles");

    PARTICLES_HIGH_ICON = itemManager.createItem(XMaterial.POPPY, "&aParticles");
    PARTICLES_HIGH_BUTTON = itemManager.createItem(XMaterial.RED_DYE, "&aParticles");

    PARTICLES_EXTREME_ICON = itemManager.createItem(XMaterial.POPPY, "&aParticles");
    PARTICLES_EXTREME_BUTTON = itemManager.createItem(XMaterial.CYAN_DYE, "&aParticles");

    particleSettingItems = new ItemPair(
        new IconSet(Arrays.asList(PARTICLES_OFF_ICON, PARTICLES_LOW_ICON, PARTICLES_MEDIUM_ICON, PARTICLES_HIGH_ICON, PARTICLES_EXTREME_ICON)),
        new ButtonSet(Arrays.asList(PARTICLES_OFF_BUTTON, PARTICLES_LOW_BUTTON, PARTICLES_MEDIUM_BUTTON, PARTICLES_HIGH_BUTTON, PARTICLES_EXTREME_BUTTON)));

    String[] soundsDesc = new String[]{"&7Toggles sounds playing", "&7when getting hugged."};

    SOUNDS_ENABLED_ICON = itemManager.createItem(XMaterial.BELL, "&aSounds",
        Arrays.asList(soundsDesc));
    SOUNDS_DISABLED_ICON = itemManager.createItem(XMaterial.BELL, "&cSounds",
        Arrays.asList(soundsDesc));

    SOUNDS_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aSounds",
        Collections.singletonList(clickToDisable));
    SOUNDS_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cSounds",
        Collections.singletonList(clickToEnable));

    soundSettingsItems = new ItemPair(
        new IconSet(Arrays.asList(SOUNDS_ENABLED_ICON, SOUNDS_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(SOUNDS_ENABLED_BUTTON, SOUNDS_DISABLED_BUTTON)));

    //Your Indicators
    String[] chatIndicatorDesc = new String[]{
        "&7Toggles whether you will",
        "&7receive chat notifications",
        "&7when you get hugged."};

    CHAT_INDICATOR_ENABLED_ICON = itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, "&aChat Indicator",
        Arrays.asList(chatIndicatorDesc));
    CHAT_INDICATOR_DISABLED_ICON = itemManager.createItemNoAttrib(XMaterial.FLOWER_BANNER_PATTERN, "&cChat Indicator",
        Arrays.asList(chatIndicatorDesc));

    CHAT_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aChat Indicator",
        Collections.singletonList(clickToDisable));
    CHAT_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cChat Indicator",
        Collections.singletonList(clickToEnable));

    chatIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(CHAT_INDICATOR_ENABLED_ICON, CHAT_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(CHAT_INDICATOR_ENABLED_BUTTON, CHAT_INDICATOR_DISABLED_BUTTON)));

    String[] actionbarIndicatorDesc = new String[]{
        "&7Toggles whether you will",
        "&7receive actionbar notifications",
        "&7when you get hugged."};

    ACTIONBAR_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.CAMPFIRE, "&aActionbar Indicator",
        Arrays.asList(actionbarIndicatorDesc));
    ACTIONBAR_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.CAMPFIRE, "&cActionbar Indicator",
        Arrays.asList(actionbarIndicatorDesc));

    ACTIONBAR_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aActionbar Indicator",
        Collections.singletonList(clickToDisable));
    ACTIONBAR_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cActionbar Indicator",
        Collections.singletonList(clickToEnable));

    actionbarIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(ACTIONBAR_INDICATOR_ENABLED_ICON, ACTIONBAR_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(ACTIONBAR_INDICATOR_ENABLED_BUTTON, ACTIONBAR_INDICATOR_DISABLED_BUTTON)));

    String[] bossbarIndicatorDesc = new String[]{
        "&7Toggles whether you will",
        "&7receive bossbar notifications",
        "&7when you get hugged."};

    BOSSBAR_INDICATOR_ENABLED_ICON = itemManager.createNMSSkullItem(PluginConstants.BOSSBAR_TEXTURE_VALUE, "&aBossbar Indicator",
        Arrays.asList(bossbarIndicatorDesc));
    BOSSBAR_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.WITHER_SKELETON_SKULL, "&cBossbar Indicator",
        Arrays.asList(bossbarIndicatorDesc));

    BOSSBAR_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aBossbar Indicator",
        Collections.singletonList(clickToDisable));
    BOSSBAR_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cBossbar Indicator",
        Collections.singletonList(clickToEnable));

    bossbarIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(BOSSBAR_INDICATOR_ENABLED_ICON, BOSSBAR_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(BOSSBAR_INDICATOR_ENABLED_BUTTON, BOSSBAR_INDICATOR_DISABLED_BUTTON)));

    String[] titlesIndicatorDesc = new String[]{
        "&7Toggles whether you will",
        "&7receive title notifications",
        "&7when you get hugged."};

    TITLES_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, "&aTitles Indicator",
        Arrays.asList(titlesIndicatorDesc));
    TITLES_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.ITEM_FRAME, "&cTitles Indicator",
        Arrays.asList(titlesIndicatorDesc));

    TITLES_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aTitles Indicator",
        Collections.singletonList(clickToDisable));
    TITLES_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cTitles Indicator",
        Collections.singletonList(clickToEnable));

    titlesIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(TITLES_INDICATOR_ENABLED_ICON, TITLES_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(TITLES_INDICATOR_ENABLED_BUTTON, TITLES_INDICATOR_DISABLED_BUTTON)));

    String[] toastIndicatorDesc = new String[]{
        "&7Toggles whether you will",
        "&7receive toast notifications",
        "&7when you get hugged."};

    TOAST_INDICATOR_ENABLED_ICON = itemManager.createItem(XMaterial.BREAD, "&aToast Indicator",
        Arrays.asList(toastIndicatorDesc));
    TOAST_INDICATOR_DISABLED_ICON = itemManager.createItem(XMaterial.BREAD, "&cToast Indicator",
        Arrays.asList(toastIndicatorDesc));

    TOAST_INDICATOR_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aToast Indicator",
        Collections.singletonList(clickToDisable));
    TOAST_INDICATOR_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cToast Indicator",
        Collections.singletonList(clickToEnable));

    toastIndicatorItems = new ItemPair(
        new IconSet(Arrays.asList(TOAST_INDICATOR_ENABLED_ICON, TOAST_INDICATOR_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(TOAST_INDICATOR_ENABLED_BUTTON, TOAST_INDICATOR_DISABLED_BUTTON)));

    // Plugin Settings
    PASSIVE_MODE_ENABLED = itemManager.createItem(XMaterial.POPPY, "&fPassive Mode: &aEnabled");
    PASSIVE_MODE_DISABLED = itemManager.createItem(XMaterial.WITHER_ROSE, "&fPassive Mode: &cDisabled");

    CHECK_FOR_UPDATES_ENABLED_ICON = itemManager.createItem(XMaterial.ENCHANTING_TABLE, "&aCheck For Updates");
    CHECK_FOR_UPDATES_DISABLED_ICON = itemManager.createItem(XMaterial.ENCHANTING_TABLE, "&cCheck For Updates");

    CHECK_FOR_UPDATES_ENABLED_BUTTON = itemManager.createItem(XMaterial.LIME_DYE, "&aCheck For Updates",
        Collections.singletonList(clickToDisable));
    CHECK_FOR_UPDATES_DISABLED_BUTTON = itemManager.createItem(XMaterial.GRAY_DYE, "&cCheck For Updates",
        Collections.singletonList(clickToEnable));

    checkForUpdatesItems = new ItemPair(
        new IconSet(Arrays.asList(CHECK_FOR_UPDATES_ENABLED_ICON, CHECK_FOR_UPDATES_DISABLED_ICON)),
        new ButtonSet(Arrays.asList(CHECK_FOR_UPDATES_ENABLED_BUTTON, CHECK_FOR_UPDATES_DISABLED_BUTTON)));
  }

  public ItemPair getHuggableItems() {
    return huggableItems;
  }

  public ItemPair getParticleSettingItems() {
    return particleSettingItems;
  }

//  public ItemStack getParticleSettingItems(Player player) {
//    plugin.getPlayerDataManager().getParticleValue(player.getUniqueId());
//
//  }

  public ItemPair getSoundSettingsItems() {
    return soundSettingsItems;
  }

  public ItemPair getChatIndicatorItems() {
    return chatIndicatorItems;
  }

  public ItemPair getActionbarIndicatorItems() {
    return actionbarIndicatorItems;
  }

  public ItemPair getBossbarIndicatorItems() {
    return bossbarIndicatorItems;
  }

  public ItemPair getTitlesIndicatorItems() {
    return titlesIndicatorItems;
  }

  public ItemPair getToastIndicatorItems() {
    return toastIndicatorItems;
  }

  public ItemPair getCheckForUpdatesItems() {
    return checkForUpdatesItems;
  }

  public ItemPair getParticleSettingsPair() {
    return particleSettingsPair;
  }
}
