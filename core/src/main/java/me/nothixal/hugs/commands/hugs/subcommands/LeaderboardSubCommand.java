package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.guis.LeaderboardGUI;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaderboardSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public LeaderboardSubCommand(HugsPlugin plugin) {
    super("leaderboard", Permissions.LEADERBOARD.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sendTextLeaderboard(sender);
      return true;
    }

    Player player = (Player) sender;

    if (!plugin.getConfig().getBoolean(Settings.LEADERBOARD_GUI_ENABLED.getValue())) {
      sendTextLeaderboard(player);
      return true;
    }

    //player.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Retrieving leaderboard. This could take a while."));
    new LeaderboardGUI(plugin).openGUI(player);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

  private void sendTextLeaderboard(CommandSender sender) {
    sender.sendMessage(ChatUtils.colorChat("&dLeaderboard"));
  }
}
