package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MenuSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public MenuSubCommand(HugsPlugin plugin) {
    super("menu", Permissions.MENU.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;

    int length = args.length;

    if (length == 0) {
      if (plugin.getVersionChecker().getServerVersion().ordinal() > 7) {
        new HelpGUI(plugin).openGUI(player);
      } else {
        player.sendMessage(ChatUtils.colorChat("&c&lERROR: &7This server version can't support the help menu."));
        player.sendMessage(ChatUtils.colorChat("&7Have an admin change the setting &euse_help_menu &7to &cfalse&7."));
      }

      return true;
    }

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }
}
