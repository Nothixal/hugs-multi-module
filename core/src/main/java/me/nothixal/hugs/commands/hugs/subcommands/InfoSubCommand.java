package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.guis.PluginLexiconGUI;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public InfoSubCommand(HugsPlugin plugin) {
    super("info", Permissions.INFO.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;

    int length = args.length;

    if (length == 0) {
      new PluginLexiconGUI(plugin).openGUI(player);
      return true;
    }

    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

}
