package me.nothixal.hugs.commands.hugs.subcommands;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.types.PermissionType;
import me.nothixal.hugs.guis.HelpGUI;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  private final String[] helpPages = {"1", "2"};

  public HelpSubCommand(HugsPlugin plugin) {
    super("help", Permissions.USE.getPermissionNode(), true);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    int length = args.length;

    if (!(sender instanceof Player)) {
      if (length > 1) {
        sender.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
        return true;
      }

      this.sendHugsHelpPageConsole(sender);
      return true;
    }

    Player player = (Player) sender;

    if (length == 0 && plugin.getConfig().getBoolean(Settings.USE_HELP_MENU.getValue())) {
      if (plugin.getVersionChecker().getServerVersion().ordinal() > 7) {
        new HelpGUI(plugin).openGUI(player);
      } else {
        player.sendMessage(ChatUtils.colorChat("&c&lERROR: &7This server version can't support the help menu."));
        player.sendMessage(ChatUtils.colorChat("&7Have an admin change the setting &euse_help_menu &7to &cfalse&7."));
      }

      return true;
    }

    if (length > 2) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      return true;
    }

    if (length == 1) {
      if (!Arrays.asList(helpPages).contains(args[0].toLowerCase())) {
        player.sendMessage(ChatUtils.colorChat(Errors.INVALID_ARGUMENTS.getLangValue()));
        return true;
      }

      if (args[0].equalsIgnoreCase("1")) {
        sendHugsHelpPage(player);
        return true;
      }

//      if (args[0].equalsIgnoreCase("2")) {
//        if (Cooldown.hasCooldown(player.getUniqueId(), Cooldowns.HUGS_HELP_PAGE_TWO_COMMAND_COOLDOWN.getValue())) {
//          return true;
//        }
//
//        sendHugsHelpPage2(player);
//        new Cooldown(player.getUniqueId(), Cooldowns.HUGS_HELP_PAGE_TWO_COMMAND_COOLDOWN.getValue(), Cooldowns.HUGS_HELP_PAGE_TWO_COMMAND_COOLDOWN.getCooldown()).start();
//        return true;
//      }
    }

    sendHugsHelpPage(player);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

  private void sendHugsHelpPage(CommandSender sender) {
    String prefixColor = PluginConstants.BACKUP_PREFIX_COLOR_BOLD;
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR;
    String secondaryColor = PluginConstants.SECONDARY_COLOR;

    //sender.sendMessage(ChatUtils.colorChat(prefixColor + "Hugs Commands &8&l>> &e&nLegend:&r&7 <> = Required, [] = Optional"));
    sender.sendMessage(ChatUtils.colorChat(prefixColor + "Hugs Commands &8&l>> &7Viewing help page &8(&f1&7/&f2&8) &8[&e←&8] &8[&e→&8]"));
//    sender.sendMessage(ChatUtils.colorChat("&e&lLEGEND:&r &7<> = Required, [] = Optional"));

    TextComponent hugCommand = buildCommand(sender,
        Arrays.asList(Permissions.HUG.getPermissionNode(), Permissions.HUG_RANDOM.getPermissionNode(), Permissions.HUG_ALL.getPermissionNode()),
        "&7/hug <player>",
        CommandType.NORMAL,
        "/hug ",
        "&fGives a hug to the designated player.",
        Arrays.asList("&f/hug <player>", "&f/hug *", "/hug all", "/hug everyone"));

    sender.spigot().sendMessage(hugCommand);

    TextComponent hugsCommand = buildCommand(sender,
        Collections.singletonList(Permissions.USE.getPermissionNode()),
        "&7/hugs",
        CommandType.NORMAL,
        "/src/main/java/hugs",
        "&fBase command for the plugin.",
        Collections.singletonList("&f/hugs [subcommand]"));

    sender.spigot().sendMessage(hugsCommand);

    TextComponent helpCommand = buildCommand(sender,
        Collections.singletonList(Permissions.USE.getPermissionNode()),
        "&7/hugs help [page] &8- &e(Pages Coming Soon)",
        CommandType.NORMAL,
        "/hugs help ",
        "&fShows the help page.",
        Collections.singletonList("&f/hugs help [page]"));

    sender.spigot().sendMessage(helpCommand);

    TextComponent infoCommand = buildCommand(sender,
        Collections.singletonList(Permissions.STATS.getPermissionNode()),
        "&7/hugs stats [player]",
        CommandType.NORMAL,
        "/hugs stats ",
        "&fShows a player's hug stats.",
        Collections.singletonList("&f/hugs stats <player>"));

    sender.spigot().sendMessage(infoCommand);


    TextComponent totalCommand = buildCommand(sender,
        Collections.singletonList(Permissions.TOTAL.getPermissionNode()),
        "&7/hugs total",
        CommandType.NORMAL,
        "/hugs total",
        "&fShows the total amount of hugs given.",
        Collections.singletonList("&f/hugs total"));

    sender.spigot().sendMessage(totalCommand);

    TextComponent leaderboardCommand = buildCommand(sender,
        Collections.singletonList(Permissions.LEADERBOARD.getPermissionNode()),
        "&7/hugs leaderboard",
        CommandType.NORMAL,
        "/hugs leaderboard",
        "&fShows the hugs leaderboard.",
        Collections.singletonList("&f/hugs leaderboard"));

    sender.spigot().sendMessage(leaderboardCommand);

    TextComponent settingsCommand = buildCommand(sender,
        Collections.singletonList(Permissions.PREFERENCES.getPermissionNode()),
        "&7/hugs preferences",
        CommandType.NORMAL,
        "/hugs preferences",
        "&fModify your preferences!",
        Collections.singletonList("&f/hugs preferences"));

    sender.spigot().sendMessage(settingsCommand);

    TextComponent toggleCommand = buildCommand(sender,
        Collections.singletonList(Permissions.TOGGLE.getPermissionNode()),
        "&7/hugs toggle",
        CommandType.NORMAL,
        "/hugs toggle",
        "&fToggle whether or not you want to be hugged!",
        Collections.singletonList("&f/hugs toggle"));

    sender.spigot().sendMessage(toggleCommand);

    TextComponent reloadCommand = buildCommand(sender,
        Collections.singletonList(Permissions.RELOAD.getPermissionNode()),
        "&7/hugs reload",
        CommandType.ADMIN,
        "/hugs reload",
        "&fReloads the config.",
        Collections.singletonList("&f/hugs reload"));

    sender.spigot().sendMessage(reloadCommand);

    TextComponent updateCommand = buildCommand(sender,
        Collections.singletonList(Permissions.UPDATE.getPermissionNode()),
        "&7/hugs update",
        CommandType.ADMIN,
        "/hugs update",
        "&fChecks for a plugin update.",
        Collections.singletonList("&f/hugs update"));

    sender.spigot().sendMessage(updateCommand);

    TextComponent versionCommand = buildCommand(sender,
        Collections.singletonList(Permissions.UPDATE.getPermissionNode()),
        "&7/hugs version",
        CommandType.ADMIN,
        "/hugs version",
        "&fGets the version of the plugin.",
        Collections.singletonList("&f/hugs version"));

    sender.spigot().sendMessage(versionCommand);

    TextComponent purgeCommand = buildCommand(sender,
        Arrays.asList(Permissions.PURGE.getPermissionNode(), Permissions.PURGE_ALL.getPermissionNode()),
        "&7/hugs purge <player> &8- &c(Permanent Action!)",
        CommandType.ADMIN,
        "/hugs purge ",
        "&fClear a player's stats.",
        Collections.singletonList("&f/hugs purge <player>"));

    sender.spigot().sendMessage(purgeCommand);

    // TODO: Add multiple pages in.
//    sender.sendMessage(" ");
    BaseComponent[] click = new ComponentBuilder()
        .append("click").underlined(true).color(ChatColor.WHITE)
        .event(
            new HoverEvent(HoverEvent.Action.SHOW_TEXT,
            new ComponentBuilder("Click the commands to insert them into the chat.")
                .color(ChatColor.LIGHT_PURPLE)
                .create()))
        .create();

    BaseComponent[] hover = new ComponentBuilder()
        .append("hover").underlined(true).color(ChatColor.WHITE)
        .event(
            new HoverEvent(HoverEvent.Action.SHOW_TEXT,
            new ComponentBuilder("Hover over the commands to get more info about them.")
                .color(ChatColor.LIGHT_PURPLE)
                .create()))
        .create();

//    sender.sendMessage(ChatUtils.colorChat("&e&lLEGEND:&r &7<> = Required, [] = Optional"));

    BaseComponent[] tip = new ComponentBuilder()
        .append("TIP:").color(ChatColor.YELLOW).bold(true)
        .append(" ").reset()
        .append("You can ").color(ChatColor.GRAY)
        .append(click)
        .append(" ").reset()
        .append("or ").color(ChatColor.GRAY)
        .append(hover)
        .append(" ").reset()
        .append("on the commands for more info!").color(ChatColor.GRAY)
        .create();

//    sender.spigot().sendMessage(tip);

    //sender.sendMessage(TextUtils.colorText("&8&l««« &7Prev Page &8&m      &r &3Next Page &3&l»&b&l»&f&l»"));
//    sender.sendMessage(ChatUtils.colorChat("&8&l««« &8Prev Page &8[&e←&8] &8&l<&b" + PluginConstants.HEART + "&8&l> &8[&e→&8] &3Next Page &3&l»&b&l»&f&l» "));
  }

  private enum CommandType {

    NORMAL("&bNormal"),
    ADMIN("&cAdministrative"),
    ;

    private final String value;

    CommandType(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }
  }

  private TextComponent buildCommand(CommandSender sender, List<String> permissionHover,
      String command, CommandType commandType, String commandClick, String description, List<String> usages) {
    PermissionType type = PermissionType.PARTIAL;

    if (permissionHover.stream().allMatch(sender::hasPermission)) {
      type = PermissionType.ALL;
    }

    if (permissionHover.stream().noneMatch(sender::hasPermission)) {
      type = PermissionType.NONE;
    }

    String permissionCharacter = "";

    if (type == PermissionType.ALL) {
      permissionCharacter = "&a&l+";
    } else if (type == PermissionType.NONE) {
      permissionCharacter = "&4&l-";
    } else if (type == PermissionType.PARTIAL) {
      permissionCharacter = "&e&l" + PluginConstants.OLD_DIVISION;
    }

    TextComponent commandPermission = new TextComponent(" " + ChatUtils.colorChat(permissionCharacter + " "));

    StringBuilder stringBuilder = new StringBuilder();

    for (int i = 0; i < permissionHover.size(); i++) {
      if (sender.hasPermission(permissionHover.get(i))) {
        stringBuilder.append(ChatUtils.colorChat("&a&l✔ &a"));
      } else {
        stringBuilder.append(ChatUtils.colorChat("&c&l✖ &c"));
      }

      stringBuilder.append(ChatUtils.colorChat(permissionHover.get(i)));

      if (i != permissionHover.size() - 1) {
        stringBuilder.append("\n");
      }
    }

    String permHover = ChatUtils.colorChat("&7Permission Nodes\n") + stringBuilder.toString();
    commandPermission.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(permHover).create()));

    StringBuilder finalUsages = new StringBuilder();
    finalUsages.append("\n");

    for (int i = 0; i < usages.size(); i++) {
      finalUsages.append(ChatUtils.colorChat("&8 - &f" + usages.get(i)));

      if (i != usages.size() - 1) {
        finalUsages.append("\n");
      }
    }

    List<String> commandHoverLayout = Arrays.asList(
        "&7Command Type: " + commandType.getValue(),
        "&7Description: " + description,
        "&7Usages: " + finalUsages.toString(),
        "",
        "&8Click to auto-complete.");

    StringBuilder commandHoverBuilder = new StringBuilder();

    for (int i = 0; i < commandHoverLayout.size(); i++) {
      commandHoverBuilder.append(ChatUtils.colorChat(commandHoverLayout.get(i)));

      if (i != commandHoverLayout.size() - 1) {
        commandHoverBuilder.append("\n");
      }
    }

    String finalCommandHover = commandHoverBuilder.toString();

    TextComponent baseCommand = new TextComponent(ChatUtils.colorChat(command));
    baseCommand.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(finalCommandHover).create()));
    baseCommand.setClickEvent(new ClickEvent(Action.SUGGEST_COMMAND, commandClick));

    TextComponent finalBuild = new TextComponent();
    finalBuild.addExtra(commandPermission);
    finalBuild.addExtra(baseCommand);

    return finalBuild;
  }

  private void sendHugsHelpPageConsole(CommandSender sender) {
    sender.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l---&8&l<&3+&8&l>&m&l----&8&l<<&b&l Hug Commands &8&l>>&m&l----&8&l<&3+&8&l>&m&l---&8&l>"));

    sender.sendMessage(ChatUtils.colorChat("&b/hug &3<player> &7- &fGives a hug to a designated player."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs &7- &fBase command for the plugin / Plugin Information."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs help &3<page> &7- &fShows the help page."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs total &7- &fShows the total amount of hugs given."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs leaderboard &7- &fShows the hugs leaderboard."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs info &3<player> &7- &fShows a player's Hug stats."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs reload &7- &fReloads the config."));
    sender.sendMessage(ChatUtils.colorChat("&b/hugs purge &3<player> &7- &fClear a player's stats."));
  }

  private List<List<String>> partitionList;

  public void test2() {
    List<String> lines = new ArrayList<>();
    lines.add("Some command thing that is apart of the list displayed in the pages");
    this.partitionList = Lists.partition(lines, 10);
  }


  private List<String> getEmojiPage(int page) {
    if (page < this.partitionList.size()) {
      return this.partitionList.get(0);
    }

    if (this.partitionList.size() >= page) {
      return this.partitionList.get(page - 1);
    }

    return null;
  }
}
