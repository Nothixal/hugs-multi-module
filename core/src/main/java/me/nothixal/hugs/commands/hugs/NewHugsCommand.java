package me.nothixal.hugs.commands.hugs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.DonateSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.HelpSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.InfoSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.LeaderboardSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.MenuSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.PreferencesSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.PurgeSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.ReloadSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.SettingsSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.StatsSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.ToggleSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.TotalSubCommand;
import me.nothixal.hugs.commands.hugs.subcommands.UpdateSubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.utils.PluginConstants;
import me.nothixal.hugs.utils.chat.ChatUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

public class NewHugsCommand implements TabExecutor {

  private final HugsPlugin plugin;

  private final Map<String, SubCommand> subCommands;

  public NewHugsCommand(HugsPlugin plugin) {
    this.plugin = plugin;

    this.subCommands = new HashMap<>();
    subCommands.put("help", new HelpSubCommand(plugin));
    subCommands.put("menu", new MenuSubCommand(plugin));
    subCommands.put("info", new InfoSubCommand(plugin));
    subCommands.put("stats", new StatsSubCommand(plugin));
    subCommands.put("leaderboard", new LeaderboardSubCommand(plugin));
    subCommands.put("purge", new PurgeSubCommand(plugin));
    subCommands.put("reload", new ReloadSubCommand(plugin));
    subCommands.put("toggle", new ToggleSubCommand(plugin));
    subCommands.put("settings", new SettingsSubCommand(plugin));
    subCommands.put("total", new TotalSubCommand(plugin));
    subCommands.put("update", new UpdateSubCommand(plugin));

    subCommands.put("preferences", new PreferencesSubCommand(plugin));
    subCommands.put("donate", new DonateSubCommand(plugin));
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sendPluginInfo(sender);
      return true;
    }

    Player player = (Player) sender;
    int length = args.length;

    if (!player.hasPermission(Permissions.USE.getPermissionNode())) {
      player.sendMessage(ChatUtils.colorChat(Errors.NO_PERMISSION.getLangValue()).replace("%permission%", Permissions.USE.getPermissionNode()));
      return true;
    }

    if (length == 0) {
      sendPluginInfo(player);
      return true;
    }

    if (subCommands.containsKey(args[0])) {
      SubCommand subCommand = subCommands.get(args[0]);

      if (!player.hasPermission(subCommand.getPermissionNode())) {
        player.sendMessage(ChatUtils.colorChat(Errors.NO_PERMISSION.getLangValue()).replace("%permission%", subCommand.getPermissionNode()));
        return true;
      }

      String[] values = Arrays.copyOfRange(args, 1, length);
      subCommand.onCommand(sender, cmd, label, values);
    } else {
      player.sendMessage(ChatUtils.colorChat(Errors.INVALID_ARGUMENTS.getLangValue()));
      //player.sendMessage("Invalid sub command! (" + args[0] + ")");
    }

    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 1) {
      return subCommands.entrySet().stream().filter(e -> {
        if (sender instanceof ConsoleCommandSender) {
          return e.getValue().isConsoleAllowed();
        }

        return sender.hasPermission(e.getValue().getPermissionNode()) && e.getKey().startsWith(args[0]);
      }).map(Map.Entry::getKey).collect(Collectors.toList());
    }

    if (subCommands.containsKey(args[0])) {
      String[] values = Arrays.copyOfRange(args, 1, args.length);
      return subCommands.get(args[0]).onTabComplete(sender, cmd, label, values);
    }

    return new ArrayList<>();
  }

  protected void sendNoArgumentsMessage(Player player) {
    sendPluginInfo(player);
  }

  private void sendPluginInfo(CommandSender sender) {
    sender.sendMessage(ChatUtils.colorChat("&b«&3«&8« &8<&3+&8> &8&m     &r &8<< &bHugs &8>> &8&m     &r &8<&3+&8> &8»&3»&b»"));
    sender.sendMessage(ChatUtils.colorChat("&3Hugs &8>> &7Version: &f" + plugin.getDescription().getVersion()));
    sender.sendMessage(ChatUtils.colorChat("&3Hugs &8>> &7Created by: &b" + PluginConstants.PLUGIN_CREATOR));
    sender.sendMessage(ChatUtils.colorChat("&7Type &b/hugs help &7for all of the plugin commands!"));
  }

  private void sendPluginInfo(Player player) {
    String primaryHex = PluginConstants.DEFAULT_PREFIX_COLOR_HEX;
    String primaryColor = PluginConstants.DEFAULT_PREFIX_COLOR;
    String secondaryColor = PluginConstants.SECONDARY_COLOR;
    String heart = PluginConstants.HEART;

//    player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l-------&8&l<&3❤&8&l>&m&l-------&8&l<<&b&l Hugs &8&l>>&m&l-------&8&l<&3❤&8&l>&m&l-------&8&l>"));
//    player.sendMessage(ChatUtils.colorChat("&b&l«&3&l«&8&l« &8&l<&3+&8&l> &8&l&m     &r &8&l<< " + PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs &8&l>> &8&l&m     &r &8&l<&3+&8&l> &8&l»&3&l»&b&l»"));

    //player.sendMessage(ChatUtils.colorChat(PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs Plugin &8&l>> &7Version &f" + plugin.getDescription().getVersion()));
//    player.sendMessage(ChatUtils.colorChat(PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs Plugin &8&l>> &7Developed By &b" + PluginConstants.PLUGIN_CREATOR));

    // If the plugin is running on anything less that 1.16
    if (plugin.getVersionChecker().getServerVersion().ordinal() < 9) {
      player.sendMessage(ChatUtils.colorChat("&b&l«&3&l«&8&l« &8&l<&b+&8&l> &8&l&m     &r &8&l<< &b&lHugs &8&l>> &8&l&m     &r &8&l<&b+&8&l> &8&l»&3&l»&b&l» "));

      BaseComponent[] createdBy = new ComponentBuilder()
          .append(PluginConstants.PLUGIN_CREATOR).color(ChatColor.AQUA)
          .event(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("Click to view other plugins I've made!").color(ChatColor.YELLOW).create()))
          .event(new ClickEvent(ClickEvent.Action.OPEN_URL, PluginConstants.PLUGIN_AUTHOR_HOMEPAGE))
          .create();

      BaseComponent[] callToAction = new ComponentBuilder()
          .append("/hugs help").color(ChatColor.AQUA)
          .event(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("Click to execute the command!").color(ChatColor.GRAY).create()))
          .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/hugs help"))
          .create();

      BaseComponent[] pluginInfo = new ComponentBuilder()
          .append("Hugs").color(ChatColor.AQUA).bold(true).append(" >> ").color(ChatColor.DARK_GRAY).append("").reset()
          .append("Version: ").color(ChatColor.GRAY).append(plugin.getDescription().getVersion()).color(ChatColor.WHITE)
          .append("\n")
          .append("Hugs").color(ChatColor.AQUA).bold(true).append(" >> ").color(ChatColor.DARK_GRAY).append("").reset()
          .append("Developed by: ").color(ChatColor.GRAY).append(createdBy).append("").reset()
          .append("\n")
          //.append("Created By: ").color(ChatColor.GRAY).append(createdBy).append("").reset()
          //.append("\n")
          .append("Type ").color(ChatColor.GRAY)
          .append(callToAction).append(" ").reset()
          .append("for all of the plugin commands!").color(ChatColor.GRAY)
          .create();

      player.spigot().sendMessage(pluginInfo);
      return;
    }

//    CenteredChat.sendCenteredMessage(player, ChatUtils.colorChat(primaryColor + "&l«" + secondaryColor + "&l«&8&l« &8&l<" + secondaryColor + heart + "&8&l> &8&l&m     &r &8&l<< " + PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs &8&l>> &8&l&m     &r &8&l<" + secondaryColor + heart + "&8&l> &8&l»" + secondaryColor + "&l»" + primaryColor + "&l» "));

    player.sendMessage(ChatUtils.colorChat(primaryColor + "&l«" + secondaryColor + "&l«&8&l« &8&l<" + primaryColor + "+&8&l> &8&l&m     &r &8&l<< " + PluginConstants.DEFAULT_PREFIX_COLOR_BOLD + "Hugs &8&l>> &8&l&m     &r &8&l<" + primaryColor + "+&8&l> &8&l»" + secondaryColor + "&l»" + primaryColor + "&l» "));

//    player.sendMessage(ChatUtils.colorChat(primaryColor + "&lHugs &8&l>> &7Developed by: " + primaryColor + "Nothixal"));
//    player.sendMessage(ChatUtils.colorChat("&8[" + primaryColor + "Hugs&8] &7Version: &a" + plugin.getDescription().getVersion()));
//    player.sendMessage(ChatUtils.colorChat("&8[" + primaryColor + "Hugs&8] &7Use " + primaryColor + "/hugs help &7to view available commands."));

    BaseComponent[] createdBy = new ComponentBuilder()
        .append(PluginConstants.PLUGIN_CREATOR).color(ChatColor.of(primaryHex))
        .event(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("Click to view other plugins I've made!").color(ChatColor.YELLOW).create()))
        .event(new ClickEvent(ClickEvent.Action.OPEN_URL, PluginConstants.PLUGIN_AUTHOR_HOMEPAGE))
        .create();

    BaseComponent[] callToAction = new ComponentBuilder()
        .append("/hugs help").color(ChatColor.of(primaryHex))
        .event(new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("Click to execute the command!").color(ChatColor.GRAY).create()))
        .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/hugs help"))
        .create();

    BaseComponent[] pluginInfo = new ComponentBuilder()
        .append("Hugs").color(ChatColor.of(primaryHex)).bold(true).append(" >> ").color(ChatColor.DARK_GRAY).append("").reset()
        .append("Developed by: ").color(ChatColor.GRAY).append(createdBy).append("").reset()
        .append("\n")
        .append("Hugs").color(ChatColor.of(primaryHex)).bold(true).append(" >> ").color(ChatColor.DARK_GRAY).append("").reset()
        .append("Version: ").color(ChatColor.GRAY).append(plugin.getDescription().getVersion()).color(ChatColor.GREEN)
        .append("\n")
        //.append("Created By: ").color(ChatColor.GRAY).append(createdBy).append("").reset()
        //.append("\n")
        .append("Type ").color(ChatColor.GRAY)
        .append(callToAction).append(" ").reset()
        .append("for all of the plugin commands!").color(ChatColor.GRAY)
        .create();

    player.spigot().sendMessage(pluginInfo);

    //player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l--------&8&l<&3❤&8&l>&m&l--------&8&l<<&b❤&8&l>>&m&l--------&8&l<&3❤&8&l>&m&l--------&8&l>"));
  }

}
