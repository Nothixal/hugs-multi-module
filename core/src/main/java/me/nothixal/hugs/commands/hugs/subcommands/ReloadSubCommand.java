package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.configuration.Messages.Lists;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ReloadSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public ReloadSubCommand(HugsPlugin plugin) {
    super("reload", Permissions.RELOAD.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    reload(sender);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }

  private void reload(CommandSender sender) {
    plugin.reload(sender);

    for (String s : Lists.RELOAD.getLangValue()) {
      sender.sendMessage(ChatUtils.colorChat(s));
    }
  }
}
