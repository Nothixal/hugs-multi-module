package me.nothixal.hugs.commands;

import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public abstract class SubCommand {

  protected final String name;
  protected String description;
  protected final String permissionNode;
  protected final boolean isConsoleAllowed;

  public SubCommand(String name, String permissionNode, boolean isConsoleAllowed) {
    this.name = name;
    this.permissionNode = permissionNode;
    this.isConsoleAllowed = isConsoleAllowed;
  }

  public abstract boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

  public abstract List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args);

  public String getName() {
    return name;
  }

  public String getPermissionNode() {
    return this.permissionNode;
  }

  public Boolean isConsoleAllowed() {
    return this.isConsoleAllowed;
  }
}