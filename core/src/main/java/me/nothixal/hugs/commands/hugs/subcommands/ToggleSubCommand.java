package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.managers.data.PlayerDataManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public ToggleSubCommand(HugsPlugin plugin) {
    super("toggle", Permissions.TOGGLE.getPermissionNode(), false);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    PlayerDataManager dataManager = plugin.getPlayerDataManager();

    dataManager.setWantsHugs(player.getUniqueId(), !dataManager.wantsHugs(player.getUniqueId()));

    if (dataManager.wantsHugs(player.getUniqueId())) {
      player.sendMessage("You are now huggable!");
    } else {
      player.sendMessage("You are no longer huggable!");
    }


    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }
}
