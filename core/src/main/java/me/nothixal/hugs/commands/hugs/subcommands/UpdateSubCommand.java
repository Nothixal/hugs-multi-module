package me.nothixal.hugs.commands.hugs.subcommands;

import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.commands.SubCommand;
import me.nothixal.hugs.enums.Permissions;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UpdateSubCommand extends SubCommand {

  private final HugsPlugin plugin;

  public UpdateSubCommand(HugsPlugin plugin) {
    super("update", Permissions.UPDATE.getPermissionNode(), true);
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      plugin.getPluginUpdater().checkForUpdate();
      return true;
    }

    Player player = (Player) sender;

    plugin.getPluginUpdater().sendUpdateMessage(player);
    return false;
  }

  @Override
  public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
    return null;
  }
}
