package me.nothixal.hugs.commands;

import com.cryptomorin.xseries.XMaterial;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.enums.Cooldowns;
import me.nothixal.hugs.enums.Permissions;
import me.nothixal.hugs.enums.Type;
import me.nothixal.hugs.enums.configuration.Errors;
import me.nothixal.hugs.enums.configuration.Messages;
import me.nothixal.hugs.enums.configuration.Settings;
import me.nothixal.hugs.enums.configuration.Usages;
import me.nothixal.hugs.utils.Cooldown;
import me.nothixal.hugs.utils.NewCooldown;
import me.nothixal.hugs.utils.chat.ChatUtils;
import me.nothixal.hugs.utils.logger.LogUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class HugCommand implements CommandExecutor {

  private Map<Player, Boolean> invinciblePlayers = new HashMap<>();

  private final List<String> everyone = Arrays.asList("@a", "*", "everyone", "everybody", "all");
  private final List<String> random = Arrays.asList("@r", "random", "rand", "someone", "somebody");

  private final HugsPlugin plugin;

  public HugCommand(HugsPlugin plugin) {
    this.plugin = plugin;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (!(sender instanceof Player)) {
      sender.sendMessage("Player only command!");
      return true;
    }

    Player player = (Player) sender;
    final Player receiver;

    int length = args.length;

    if (length == 0) {
      String hugCmdCooldown = Cooldowns.HUG_COMMAND_COOLDOWN.getValue();

      // Prevent the user from spamming the command with no arguments. For whatever reason that might be.
      if (Cooldown.hasCooldown(player.getUniqueId(), hugCmdCooldown)) {
        return true;
      }

      player.sendMessage(ChatUtils.colorChat(Usages.HUG.getLangValue()));
      new Cooldown(player, hugCmdCooldown, Cooldowns.HUG_COMMAND_COOLDOWN.getCooldown()).start();
      return true;
    }

    if (length > 1) {
      player.sendMessage(ChatUtils.colorChat(Errors.TOO_MANY_ARGUMENTS.getLangValue()));
      return true;
    }

    String selfCooldown = Cooldowns.SELF_HUG_COOLDOWN.getValue();
    String normalCooldown = Cooldowns.NORMAL_HUG_COOLDOWN.getValue();
    String massCooldown = Cooldowns.MASS_HUG_COOLDOWN.getValue();

    String cooldownError = Errors.COOLDOWN_MESSAGE.getLangValue();

    // Check to see if the player has a cooldown for any of the hug types.
    if (Cooldown.hasCooldown(player, selfCooldown)) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          Cooldown.getTimeLeft(plugin, player, selfCooldown))));
      return true;
    }

    if (Cooldown.hasCooldown(player, normalCooldown)) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          Cooldown.getTimeLeft(plugin, player, normalCooldown))));
      return true;
    }

    if (Cooldown.hasCooldown(player, massCooldown)) {
      player.sendMessage(ChatUtils.colorChat(cooldownError.replace("%cooldown%",
          Cooldown.getTimeLeft(plugin, player, massCooldown))));
      return true;
    }

    // Check to see if the player is trying to give a mass hug.
    if (everyone.contains(args[0].toLowerCase())) {
      if (!player.hasPermission(Permissions.HUG_ALL.getPermissionNode())) {
        player.sendMessage(ChatUtils.colorChat(
            Errors.NO_PERMISSION.getLangValue().replace("%permission%", Permissions.HUG_ALL.getPermissionNode())));
        return true;
      }

      //TODO Un-Comment when ready to release.
      if (Bukkit.getServer().getOnlinePlayers().size() < 3) {
        player.sendMessage(ChatUtils.colorChat(Errors.NOT_ENOUGH_PLAYERS.getLangValue()));
        return true;
      }

      giveMassHug(player);

      if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_MASS.getPermissionNode())) {
        new Cooldown(player, massCooldown, plugin.getConfig().getInt(Settings.MASS_HUG_COOLDOWN.getValue())).start();
      }

      plugin.getVerboseManager().logVerbose(player.getName() + " gave everyone a hug! :O", Type.INFO);
      return true;
    }

    // Check to see if the player is trying to give a random hug.
    if (random.contains(args[0].toLowerCase())) {
      List<Player> players = new ArrayList<>(Bukkit.getServer().getOnlinePlayers());

      players.remove(player);
      players.removeIf(p -> !plugin.getPlayerDataManager().wantsHugs(p.getUniqueId()));

      if (players.size() == 0) {
        player.sendMessage(ChatUtils.colorChat(Errors.NO_RANDOM_PLAYERS.getLangValue()));
        return true;
      }

      giveHug(player, players.get(new Random().nextInt(players.size())));
      return true;
    }

    // Check to see if the player is real.
    if (args[0].length() > 16) {
      player.sendMessage(ChatUtils.colorChat(Errors.INVALID_PLAYER.getLangValue()));
      return true;
    }

    // Check to see if the server owner wanted to force exact usernames when using the command.
    if (plugin.getConfig().getBoolean(Settings.FORCE_EXACT_NAMES.getValue())) {
      receiver = Bukkit.getServer().getPlayerExact(args[0]);
    } else {
      receiver = Bukkit.getServer().getPlayer(args[0]);
    }

    // Check to see if the player is online.
    if (receiver == null) {
      player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_NOT_FOUND.getLangValue().replace("%target%", args[0])));
      return true;
    }

    // Check to see if the player is also the receiver.
    if (player == receiver) {
      if (!plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue())) {
        player.sendMessage(ChatUtils.colorChat(Errors.NO_SELF_HUG.getLangValue()));
        return true;
      }

      giveSelfHug(player);
      new Cooldown(player, selfCooldown, plugin.getConfig().getInt(Settings.SELF_HUG_COOLDOWN.getValue())).start();

      plugin.getVerboseManager().logVerbose(player.getName() + " hugged themself.", Type.INFO);
      return true;
    }

    giveHug(player, receiver);

    if (!player.isOp() && !player.hasPermission(Permissions.BYPASS_NORMAL.getPermissionNode())) {
      new NewCooldown(player, normalCooldown, 5);

      new Cooldown(player, normalCooldown, plugin.getConfig().getInt(Settings.NORMAL_HUG_COOLDOWN.getValue())).start();
    }

    plugin.getVerboseManager().logVerbose(player.getName() + " gave " + receiver.getName() + " a hug. :3", Type.INFO);
    return true;
  }

  private void giveSelfHug(Player player) {
    // Check to see if the receiver is huggable.
    if (!plugin.getPlayerDataManager().wantsHugs(player.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(Errors.SELF_NOT_HUGGABLE.getLangValue()));
      return;
    }

//    if (plugin.getConfig().getBoolean(Settings.OVERRIDES.getValue()) && plugin.getFileManager().getOverridesYMLData().getBoolean(Settings.TITLE_MESSAGES_ENABLED.getValue())) {
//      System.out.println("Both are true");
//    }

    // Check Global Settings for chat.
//    if (plugin.getConfig().getBoolean(Settings.CHAT_MESSAGES_ENABLED.getValue())) {
//      // Check Player's Settings for chat.
//      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "chat")) {
//        sendSelfHugChatMessage(player);
//      }
//    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "chat")) {
      sendSelfHugChatMessage(player);
    }

    // Check Global Settings
//    if (plugin.getConfig().getBoolean(Settings.ACTIONBAR_MESSAGES_ENABLED.getValue())) {
//      // Check Player's Settings for actionbar.
//      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "actionbar")) {
//        sendSelfHugActionBar(player);
//      }
//    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "actionbar")) {
      sendSelfHugActionBar(player);
    }

    // Check Global Settings
//    if (plugin.getConfig().getBoolean(Settings.BOSSBAR_MESSAGES_ENABLED.getValue())) {
//      // Check Player's Settings for titles.
//      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "bossbar")) {
//        sendSelfHugBossbar(player);
//      }
//    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "bossbar")) {
      sendSelfHugBossbar(player);
    }

    // Check Global Settings
//    if (plugin.getConfig().getBoolean(Settings.TITLE_MESSAGES_ENABLED.getValue())) {
//      // Check Player's Settings for the bossbar.
//      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "titles")) {
//        sendTitle(player, player);
//      }
//    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "titles")) {
      sendTitle(player, player);
    }

    // Check Global Settings
//    if (plugin.getConfig().getBoolean(Settings.TOAST_MESSAGES_ENABLED.getValue())) {
//      // Check Player's Settings for toast notifications.
//      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "toast")) {
//        sendSelfHugToast(player);
//      }
//    }

    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "toast")) {
      sendSelfHugToast(player);
    }

    if (plugin.getConfig().getBoolean(Settings.SOUND_ENABLED.getValue())) {
      if (plugin.getPlayerDataManager().wantsSounds(player.getUniqueId())) {
        playSound(player);
      }
    }

    if (plugin.getConfig().getBoolean(Settings.PARTICLES_ENABLED.getValue())) {
      sendParticles(player);
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
      restoreHealth(player, player);
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
      restoreHunger(player, player);
    }

    if (plugin.getConfig().getBoolean(Settings.BROADCAST_SELF_HUGS.getValue())) {
      broadcastSelfHug(player, player);
    }

    if (plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      invinciblePlayers.put(player, false);
    }

    plugin.getPlayerDataManager().incrementSelfHugs(player.getUniqueId());

    plugin.getDataFile().set("total_self_hugs_given", plugin.getDataFile().getInt("total_self_hugs_given") + 1);
    plugin.saveData();
  }

  private void giveHug(Player player, Player receiver) {
    // Check to see if the receiver is huggable.
    if (!plugin.getPlayerDataManager().wantsHugs(receiver.getUniqueId())) {
      player.sendMessage(ChatUtils.colorChat(Errors.PLAYER_NOT_HUGGABLE.getLangValue().replace("%target%", receiver.getName())));
      return;
    }

    //sendChatMessageToSender(player, receiver);

    // Player Settings

    // Check Global Settings for chat.
    if (plugin.getConfig().getBoolean(Settings.CHAT_MESSAGES_ENABLED.getValue())) {
      if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "chat")) {
        sendChatMessageToSender(player, receiver);
      }

      // Check Player's Settings for chat.
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), "chat")) {
        sendChatMessageToReceiver(player, receiver);
      }
    }

    // Check Global Settings
    if (plugin.getConfig().getBoolean(Settings.ACTIONBAR_MESSAGES_ENABLED.getValue())) {
      // Check Player's Settings for actionbar.
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), "actionbar")) {
        sendActionBar(player, receiver);
      }
    }

    // Check Global Settings
    if (plugin.getConfig().getBoolean(Settings.BOSSBAR_MESSAGES_ENABLED.getValue())) {
      // Check Player's Settings for titles.
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), "bossbar")) {
        sendBossbar(player, receiver);
      }
    }

    // Check Global Settings
    if (plugin.getConfig().getBoolean(Settings.TITLE_MESSAGES_ENABLED.getValue())) {
      // Check Player's Settings for the bossbar.
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), "titles")) {
        sendTitle(player, receiver);
      }
    }

    // Check Global Settings
    if (plugin.getConfig().getBoolean(Settings.TOAST_MESSAGES_ENABLED.getValue())) {
      // Check Player's Settings for toast notifications.
      if (plugin.getPlayerDataManager().getIndicator(receiver.getUniqueId(), "toast")) {
        sendToast(player, receiver);
      }
    }

    if (plugin.getConfig().getBoolean(Settings.PARTICLES_ENABLED.getValue())) {
      if (plugin.getPlayerDataManager().wantsParticles(receiver.getUniqueId())) {
        sendParticles(receiver);
      }
    }

    if (plugin.getConfig().getBoolean(Settings.SOUND_ENABLED.getValue())) {
      if (plugin.getPlayerDataManager().wantsSounds(receiver.getUniqueId())) {
        playSound(receiver);
      }
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
      restoreHealth(player, receiver);
    }

    if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
      restoreHunger(player, receiver);
    }

    if (plugin.getConfig().getBoolean(Settings.BROADCAST_NORMAL_HUGS.getValue())) {
      broadcastHug(player, receiver);
    }

    if (plugin.getConfig().getBoolean(Settings.TEMPORARY_INVINCIBILITY_ENABLED.getValue())) {
      invinciblePlayers.put(receiver, false);
    }

    if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
      checkDeveloper(player, receiver);
    }

    plugin.getPlayerDataManager().incrementHugsGiven(player.getUniqueId());
    plugin.getPlayerDataManager().incrementHugsReceived(receiver.getUniqueId());

    plugin.getDataFile().set("total_hugs_given", plugin.getDataFile().getInt("total_hugs_given") + 1);
    plugin.saveData();
  }

  private void giveMassHug(Player player) {
    for (Player receiver : Bukkit.getServer().getOnlinePlayers()) {
      if (!plugin.getConfig().getBoolean(Settings.ALLOW_SELF_HUG.getValue()) && receiver == player) {
        continue;
      }

      if (plugin.getConfig().getBoolean(Settings.CHAT_MESSAGES_ENABLED.getValue())) {
        //sendChatMessageToSender(player, receiver);
        sendChatMessageToReceiver(player, receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.TITLE_MESSAGES_ENABLED.getValue())) {
        sendTitle(player, receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.PARTICLES_ENABLED.getValue())) {
        sendParticles(receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.SOUND_ENABLED.getValue())) {
        playSound(receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HEALTH.getValue())) {
        restoreHealth(player, receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.RESTORE_PLAYER_HUNGER.getValue())) {
        restoreHunger(player, receiver);
      }

      if (plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
        checkDeveloper(player, receiver);
      }

      plugin.getPlayerDataManager().incrementMassHugsReceived(receiver.getUniqueId());
    }

    if (plugin.getConfig().getBoolean(Settings.BROADCAST_MASS_HUGS.getValue())) {
      broadcastMassHug(player);
    }

    plugin.getPlayerDataManager().incrementMassHugsGiven(player.getUniqueId());

    plugin.getDataFile().set("total_mass_hugs_given", plugin.getDataFile().getInt("total_mass_hugs_given") + 1);
    plugin.saveData();
    //HugEvent hugEvent = new HugEvent(player, player, HugType.MASS);
    //Bukkit.getPluginManager().callEvent(hugEvent);
  }

  private void sendSelfHugChatMessage(Player player) {
    String messageToSender = Messages.SELF_HUG.getLangValue();
    player.sendMessage(ChatUtils.colorChat(messageToSender));
  }

  private void sendChatMessageToSender(Player player, Player receiver) {
    String messageToSender = Messages.MESSAGE_TO_SENDER.getLangValue().replace("%target%", receiver.getName());
    player.sendMessage(ChatUtils.colorChat(messageToSender));
  }

  private void sendChatMessageToReceiver(Player player, Player receiver) {
    String messageToReceiver = Messages.MESSAGE_TO_RECEIVER.getLangValue().replace("%sender%", player.getName());
    receiver.sendMessage(ChatUtils.colorChat(messageToReceiver));
  }

  private void broadcastHug(Player player, Player receiver) {
    String hugsBroadcast = Messages.BROADCAST_NORMAL_HUG.getLangValue().replace("%sender%", player.getName());
    hugsBroadcast = hugsBroadcast.replace("%target%", receiver.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(hugsBroadcast));
  }

  private void broadcastMassHug(Player player) {
    String massHugsBroadcast = Messages.BROADCAST_MASS_HUG.getLangValue().replace("%sender%", player.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(massHugsBroadcast));
  }

  private void broadcastSelfHug(Player player, Player receiver) {
    String massHugsBroadcast = Messages.BROADCAST_SELF_HUG.getLangValue().replace("%sender%", player.getName());
    massHugsBroadcast = massHugsBroadcast.replace("%target%", receiver.getName());
    Bukkit.broadcastMessage(ChatUtils.colorChat(massHugsBroadcast));
  }

  private void sendTitle(Player player, Player receiver) {
    String hugsTitle = Messages.TITLE_MESSAGE.getLangValue().replace("%sender%", player.getName());
    String hugsSubTitle = Messages.SUBTITLE_MESSAGE.getLangValue().replace("%sender%", player.getName());
    int fadeIn = plugin.getConfig().getInt(Settings.TITLE_FADE_IN.getValue());
    int stay = plugin.getConfig().getInt(Settings.TITLE_STAY.getValue());
    int fadeOut = plugin.getConfig().getInt(Settings.TITLE_FADE_OUT.getValue());

    receiver.sendTitle(ChatUtils.colorChat(hugsTitle), null, fadeIn * 20, stay * 20, fadeOut * 20);
    receiver.sendTitle(null, ChatUtils.colorChat(hugsSubTitle), fadeIn * 20, stay * 20, fadeOut * 20);
  }

  private void sendSelfHugActionBar(Player player) {
    String message = Messages.ACTIONBAR_SELF_HUG.getLangValue();
    plugin.getChatManager().sendActionBar(player, ChatUtils.colorChat(message));
  }

  private void sendActionBar(Player player, Player receiver) {
    String message = Messages.ACTIONBAR_NORMAL_HUG.getLangValue().replace("%player%", player.getName());
    plugin.getChatManager().sendActionBar(receiver, ChatUtils.colorChat(message));
  }

  private void sendSelfHugBossbar(Player player) {
    String message = Messages.BOSSBAR_SELF_HUG.getLangValue();
    doBossbar(player, message);
  }

  private void sendBossbar(Player player, Player receiver) {
    String message = Messages.MESSAGE_TO_RECEIVER.getLangValue().replace("%sender%", player.getName());
    doBossbar(receiver, message);
  }

  private void doBossbar(Player player, String message) {
    BossBar bossBar = Bukkit.createBossBar(ChatUtils.colorChat(message), BarColor.BLUE, BarStyle.SOLID);
    bossBar.setProgress(0.0D);

    bossBar.addPlayer(player);

    new BukkitRunnable() {
      int counter = 0;

      @Override
      public void run() {
        if (counter < 60) {
          counter++;
          bossBar.setProgress(counter / 60D);
        } else {
          bossBar.removePlayer(player);
          cancel();
        }
      }
    }.runTaskTimerAsynchronously(plugin, 0, 1);
  }

  private void sendSelfHugToast(Player player) {
    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwningPlayer(player);

    item.setItemMeta(meta);

    plugin.getAdvancementManager().sendToast(item, player.getUniqueId(), ChatUtils.colorChat(Messages.TOAST_SELF_HUG.getLangValue()), "Self Hug");
  }

  private void sendToast(Player player, Player receiver) {
    ItemStack item = new ItemStack(Material.PLAYER_HEAD);
    SkullMeta meta = (SkullMeta) item.getItemMeta();
    meta.setOwningPlayer(player);

    item.setItemMeta(meta);

    String message = Messages.TOAST_NORMAL_HUG.getLangValue().replace("%player%", player.getName());

    plugin.getAdvancementManager().sendToast(item, receiver.getUniqueId(), ChatUtils.colorChat(message), "404 Description not found.");
  }

  private void sendParticles(Player receiver) {
    String particleEffect = plugin.getConfig().getString(Settings.PARTICLE_EFFECT.getValue(), "HEART").toUpperCase();

    Particle particle = Particle.valueOf(particleEffect);

    if (particle == null) {
      LogUtils.logWarning(
          "&eWARNING: &rThe particle effect defined in the config.yml (" + particleEffect + ") is invalid!");

      LogUtils.logWarning("Go to &a"
          + "https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Particle.html"
          + " &rfor a list of valid particle effects.");

      return;
    }

    // spawnParticle(<PARTICLE>, <LOCATION>, <AMOUNT OF PARTICLES>, <X OFFSET>, <Y OFFSET>, <Z OFFSET>);
    if (plugin.getConfig().getBoolean(Settings.GLOBAL_PARTICLES.getValue())) {
      receiver.getWorld().spawnParticle(particle, receiver.getLocation().add(0.0D, 1.0D, 0.0D), 10, 0.4D, 0.5D, 0.4D);
    } else {
      receiver.spawnParticle(particle, receiver.getLocation().add(0.0D, 1.0D, 0.0D), 10, 0.4D, 0.5D, 0.4D);
    }

  }

  private void playSound(Player receiver) {
    String soundEffect = plugin.getConfig().getString(Settings.SOUND_EFFECT.getValue(), "ENTITY_CAT_PURR").toUpperCase();
    int volume = plugin.getConfig().getInt(Settings.SOUND_VOLUME.getValue());
    int pitch = plugin.getConfig().getInt(Settings.SOUND_PITCH.getValue());

    boolean validVolume = true;
    if (volume < 0 || volume > 10) {
      LogUtils.logWarning("The volume is the config.yml is invalid!");
      LogUtils.logWarning("Volume was set to the max value.");
      validVolume = false;
    }

    boolean validPitch = true;
    if (pitch < 0 || pitch > 16) {
      LogUtils.logWarning("The pitch is the config.yml is invalid!");
      LogUtils.logWarning("Pitch was set to the default value.");
      validPitch = false;
    }

    float realVolume = getRealVolume(volume, validVolume);
    float realPitch = getRealPitch(pitch, validPitch);

    try {
      if (plugin.getConfig().getBoolean(Settings.GLOBAL_SOUND.getValue())) {
        receiver.getWorld().playSound(receiver.getLocation(), Sound.valueOf(soundEffect), realVolume, realPitch);
      } else {
        receiver.playSound(receiver.getLocation(), Sound.valueOf(soundEffect), realVolume, realPitch);
      }
    } catch (IllegalArgumentException ex) {
      LogUtils.logWarning("&eWARNING: &rThe sound effect defined in the config.yml (" + soundEffect + ") is invalid!");

      LogUtils.logWarning("Go to &a"
          + "https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Sound.html"
          + " &rfor a list of valid sound effects.");
    }
  }

  private float getVolume(float configVolume) {
    if (configVolume > 10F) {
      configVolume = 10F;
    } else if (configVolume < 1F) {
      configVolume = 1F;
    }
    return configVolume;
  }

  private float getRealVolume(float configVolume, boolean isValid) {
    final float defaultVolume = isValid ? 0.1F : 1.0F;
    float volume = getVolume(configVolume);
    if (volume < 1F) {
      return defaultVolume * volume;
    } else {
      float ratio = ((volume - 1) / 9) * (0.8F - defaultVolume);
      return ratio + defaultVolume;
    }
  }

  private float getPitch(float configPitch) {
    if (configPitch > 16F) {
      configPitch = 16F;
    } else if (configPitch < 1F) {
      configPitch = 1F;
    }
    return configPitch;
  }

  private float getRealPitch(float configPitch, boolean isValid) {
    final float defaultPitch = isValid ? 0.5F : 2.0F;
    float pitch = getPitch(configPitch);
    if (pitch < 1F) {
      return defaultPitch * pitch;
    } else {
      float ratio = ((pitch - 1) / 9) * (1.4F - defaultPitch);
      return ratio + defaultPitch;
    }
  }

  private void restoreHealth(Player player, Player receiver) {
    receiver.setHealth(receiver.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
    plugin.getVerboseManager()
        .logVerbose(player.getName() + " restored " + receiver.getName() + "'s health.", Type.INFO);
  }

  private void restoreHunger(Player player, Player receiver) {
    receiver.setFoodLevel(20);
    plugin.getVerboseManager()
        .logVerbose(player.getName() + " restored " + receiver.getName() + "'s hunger.", Type.INFO);
  }

  private void checkDeveloper(Player player, Player receiver) {
    List<String> uuid = new ArrayList<>();
    uuid.add("03687ca030cf4c3c8c1d3b2dd75c3ff0");
    uuid.add("03687ca0-30cf-4c3c-8c1d-3b2dd75c3ff0");

    // If the receiver is not the developer, return.
    if (!uuid.contains(receiver.getUniqueId().toString())) {
      return;
    }

    // If the player has toast notifications enabled, send the developer toast.
    if (plugin.getPlayerDataManager().getIndicator(player.getUniqueId(), "toast")) {
      plugin.getAdvancementManager().sendChallengeToast(plugin.getItemManager().createItem(XMaterial.CAKE), player.getUniqueId(),
          "&eYou hugged the developer of the plugin!", "Congratulations!");
      //sendToast(player, "&eYou hugged the developer of the plugin!");
    } else {
      player.sendMessage(ChatUtils.colorChat("&6You hugged the developer of the plugin! :O"));
    }

    plugin.getVerboseManager().logVerbose(player.getName() + " hugged the developer of the plugin! :O", Type.INFO);

    if (!plugin.getConfig().getBoolean(Settings.DEVELOPER_REWARD.getValue())) {
      return;
    }

    if (player.getInventory().firstEmpty() == -1) {
      player.sendMessage(ChatUtils.colorChat(
          "&c&lATTENTION: &7You hugged the developer and your inventory is full! &7Make space for a reward. :D"));
    } else {
      player.sendMessage(ChatUtils.colorChat("&6Lucky you! Here's a cookie!"));
      player.getInventory().addItem(new ItemStack(Material.COOKIE, 1));
    }

    plugin.getVerboseManager()
        .logVerbose(player.getName() + " was given a cookie because they hugged the developer!", Type.INFO);
  }

  public Map<Player, Boolean> getInvinciblePlayers() {
    return invinciblePlayers;
  }

  public void giveHugs(Player player, Player receiver) {
    giveHug(player, receiver);
  }
}