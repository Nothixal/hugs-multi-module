package me.nothixal.hugs.commands.hugs;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.chat.ChatUtils;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HugsCommand {

  private final HugsPlugin plugin;

  private Map<CommandSender, String> typing = new HashMap<>();
  private Map<Player, String> confirming = new HashMap<>();

  private Map<UUID, UUID> typing2 = new HashMap<>();
  private Map<UUID, UUID> confirming2 = new HashMap<>();

  public HugsCommand(HugsPlugin plugin) {
    this.plugin = plugin;
//    this.requiredArgs = 4;
//    this.playerOnlyCommand = false;
//    this.permissionNode = Permissions.USE.getPermissionNode();
  }

  //@Override
  public void executeCommandAsConsole(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 0) {
      sendPluginInfo(sender);
      return;
    }

//    for (CommandHandler subCmd : subCommands) {
//      if (subCmd.getSubCommandHandler().getSubCommand().equalsIgnoreCase(args[0])) {
//        subCmd.executeCommandAsConsole(sender, cmd, label, args);
//        break;
//      }
//    }
  }

  //@Override
  public void executeCommandAsPlayer(Player player, Command cmd, String label, String[] args) {
//    for (CommandHandler subCmd : subCommands) {
//      if (subCmd.getSubCommandHandler().getSubCommand().equalsIgnoreCase(args[0])) {
//        subCmd.executeCommandAsPlayer(plapluginyer, cmd, label, args);
//        break;
//      }
//    }
  }

  //@Override
  protected void sendNoArgumentsMessage(Player player) {
    sendPluginInfo(player);
  }

  //@Override
  protected void sendNoArgumentsMessage(CommandSender sender) {

  }

//  public List<SubCommandHandler> getSubCommands() {
//    return subCommands;
//  }

  public Map<CommandSender, String> getTyping() {
    return typing;
  }

  public Map<UUID, UUID> getTyping2() {
    return typing2;
  }

  public Map<Player, String> getConfirming() {
    return confirming;
  }

  public Map<UUID, UUID> getConfirming2() {
    return confirming2;
  }

  private void sendPluginInfo(CommandSender sender) {
    sender.sendMessage(ChatUtils.colorChat("&b&l«&3&l«&8&l« &8&l<&3+&8&l> &8&l&m----- &8&l<< &b&lHugs &8&l>> &8&l&m----- &8&l<&3+&8&l> &8&l»&3&l»&b&l»"));
    sender.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Version: &f" + plugin.getDescription().getVersion()));
    sender.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Created by: &bNothixal"));
    sender.sendMessage(ChatUtils.colorChat("&7Type &b/hugs help &7for all of the plugin commands!"));
  }

  private void sendPluginInfo(Player player) {
    //player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l-------&8&l<&3❤&8&l>&m&l-------&8&l<<&b&l hugs &8&l>>&m&l-------&8&l<&3❤&8&l>&m&l-------&8&l>"));
//
//    plugin.getChatManager().sendMessage(player, "[\n"
//        + "  \"\",\n"
//        + "  {\n"
//        + "    \"text\": \"        «\",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"«\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"«  \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"❤\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"> \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"-----\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"strikethrough\": true,\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"strikethrough\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<< \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"Hugs \",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \">>\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"-----\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"strikethrough\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"strikethrough\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"❤\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"> \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" »\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"»\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"»\",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  }\n"
//        + "]");

    player.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Version: &f" + plugin.getDescription().getVersion()));

    plugin.getChatManager().sendMessage(player, "[\n"
        + "  \"\",\n"
        + "  {\n"
        + "    \"text\": \"Hugs \",\n"
        + "    \"color\": \"dark_aqua\",\n"
        + "    \"bold\": true\n"
        + "  },\n"
        + "  {\n"
        + "    \"text\": \">> \",\n"
        + "    \"color\": \"dark_gray\",\n"
        + "    \"bold\": true\n"
        + "  },\n"
        + "  {\n"
        + "    \"text\": \"Created By: \",\n"
        + "    \"color\": \"gray\",\n"
        + "    \"bold\": false\n"
        + "  },\n"
        + "  {\n"
        + "    \"text\": \"Nothixal\",\n"
        + "    \"color\": \"aqua\",\n"
        + "    \"clickEvent\": {\n"
        + "      \"action\": \"open_url\",\n"
        + "      \"value\": \"https://www.spigotmc.org/resources/authors/shadowmasterg23.303901/\"\n"
        + "    },\n"
        + "    \"hoverEvent\": {\n"
        + "      \"action\": \"show_text\",\n"
        + "      \"value\": {\n"
        + "        \"text\": \"\",\n"
        + "        \"extra\": [\n"
        + "          {\n"
        + "            \"text\": \"Click to view other plugins I've made!\",\n"
        + "            \"color\": \"gray\"\n"
        + "          }\n"
        + "        ]\n"
        + "      }\n"
        + "    }\n"
        + "  }\n"
        + "]");

    TextComponent message = new TextComponent("Hello world");
    message.setColor(ChatColor.RED);
    message.setBold(true);
    message.addExtra(ChatColor.RESET + "");

    //player.sendMessage(new TextComponent("&6Test").toLegacyText());


    //player.spigot().sendMessage(new ComponentBuilder("").color(ChatColor.RED).bold(true).append("").create());

    TextComponent command = new TextComponent(ChatUtils.colorChat("&b/hugs help"));
    command.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatUtils.colorChat("&7Click to execute the command!")).create()));
    command.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/hugs help"));

    TextComponent baseMsg = new TextComponent(ChatUtils.colorChat("&7Type "));
    baseMsg.addExtra(command);
    baseMsg.addExtra(ChatUtils.colorChat(" &7for all of the plugin commands!"));

    player.spigot().sendMessage(baseMsg);

//    plugin.getChatManager().sendMessage(player, "[\n"
//        + "  \"\",\n"
//        + "  {\n"
//        + "    \"text\": \"           «\",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"«\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"« \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"❤\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"> \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"-----\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"strikethrough\": true,\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"strikethrough\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<< \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"❤ \",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \">>\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"-----\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"strikethrough\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \" \",\n"
//        + "    \"color\": \"none\",\n"
//        + "    \"strikethrough\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"<\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"❤\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": false\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"> \",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"»\",\n"
//        + "    \"color\": \"dark_gray\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"»\",\n"
//        + "    \"color\": \"dark_aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  },\n"
//        + "  {\n"
//        + "    \"text\": \"»\",\n"
//        + "    \"color\": \"aqua\",\n"
//        + "    \"bold\": true\n"
//        + "  }\n"
//        + "]");
    //player.sendMessage(ChatUtils.colorChat("&8&l<&8&m&l--------&8&l<&3❤&8&l>&m&l--------&8&l<<&b❤&8&l>>&m&l--------&8&l<&3❤&8&l>&m&l--------&8&l>"));
  }

  private void sendLightPluginInfo(Player player) {
    player.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Running version " + plugin.getDescription().getVersion()));
    player.sendMessage(ChatUtils.colorChat("&3&lHugs &8&l>> &7Type &b/hugs help &7for all of the plugin commands!"));
  }

  private TextComponent getDoubleArrowLeft() {
    return new TextComponent("«");
  }

  private TextComponent getDoubleArrowRight() {
    return new TextComponent("»");
  }

  private TextComponent getDoubleArrowRight(ChatColor color) {
    TextComponent textComponent = new TextComponent("»");
    textComponent.setColor(color);

    return textComponent;
  }

}
