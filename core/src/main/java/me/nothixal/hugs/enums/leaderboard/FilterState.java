package me.nothixal.hugs.enums.leaderboard;

public enum FilterState {

  NORMAL("Normal Hugs"),
  MASS("Mass Hugs"),
  SELF("Self Hugs"),
//  UNIQUE("Unique Hugs"),
  ;

  private final String value;

  private static final FilterState[] values = values();

  FilterState(String value) {
    this.value = value;
  }

  public FilterState getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public FilterState getPrevious() {
    return values[(this.ordinal() - 1) % values.length];
  }

  public String getValue() {
    return value;
  }
}
