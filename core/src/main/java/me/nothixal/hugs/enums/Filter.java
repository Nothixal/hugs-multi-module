package me.nothixal.hugs.enums;

public enum Filter {

  HUGS_GIVEN("Hugs Given"),
  HUGS_RECEIVED("Hugs Received"),
  MASS_HUGS_GIVEN("Mass Hugs Given"),
  MASS_HUGS_RECEIVED("Mass Hugs Received"),
  SELF_GIVEN("Self Hugs Given"),
  ;

  private final String value;
  private static Filter[] values = values();

  Filter(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public Filter getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

}
