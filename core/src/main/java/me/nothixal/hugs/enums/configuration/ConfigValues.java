package me.nothixal.hugs.enums.configuration;

public enum ConfigValues {

  SELF_HUGS("self_hugs"),
  HUGS_GIVEN("normal_hugs.given"),
  HUGS_RECEIVED("normal_hugs.received"),
  MASS_HUGS_GIVEN("mass_hugs.given"),
  MASS_HUGS_RECEIVED("mass_hugs.received"),
  WANTS_HUGS("settings.huggable"),
  WANTS_SOUNDS("settings.sounds"),
  ;

  private final String path;

  ConfigValues(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
