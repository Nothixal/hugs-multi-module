package me.nothixal.hugs.enums.types;

public enum FileType {
  YML("yml"),
  TEXT("txt"),
  ;

  private final String value;

  FileType(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
