package me.nothixal.hugs.enums.configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import me.nothixal.hugs.HugsPlugin;
import me.nothixal.hugs.utils.chat.ChatUtils;
import org.bukkit.plugin.java.JavaPlugin;

public enum Messages {
  PREFIX("plugin_placeholders.prefix", "&3&lHugs &8&l>>"),
  HEADER("plugin_placeholders.header", "&8&l<&8&m&l-------&8&l<&3+&8&l>&m&l--------&8&l<<&b&l Hugs &8&l>>&m&l--------&8&l<&3+&8&l>&m&l-------&8&l>"),
  FOOTER("plugin_placeholders.footer", "&8&l<&8&m&l--------&8&l<&3+&8&l>&m&l---------&8&l<<&3+&8&l>>&m&l---------&8&l<&3+&8&l>&m&l--------&8&l>"),

  MESSAGE_TO_SENDER("messages.chat_messages.message_to_sender", "%prefix% &3You gave &7&l%target% &3a hug!"),
  MESSAGE_TO_RECEIVER("messages.chat_messages.message_to_receiver", "%prefix% &3You were hugged by &7&l%sender%&3!"),
  SELF_HUG("messages.chat_messages.self_hug", "%prefix% &7You hugged yourself."),

  ACTIONBAR_SELF_HUG("messages.actionbar_messages.self_hug", "&7You gave yourself a warm hug."),
  ACTIONBAR_NORMAL_HUG("messages.actionbar_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  ACTIONBAR_MASS_HUG("messages.actionbar_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  BOSSBAR_SELF_HUG("messages.bossbar_messages.self_hug", "&7You gave yourself a warm hug."),
  BOSSBAR_NORMAL_HUG("messages.bossbar_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  BOSSBAR_MASS_HUG("messages.bossbar_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  TITLE_MESSAGE("messages.title_message.title", "&3%sender%"),
  SUBTITLE_MESSAGE("messages.title_message.subtitle", "&7Hugged you!"),

  TOAST_SELF_HUG("messages.toast_messages.self_hug", "&7You gave yourself a warm hug."),
  TOAST_NORMAL_HUG("messages.toast_messages.normal_hug", "&e%player% &7gave you a warm hug. Awww &c<3"),
  TOAST_MASS_HUG("messages.toast_messages.mass_hug", "&e%player% &7gave everyone a hug!"),

  BROADCAST_SELF_HUG("messages.broadcasts.self_hugs.message", "%prefix% &7&l%sender% &3hugged themself."),
  BROADCAST_NORMAL_HUG("messages.broadcasts.normal_hugs.message", "%prefix% &7&l%sender% &3hugged &7&l%target%"),
  BROADCAST_MASS_HUG("messages.broadcasts.mass_hugs.message", "%prefix% &7&l%sender% &3gave everyone a hug!"),

  CONFIRMATION_ALL_MESSAGE("messages.confirmations.confirmation_all_message", "%prefix% &7Are you sure? Doing this will result in all player data being deleted! \nType \"yes\" to continue or type \"no\" to cancel."),
  CONFIRMATION_MESSAGE("messages.confirmations.confirmation_message", "%prefix% &7Are you sure? Doing this will result in %player%'s data being deleted! \nType \"yes\" to continue or type \"no\" to cancel."),

  CANCELLED_PLAYER_DATA_PURGE("messages.confirmations.cancelled_purge", "%prefix% &7Canceled data purge."),
  CANCELLED_FULL_DATA_PURGE("messages.confirmations.canceled_purge", "%prefix% &7Canceled data purge."),
  PURGED_ALL_DATA("messages.confirmations.data_purged", "%prefix% &7Everyone's data was &cdeleted&7!"),
  PURGED_PLAYER_DATA("messages.data.player_data_purged", "%prefix% &3%target%'s &7data was &cdeleted&7!"),

  CONFIRMATION_GUI_NAME("gui_names.confirmation", "&3&lHugs &8&l>> &8Confirmation Page"),
  LEADERBOARD_GUI_NAME("gui_names.leaderboard", "&3&lHugs &8&l>> &8Leaderboard"),

  PASSIVE_MODE("messages.experimental.passive_mode", "%prefix% &3Passive mode is enabled. You cannot harm other players!");

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private final String value;
  private final String defaultValue;

  Messages(String value, String defaultValue) {
    this.value = value;
    this.defaultValue = defaultValue;
  }

  public String getLangValue() {
    return plugin.getLangFile().getString(value, defaultValue).replace("%prefix%", plugin.getLangFile().getString("plugin_placeholders.prefix"));
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }

  public enum Lists {
    RELOAD("messages.reload_message", Arrays.asList(
        "%header%",
        "%prefix% &b&lConfiguration Reloaded",
        "%footer%")),
    TOTAL("data.total_hugs_message", Arrays.asList(
        "%header%",
        "%prefix% &3%normal_hugs_total% &7hugs have been given!",
        "%prefix% &3%mass_hugs_total% &7mass hugs have been given!",
        "%footer%")),
    INFO("data.player_profile", Arrays.asList(
        "&3&lHugs Given&7: &b%total_hugs_given%",
        "&3&lHugs Received&7: &b%total_hugs_received%",
        "&3&lMass hugs Given&7: &b%total_mass_hugs_given%",
        "&3&lMass hugs Received&7: &b%total_mass_hugs_received%"));

    private final String path;
    private final List<String> defaultValue;

    Lists(String path, List<String> defaultValue) {
      this.path = path;
      this.defaultValue = defaultValue;
    }

    public List<String> getLangValue() {
      List<String> modified = new ArrayList<>();

      for (String s : plugin.getLangFile().getStringList(path)) {
        s = s.replace("%prefix%", ChatUtils.colorChat(Messages.PREFIX.getLangValue()));
        s = s.replace("%header%", ChatUtils.colorChat(Messages.HEADER.getLangValue()));
        s = s.replace("%footer%", ChatUtils.colorChat(Messages.FOOTER.getLangValue()));
        modified.add(s);
      }

      return modified;
    }

    public String getValue() {
      return path;
    }

    public List<String> getDefaultValue() {
      return defaultValue;
    }

    @Override
    public String toString() {
      return path;
    }

  }

}
