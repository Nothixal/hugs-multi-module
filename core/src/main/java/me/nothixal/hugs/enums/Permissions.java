package me.nothixal.hugs.enums;

public enum Permissions {

  USE("hugs.use"),

  HUG("hugs.hug"),
  HUG_ALL("hugs.hug.everyone"),
  HUG_RANDOM("hugs.hug.random"),

  BYPASS_SELF("hugs.bypass.self"),
  BYPASS_NORMAL("hugs.bypass.normal"),
  BYPASS_MASS("hugs.bypass.mass"),

  STATS("hugs.stats"),
  STATS_OTHERS("hugs.stats.others"),

  TOGGLE("hugs.toggle"),
  TOTAL("hugs.total"),
  LEADERBOARD("hugs.leaderboard"),
  INFO("hugs.info"),
  ADMIN("hugs.admin"),
  PURGE("hugs.purge"),
  PURGE_ALL("hugs.purge.everyone"),

  SETTINGS("hugs.settings"),

  PREFERENCES("hugs.preferences"),

  MENU("hugs.menu"),
  MENU_INFO("hugs.menu.plugin.info"),
  MENU_PLUGIN_SETTINGS("hugs.menu.plugin.settings"),

  UPDATE("hugs.update"),
  RELOAD("hugs.reload"),
  ;

  private final String permissionNode;

  Permissions(String permissionNode) {
    this.permissionNode = permissionNode;
  }

  public String getPermissionNode() {
    return permissionNode;
  }

  @Override
  public String toString() {
    return permissionNode;
  }

}
