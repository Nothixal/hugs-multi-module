package me.nothixal.hugs.enums;

public enum ItemState {

  DISABLED,
  OVERRIDDEN,
  UNMODIFIABLE,
  ACTIVE,
  ;

  ItemState() {

  }

}
