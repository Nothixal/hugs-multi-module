package me.nothixal.hugs.enums.configuration;

import me.nothixal.hugs.HugsPlugin;
import org.bukkit.plugin.java.JavaPlugin;

public enum Errors {

  SELF_NOT_HUGGABLE("errors.self_not_huggable", "%prefix% &7You can't hug yourself because you aren't huggable!"),

  PLAYER_NOT_HUGGABLE("errors.player_not_huggable", "%prefix% &6&l%target% &7doesn't want to be hugged. :'("),
  PLAYER_NOT_FOUND("errors.player_not_found", "%prefix% &6&l%target% &7couldn't be found! Are they online?"),

  NOT_ENOUGH_PLAYERS("errors.not_enough_players", "&c&lERROR: &7At least three players must be online in order to give a mass hug!"),
  NO_RANDOM_PLAYERS("errors.no_random_players", "%prefix% &c&lERROR: &7No one wanted to be hugged. :("),

  NO_SELF_HUG("errors.self_hug_disallowed", "%prefix% &c&lHmm &7well... this is awkward. It seems as if something is preventing you from hugging yourself."),

  INVALID_ARGUMENTS("errors.invalid_arguments", "%prefix% &7Type &b/hugs help &7for the proper command usages!"),
  TOO_MANY_ARGUMENTS("errors.too_many_arguments", "&c&lERROR: &7Too many arguments. Type &b/hugs help &7for the correct usage!"),
  TOO_FEW_ARGUMENTS("errors.too_few_arguments", "&c&lERROR: &7Too few arguments. Type &b/hugs help &7for the correct usage!"),

  INVALID_PLAYER("errors.invalid_player", "&c&lERROR: &7Invalid Name. Names are, at maximum, only 16 characters long."),

  PLAYER_DATA_NOT_FOUND("errors.data_not_found", "&c&lERROR: &7%target% has not given nor received any hugs. :'( \nWhat part about \"Spread the Love\" didn't you understand?"),

  NO_PERMISSION("errors.no_permission", "&cYou do not have the required permissions to use this command! Silly Goose. You need &e%permission% &cto use this command!"),
  COOLDOWN_MESSAGE("errors.cooldown_message", "&cYou must wait &e%cooldown% &cseconds before giving another hug!"),
  ;

  private static HugsPlugin plugin = JavaPlugin.getPlugin(HugsPlugin.class);
  private final String value;
  private final String def;

  Errors(String value, String def) {
    this.value = value;
    this.def = def;
  }

  public String getLangValue() {
    return plugin.getLangFile().getString(value, def).replace("%prefix%", Messages.PREFIX.getLangValue());
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return value;
  }
}
