package me.nothixal.hugs.enums.leaderboard;

public enum SortState {

  ALL_TIME(),
  TODAY(),
  THIS_WEEK(),
  THIS_MONTH(),
  ;

  private static final SortState[] values = values();

  public SortState getNext() {
    return values[(this.ordinal() + 1) % values.length];
  }

  public SortState getPrevious() {
    return values[(this.ordinal() - 1) % values.length];
  }

}
