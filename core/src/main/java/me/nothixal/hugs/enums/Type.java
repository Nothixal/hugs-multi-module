package me.nothixal.hugs.enums;

public enum Type {

  INFO,
  WARNING,
  ERROR,

  COMMAND,
  RIGHT_CLICK,
  LEFT_CLICK,
  ;

  Type() {

  }
}
