package me.nothixal.hugs.enums;

public enum Cooldowns {

  SELF_HUG_COOLDOWN("selfHugCooldown", 0),
  NORMAL_HUG_COOLDOWN("normalHugCooldown", 0),
  MASS_HUG_COOLDOWN("massHugCooldown", 0),

  HUG_COMMAND_COOLDOWN("hugCommandCooldown", 7),
  HUGS_STATS_COMMAND_COOLDOWN("hugsStatsCommandCooldown", 7),
  HUGS_TOTAL_COMMAND_COOLDOWN("hugsTotalCommandCooldown", 7),
  ;

  private final String value;
  private final int cooldown;

  Cooldowns(String value, int cooldownInSeconds) {
    this.value = value;
    this.cooldown = cooldownInSeconds;
  }

  public String getValue() {
    return value;
  }

  public int getCooldown() {
    return cooldown;
  }
}
